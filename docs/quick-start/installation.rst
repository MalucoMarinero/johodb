Installing JohoDB
=================

JohoDB can be run on the client using a prebuilt javascript file, or bundled in as an npm package.::

    npm install johodb

.. highlight:: javascript

Once installed you can require JohoDB in your source code with::

    var JohoDB = require('johodb');

Get started with your database with::
    
    var db = new JohoDB('YOUR_DATABASE_NAME');

From here you'll need to add your schema. Learn to do this at :doc:`schema-and-migrations`.

.. todo::
    Installation as a linked javascript file.
