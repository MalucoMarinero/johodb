.. highlight:: javascript
    :linenothreshold: 5

Initialising the Database
=========================
Once you've created your schema and have migrations to match, you're ready to connect to your database with:

.. js:function:: db.init([initSettings])
    
    :param object initSettings
        An object with optional settings for initialising the database.

Settings include:

:storageType: A Storage Adapter to use, if you want to override JohoDB's automatic detection. The only two available with JohoDB are: `JohoDB.WebSQLStorage` and `JohoDB.IndexedDBStorage`.

:fieldRegistry: A custom Field Registry to use. Only required if you need more field types than what JohoDB provides.

:clobber: Destroy the database if it exists and start from scratch. Good for development and testing.

:dontLoadStore: A testing setting that will stop JohoDB before it connects to the IndexedDB or WebSQL database.


While you can be specific here, for most use cases it's just fine to call `db.init()`.

JohoDB will do the following:

1. Go through the schema and build your API to save and query with, at `db.models`.
2. Connect to the Database (IndexedDB or WebSQL).
3. Check the database version (represented by number of migrations)
4. If the database is new, or older than the current version, apply migrations to bring it up to schema, creating tables and adding columns as required.

Once this is done, you will now be able to save and query data.

