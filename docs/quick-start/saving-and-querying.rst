.. highlight:: javascript
    :linenothreshold: 5

Saving and Querying Data
========================
Now that you've initialised your database, you can now save and query data. All save and query operations are asynchronous, so you'll receive promises from any operation you start, which will eventually return your desired value or an error.

Saving Data
-----------

If we were using the schema from before, with a Person and a Message table, a save could look like this::

    db.models.Person.save({
        'id': 'a-unique-id-of-some-sort',
        'name': 'Joe Bloggs',
        'age': 25
    }).then(function(savedRecord) {
        // do something once the record is saved
    }).catch(function(err) {
        // if you wish to catch any errors, throw catch on the end of
        // your promise chain
    });

.. js:function:: Model.save(saveRecord[, options])

    :param object saveRecord:
        The object to be saved into the database. This can be any object, the only thing that matters is it has attributes matching the fields on the schema.
    :param object options:
        Options for the save operation, all are optional.

        **updateRelations**: Will update the keys for any relationship fields as required. This will ONLY update the keys, and not save the rest of the data in related records.
        
        **updateRelatedRecords**: Will update any related records for relationship fields if they are provided as objects or arrays.


When you use `save` whatever you pass to it will be validated against the schema and then saved in the data store. You can also just do a validation if you want by using `validate` instead in the same way.

.. js:function:: Model.validate(saveRecord[, options])

    :param object saveRecord:
        The object to be validated.
    :param object options:
        Options for the validation operation.
    :returns Promise<validationResult>:
        Promise resolves on end of validation.

        The Validation Result returns the following information:

        - **isValid**: is true if the record passed validation
        - **errorCount**: number of invalid fields, if any
        - **validating**: a reference to the validated record
        - **errors**: an object with any field errors attached

Querying Data
-------------

Querying the data is done by building a query from chained methods and then evaluating that query. You can start a query by calling `.query()` from a model. An example where we get all of the available records::

    db.models.Person.query().all().evaluate().then(function(records) {
        // do something with the records
    });

If you want to get a single record you can use a get method in the query::

    db.models.Person.query().get({
        'name': 'Joe Bloggs'
    }).evaluate().then(function(record) {
        // do something with the record, if not found record will be null
    });

If you want to get multiple records based on a query, you can use `filter` or `exclude`::

    // get all persons with an age of 25
    db.models.Person.query().filter({'age': 25}).evaluate();

    // get all persons above the age of 25
    db.models.Person.query().filter({'age__gt': 25}).evaluate();

    // exclude all persons below of equal to the age of 25
    db.models.Person.query().exclude({'age__lte': 25}).evaluate();

The object you pass to a `get`, `filter`, or `exclude` function contains the fields you want to query by. Providing the field name alone means you wish for an exact match to the value, otherwise you can provide a modifier after two underscores to do a different type of match.

:__gt: Greater than the query value.
:__gte: Greater than or equal to the query value.
:__lt: Less than the query value.
:__lte: Less than or equal to the query value.
:__in: Equals one of the query value, which must be an array.

.. note::
    If you wish to query directly after a save operation, you should delay by a moment to ensure that the saved record shows up in the query. You can delay acting on a promise by calling `.delay(10)`.
