Field Reference
============================
See relational fields at: :doc:`relations-reference`

Field Options
-------------
The following options are available to all field types and are optional.

primaryKey
~~~~~~~~~~
If `true`, this field will be the primary key for identifying records in the table. ManyToMany tables will use this primary key as it's point of reference.

All tables must have at least **ONE** primary key.

required
~~~~~~~~~~
If `true`, this field MUST be defined as something other than `null` or `undefined` when saved.



Field Types
-------------
Fields are specified by `type` in the schema. The `type` is case insensitive, and in some cases allow for abbreviated versions of the field's formal name.

String
~~~~~~~~~~

*Matching Types*: str, string, text

A string field, for small to large sized strings. This is the only field type you can use for representing a primary key, which ideally would be similar to a UUID in structure to prevent collisions.

Integer
~~~~~~~~~~

*Matching Types*: int, integer

An integer field, returned and saved as a Javascript number. This field will be validated to ensure that the number is in fact an integer.


Float
~~~~~~~~~~

*Matching Types*: float

An float field, returned and saved as a Javascript number.


Boolean
~~~~~~~~~~
*Matching Types*: bool, boolean

A boolean field, representing a `true`, `false` or `undefined` value.

JSON
~~~~~~~~~~
*Matching Types*: json

A field that can represent any Javascript object literal. When stored in the database it will be serialised using `JSON.stringify()` and retrieved using `JSON.parse()`.

JSON field types are not indexed, so their ideal use is as a non searchable element of your database.

DateTime
~~~~~~~~
*Matching Types*: datetime

A field representing a DateTime. Can be saved as any value that can be parsed by the momentjs.com library, however it will ALWAYS be returned as a moment for continuity. Timezone information is maintained when saving.

Date
~~~~~~~~
*Matching Types*: date

A field representing a Date with no timezone information. As with DateTime, it is wrapped with momentjs.com when returned.

Time
~~~~~~~~
*Matching Types*: time

A field representing a Time with timezone information. As with DateTime, it is wrapped with momentjs.com when returned.

