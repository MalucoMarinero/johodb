Model API
===========

.. js:function:: Model.validate(validationRecord[, options])
    
    Validates the record, returning a validation result.

    :param object saveRecord:
        The object to be saved into the database. This can be any object, the only thing that matters is it has attributes matching the fields on the schema.
    :param object options:
        Options for the save operation.
    :returns Promise<validationResult>:
        Promise resolves on end of validation.

        The Validation Result returns the following information:

        - **isValid**: is true if the record passed validation
        - **errorCount**: number of invalid fields, if any
        - **validating**: a reference to the validated record
        - **errors**: an object with any field errors attached



.. js:function:: Model.save(saveRecord[, options])

    Save the record to the database.

    :param object saveRecord:
        The object to be saved into the database. This can be any object, the only thing that matters is it has attributes matching the fields on the schema.
    :param object options:
        Options for the save operation, all are optional.

        **updateRelations**: Will update the keys for any relationship fields as required. This will ONLY update the keys, and not save the rest of the data in related records.
        
        **updateRelatedRecords**: Will update any related records for relationship fields if they are provided as objects or arrays.

    :returns Promise<saveRecord>:
        Promise resolves on full completion of saving, including all extra record saves.


.. js:function:: Model.delete(deletionRecord[, options])

    Delete a record from the database.

    :param object deleteRecord:
        The object to be deleted from the database. This can be any object, the only thing that matters is that it has a primary key that corresponds to the schema.
    :param object options:
        Placeholder argument, there are no delete options at this time.

    :returns Promise<saveRecord>:
        Promise resolves on full completion of delete, including cascading deletes or nulling of foreign keys.


.. js:function:: Model.query()

    Begin a query object, for querying the database. See :doc:`query-api` for more details.
    Queries can also be immediately created with a first step by calling the relevant method first, for example, `Model.filter(args)`.

    :returns Query:

