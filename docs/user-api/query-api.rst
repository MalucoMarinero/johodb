Query API
=========

Queries are an object created from the Model API. Once created, you can add a number of steps to the query to get your desired results from the database. Queries are only executed after calling `Query.evaluate()`, which will return a promise with the appropriate results.

.. js:function:: Query.all()

    Add a step to the query that collects all records.


.. js:function:: Query.get(lookupArgs)

    Add a step to the query that collects a single record based on the lookup arguments.

    :param object lookupArgs: An object literal with lookup arguments.



.. js:function:: Query.filter(lookupArgs)

    Add a step to the query that filters records based on the lookup arguments.

    :param object lookupArgs: An object literal with lookup arguments.


.. js:function:: Query.exclude(lookupArgs)

    Add a step to the query that excludes records based on the lookup arguments.

    :param object lookupArgs: An object literal with lookup arguments.


.. js:function:: Query.evaluate([options])

    Evaluate the query as built and return the record[s] collected in a promise.
    
    :param object options:
        Options are provided as an object, but are not required.

        **depth**: Decide how deeply the evaluation will go to collect relations. By default this argument is set at 1 and will collect the nearest relations only. Raising the depth can greatly increase the execution time and result in circular record collection, which is not optimised at present.

        **only**: Specify exactly which relations the evaluation will collect. Provide these arguments as an array with the relevant relation names. If you wish to collect deeper relations, specify them using an underscore, for example: ["pets__owners", "pets__locations"]

    :return Promise<records>:
        Promise resolves once the records and all depth recovered records are collected.


Lookup Arguments
----------------
Most query steps will require lookup arguments to be used. The lookup arguments are an object literal with keys that define which field to target, and values which define what to match against. By default lookup arguments query for an exact match, but you can also provide modifiers as a postfix to the fields, for example `age__gt` for the everything greater than on the field `age`.

Modifiers
~~~~~~~~~

:__gt: Greater than the query value.
:__gte: Greater than or equal to the query value.
:__lt: Less than the query value.
:__lte: Less than or equal to the query value.
:__in: Equals one of the query value, which must be an array.


Relational Queries
~~~~~~~~~~~~~~~~~~
It is possible to perform lookups based on the fields of related objects. For example, if you had a person record with a relation `home`, you could query against it like so::

    // filter all people who live in a home in the suburb 'Moonah'
    db.models.Person.query().filter({'home__suburb': 'Moonah'}).evaluate();

Field names must be separated with two underscores. You can add modifiers to the end.

