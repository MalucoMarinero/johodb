.. highlight:: javascript
    :linenothreshold: 5

Relations Reference
========================
Relationships are established in the schema just like fields, but once created there is a little more to them.

Relation Options
----------------
The following options are available to all relation types.

relation
~~~~~~~~
The name of the table this relation will be targeted at.

relatedName
~~~~~~~~~~~
The field the opposite table will use to refer to this relation.

onDelete
~~~~~~~~
The behaviour that will occur when a relation subject is deleted. This defaults to `cascade`.

:cascade: Deletes will cascade down, where deleting the target of a foreign key relationship will also delete all records that link to it.

:setNull: When deleting the target of a foreign key relationship, all related records will have their foreign key field set to null.




Relation Types
--------------
Relations are specified by `type` in the schema. The `type` is case insensitive, and in some cases allow for abbreviated versions of the relations's formal name.

ForeignKey
~~~~~~~~~~
*Matching Types*: fk, foreignkey

The table this relation is given to can be associated to one record of the target table by using their primary key.

The target table will be able to collect all records that have it as it's foreign key by using the name specified as the `relatedName`.

ManyToMany
~~~~~~~~~~
*Matching Types*: many2many, m2m, manytomany

This table can be associated to as many of the relation table's records as you like, through a joining table that is automatically created by the underlying data store.


Saving Relations
------------------

When using the save method, you must set an option making it clear you wish to update the relationships of this record. An example::

    db.models.Household.save({
        id: "thisHouseID",
        address: "52 Goldilock Road",
        suburb: "Manjimup",
        postCode: "8732",
        occupants: [
          {id: "jrakich", firstName: "James", lastName: "Rakich", dateOfBirth: new Date(1952, 10, 12)},
          {id: "joebloggs", firstName: "Joe", lastName: "Bloggs", dateOfBirth: new Date(1982, 9, 2)}
        ]
    }, {updateRelatedRecords: true});

In this case the `Household` is the subject of a foreign key relationship from the `Person` table, with `occupants` as the `relatedName`. Because we've set `updateRelatedRecords` to true, this save will *also* save/update the occupants and set their foreign key to the correct id (`thisHouseID` in this case).

If you want to *only* update the foreign keys, without affecting the rest of the relation's data, set the option to `updateRelations` instead, and it won't touch the rest of the data, just the foreign key.

The inverse of this, with a `Person`, could look like this::

    db.models.Person.save({
        id: "jrakich",
        firstName: "James",
        lastName: "Rakich",
        dateOfBirth: new Date(1952, 10, 12)},
        home: {
            id: "thisHouseID",
            address: "52 Goldilock Road",
            suburb: "Manjimup",
            postCode: "8732"
        }
    }, {updateRelatedRecords: true});

Here the person would be saved, and so would their household, with the appropriate foreign key in it's place. If the household already existed this would be equivalent::

    db.models.Person.save({
        id: "jrakich",
        firstName: "James",
        lastName: "Rakich",
        dateOfBirth: new Date(1952, 10, 12)},
        home: "thisHouseID"
    });

As you can see in this case we can directly set the foreign key if we wish.





