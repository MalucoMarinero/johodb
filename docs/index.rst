.. johoDB documentation master file, created by
   sphinx-quickstart on Tue Apr 29 20:53:52 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to johoDB's documentation!
==================================

Preamble
--------

JohoDB is an abstraction library strictly for browser supported databases. It allows the developer to build a relational database for use on the client, completely independant of any server calls. JohoDB currently supports IndexedDB and WebSQL, which allows it to support the following browsers:

Desktop Browsers
~~~~~~~~~~~~~~~~

- Safari 3.1+
- Opera 10.5+
- Chrome 4.0+
- Firefox 16.0+
- Internet Explorer 10.0+

Mobile/Tablet Browsers
~~~~~~~~~~~~~~~~~~~~~~

- iOS Safari 3.2+
- Android 2.1+
- Chrome for Android 35+
- Blackberry 7.0+
- Opera Mobile 11.0+
- Firefox for Android 29.0+
- IE Mobile 10.0+

Local storage is not and **will never be** supported by JohoDB. It is merely a key value store and any attempt at implementation would be horribly inefficient, and looking to the future this browser support level is adequate for the purposes JohoDB is meant for, namely complex web applications.

Quickstart
----------
.. toctree::
   :maxdepth: 2

   quick-start/installation
   quick-start/schema-and-migrations
   quick-start/initialising
   quick-start/saving-and-querying

User API
----------
.. toctree::
   :maxdepth: 2

   user-api/field-reference
   user-api/relations-reference
   user-api/model-api
   user-api/query-api


Indices and tables
==================

* :ref:`search`

