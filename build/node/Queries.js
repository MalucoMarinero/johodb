///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>
var _ = require('lodash');
var Promise = require('bluebird');

(function (QueryStepType) {
    QueryStepType[QueryStepType["ALL"] = 0] = "ALL";
    QueryStepType[QueryStepType["GET"] = 1] = "GET";
    QueryStepType[QueryStepType["FILTER"] = 2] = "FILTER";
    QueryStepType[QueryStepType["EXCLUDE"] = 3] = "EXCLUDE";
})(exports.QueryStepType || (exports.QueryStepType = {}));
var QueryStepType = exports.QueryStepType;

var Query = (function () {
    function Query(model, steps) {
        if (typeof steps === "undefined") { steps = []; }
        this.model = model;
        this.steps = steps;
    }
    Query.prototype.clone = function () {
        return new Query(this.model, _.clone(this.steps));
    };

    Query.prototype.buildLookups = function (args) {
        var _this = this;
        var lookups = [];
        _.forOwn(args, function (val, key) {
            var field = key.split('__')[0];
            if (_this.model.fields[field]) {
                lookups = lookups.concat(_this.model.fields[field].queryArgToLookups(key, val, _this.model));
            } else {
                lookups.push(new FieldLookup(key, val, _this.model));
            }
        });

        return lookups;
    };

    Query.prototype.addStep = function (stepType, args) {
        if (_.keys(args).length < 1 && stepType != 0 /* ALL */) {
            throw "No arguments provided for " + QueryStepType[stepType];
        }

        var newStep = {
            type: stepType,
            lookups: this.buildLookups(args)
        };
        this.steps.push(newStep);
        return this;
    };

    Query.prototype.hasStep = function (stepType) {
        return this.steps.some(function (step) {
            return step.type == stepType;
        });
    };

    Query.prototype.getStep = function (stepType) {
        return this.steps.filter(function (step) {
            return step.type == stepType;
        })[0];
    };

    Query.prototype.all = function () {
        var newQuery = this.clone();
        newQuery.addStep(0 /* ALL */, {});
        return newQuery;
    };

    Query.prototype.get = function (args) {
        return this.clone().addStep(1 /* GET */, args);
    };

    Query.prototype.filter = function (args) {
        return this.clone().addStep(2 /* FILTER */, args);
    };

    Query.prototype.exclude = function (args) {
        return this.clone().addStep(3 /* EXCLUDE */, args);
    };

    Query.prototype.evaluate = function (opts) {
        var _this = this;
        if (!opts)
            opts = {};
        if (opts.depth == undefined)
            opts.depth = 1;

        return new Promise(function (resolve, reject) {
            var p = _this.model.store.evaluateQuery(_this.model, _this, opts);
            p = p.then(function (result) {
                if (opts.depth > 0 && result) {
                    var newOpts = { depth: opts.depth - 1 };

                    if (_.isArray(result)) {
                        var pStack = result.map(function (record) {
                            return _this.getRelatedRecords(record, newOpts);
                        });

                        var p2 = Promise.all(pStack);
                    } else {
                        var p2 = _this.getRelatedRecords(result, newOpts);
                    }
                    p2.then(function (relatedRecords) {
                        _this.zipRelatedRecords(result, relatedRecords);
                        resolve(result);
                    });
                    return p2;
                } else {
                    resolve(result);
                }
            });

            p.catch(function (error) {
                reject(error);
            });
        });
    };

    Query.prototype.getRelatedRecords = function (record, opts) {
        var _this = this;
        var stack = [];

        _.forOwn(this.model.relations, function (relation, name) {
            if (relation.type == 'ForeignKey' && record[name] != undefined) {
                var query = {};
                query[relation.relatedKey] = record[name];
                stack.push(relation.model.query().get(query).evaluate(opts));
            } else if (relation.type == 'OneToMany') {
                var query = {};
                query[relation.relatedName] = record[_this.model.primaryKey];
                stack.push(relation.model.query().filter(query).evaluate(opts));
            } else if (relation.type == 'ManyToMany') {
                stack.push(_this.model.store.collectManyToManyRelations(relation, record[_this.model.primaryKey], opts));
            } else {
                stack.push(Promise.resolve(undefined));
            }
        });

        return Promise.all(stack);
    };

    Query.prototype.zipRelatedRecords = function (result, relatedRecords) {
        var _this = this;
        if (_.isArray(result)) {
            result.forEach(function (record, ix) {
                _this.zipRelatedRecord(record, relatedRecords[ix]);
            });
        } else {
            this.zipRelatedRecord(result, relatedRecords);
        }
    };

    Query.prototype.zipRelatedRecord = function (record, relatedRecords) {
        var ix = 0;
        _.forOwn(this.model.relations, function (relation, name) {
            record[name] = relatedRecords[ix];
            ix += 1;
        });
    };
    return Query;
})();
exports.Query = Query;

(function (LookupType) {
    LookupType[LookupType["EXACT"] = 0] = "EXACT";
    LookupType[LookupType["GT"] = 1] = "GT";
    LookupType[LookupType["GTE"] = 2] = "GTE";
    LookupType[LookupType["LT"] = 3] = "LT";
    LookupType[LookupType["LTE"] = 4] = "LTE";
    LookupType[LookupType["IN"] = 5] = "IN";
})(exports.LookupType || (exports.LookupType = {}));
var LookupType = exports.LookupType;

exports.LookupTypeMap = {
    "exact": 0 /* EXACT */,
    "gt": 1 /* GT */,
    "gte": 2 /* GTE */,
    "lt": 3 /* LT */,
    "lte": 4 /* LTE */,
    "in": 5 /* IN */
};

var FieldLookup = (function () {
    function FieldLookup(name, value, model) {
        this.value = value;
        this.model = model;
        var terms = name.split('__');
        this.type = !!exports.LookupTypeMap[terms[terms.length - 1]] ? exports.LookupTypeMap[terms.pop()] : 0 /* EXACT */;
        this.fields = terms;
    }
    return FieldLookup;
})();
exports.FieldLookup = FieldLookup;
