var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
///<reference path="../../typedefs/node.d.ts"/>
///<reference path="../../typedefs/bluebird.d.ts"/>
///<reference path="../../typedefs/lodash.d.ts"/>
var _ = require('lodash');

if (window.webkitIndexedDB) {
    window.indexedDB = window.webkitIndexedDB;
    window.IDBKeyRange = window.webkitIDBKeyRange;
    window.IDBTransaction = window.webkitIDBTransaction;
}

var IDBKeyRange = window.IDBKeyRange;

var Promise = require('bluebird');

var READ_WRITE_FLAG = "readwrite";
var BaseStorage = require('./BaseStorage');
var IndexedDBMapping = require('./IndexedDBMapping');
var Schema = require('../Schema');

var Queries = require('../Queries');

var UUID = require('../utils/UUID');

var SaveAction;
(function (SaveAction) {
    SaveAction[SaveAction["UPDATE_RECORD"] = 0] = "UPDATE_RECORD";
    SaveAction[SaveAction["ADD_RECORD"] = 1] = "ADD_RECORD";
})(SaveAction || (SaveAction = {}));

var IndexedDBStorage = (function (_super) {
    __extends(IndexedDBStorage, _super);
    function IndexedDBStorage() {
        _super.apply(this, arguments);
    }
    IndexedDBStorage.prototype.initDB = function (tableSchema, migrations, clobber) {
        var _this = this;
        if (clobber) {
            this.promise = this.promise.then(function () {
                return new Promise(function (resolve, reject) {
                    console.warn('Clobbering IndexedDB', _this.name);
                    var req = indexedDB.deleteDatabase(_this.name);
                    req.onsuccess = function (e) {
                        console.warn('Clobbered IndexedDB', _this.name);
                        resolve(e);
                    };

                    req.onerror = function (e) {
                        reject(e);
                        console.error("Failed clobber of IndexedDB", e);
                    };
                });
            });
        }

        this.promise = this.promise.then(function () {
            return _this.openDB(tableSchema, migrations);
        });

        return this.promise;
    };

    IndexedDBStorage.prototype.openDB = function (tableSchema, migrations) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var req = window.indexedDB.open(_this.name, migrations.length);
            req.onsuccess = function (e) {
                var idb = e.target.result;
                resolve(idb);
            };

            req.onupgradeneeded = function (e) {
                var idb = e.target.result;
                var tx = e.target.transaction;
                var migrationsToDo = (idb.version == 1) ? migrations : migrations.slice(idb.version);

                _this.applyMigrations(tx, migrationsToDo);
            };

            req.onerror = function (e) {
                reject(e);
            };
        });
    };

    IndexedDBStorage.prototype.applyMigrations = function (tx, migrations) {
        var _this = this;
        migrations.forEach(function (migration) {
            migration.actions.forEach(function (action) {
                _this.applyMigrationAction(tx, action, migration.frozenDatabaseModel);
            });
        });
    };

    IndexedDBStorage.prototype.applyMigrationAction = function (tx, action, dbModel) {
        switch (action.type) {
            case Schema.MigrationActionTypes.ADD_TABLE:
                tx.db.createObjectStore(action.args.tableName);
                break;
            case Schema.MigrationActionTypes.ADD_COLUMN:
                var mapping = IndexedDBMapping.getMappingFor(action.args.tableName, dbModel.models[action.args.tableName].fields[action.args.columnName]);

                if (mapping.createsStore()) {
                    mapping.createIDBStore(tx);
                } else {
                    var store = tx.objectStore(action.args.tableName);
                    mapping.makeIDBIndex(store);
                }
                break;
            case Schema.MigrationActionTypes.REMOVE_COLUMN:
                var mapping = IndexedDBMapping.getMappingFor(action.args.tableName, dbModel.models[action.args.tableName].fields[action.args.columnName]);

                if (mapping.createsStore()) {
                    mapping.deleteIDBStore(tx);
                } else {
                    var store = tx.objectStore(action.args.tableName);
                    mapping.removeIDBIndex(store);
                }
                break;
            case Schema.MigrationActionTypes.ALTER_COLUMN:
                var mapping = IndexedDBMapping.getMappingFor(action.args.tableName, dbModel.models[action.args.tableName].fields[action.args.columnName]);

                if (mapping.createsStore()) {
                    mapping.deleteIDBStore(tx);
                    mapping.createIDBStore(tx);
                } else {
                    var store = tx.objectStore(action.args.tableName);
                    mapping.removeIDBIndex(store);
                    mapping.makeIDBIndex(store);
                }
                break;
            case Schema.MigrationActionTypes.REMOVE_TABLE:
                tx.db.deleteObjectStore(action.args.tableName);
                break;
        }
    };

    IndexedDBStorage.prototype.toIndexedDBRecord = function (model, record) {
        var idbRecord = {};
        _.forOwn(model.fields, function (field, name) {
            var mapping = IndexedDBMapping.getMappingFor(model.name, field);
            if (mapping.createsValue()) {
                if (mapping.changesRecord()) {
                    idbRecord = mapping.toIDBRecord(record, idbRecord);
                } else {
                    idbRecord[name] = mapping.toIDBValue(record[name]);
                }
            }
        });

        return idbRecord;
    };

    IndexedDBStorage.prototype.fromIndexedDBRecord = function (model, idbRecord) {
        var record = {};
        _.forOwn(model.fields, function (field, name) {
            var mapping = IndexedDBMapping.getMappingFor(model.name, field);
            if (mapping.createsValue()) {
                if (mapping.changesRecord()) {
                    record = mapping.fromIDBRecord(idbRecord, record);
                } else {
                    record[name] = mapping.fromIDBValue(idbRecord[name]);
                }
            }
        });

        return record;
    };

    IndexedDBStorage.prototype.updateForeignKey = function (model, recordKey, field, reverseKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.promise.then(function (idb) {
                var req = idb.transaction(model.name, READ_WRITE_FLAG).objectStore(model.name).index(model.primaryKey).openCursor(IDBKeyRange.only(recordKey));

                req.onsuccess = function (e) {
                    var cursor = e.target.result;
                    if (cursor) {
                        var record = cursor.value;
                        record[field] = reverseKey;
                        var updateReq = cursor.update(record);
                        updateReq.onsuccess = function (e) {
                            resolve(record);
                        };
                        updateReq.onerror = function (e) {
                            reject(e);
                        };
                    } else {
                        reject(new Error("ForeignKey update failed"));
                    }
                };
                req.onerror = function (e) {
                    reject(e);
                };
            });
        });
    };

    IndexedDBStorage.prototype.clearForeignKeys = function (model, recordKey, field, exceptions) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.promise.then(function (idb) {
                var stack = [];

                var req = idb.transaction(model.name, READ_WRITE_FLAG).objectStore(model.name).index(field).openCursor(IDBKeyRange.only(recordKey));

                req.onsuccess = function (e) {
                    var cursor = e.target.result;
                    if (cursor) {
                        var record = cursor.value;

                        if (exceptions.indexOf(record[model.primaryKey]) == -1) {
                            record[field] = undefined;
                            var ureq = cursor.update(record);
                            stack.push(new Promise(function (res, rej) {
                                ureq.onsuccess = function (e) {
                                    res(true);
                                };
                                ureq.onerror = function (e) {
                                    rej(false);
                                };
                            }));
                        } else {
                            cursor.continue();
                        }
                    } else {
                        Promise.all(stack).then(function (e) {
                            return resolve(true);
                        });
                    }
                };
                req.onerror = function (e) {
                    reject(e);
                };
            });
        });
    };

    IndexedDBStorage.prototype.collectManyToManyRelations = function (relation, key, opts) {
        var _this = this;
        var rel = relation;
        return new Promise(function (resolve, reject) {
            _this.promise.then(function (idb) {
                var stack = [];
                var req = idb.transaction(rel.relationTableName).objectStore(rel.relationTableName).index(rel.getOwnKey()).openCursor(IDBKeyRange.only(key));
                req.onsuccess = function (e) {
                    var cursor = e.target.result;
                    if (cursor) {
                        var record = cursor.value;
                        var recKey = record[rel.getRelKey()];
                        var query = {};
                        query[rel.model.primaryKey] = recKey;
                        stack.push(rel.model.query().get(query).evaluate(opts));
                        cursor.continue();
                    } else {
                        Promise.all(stack).then(function (records) {
                            return resolve(records);
                        });
                    }
                };

                req.onerror = function (e) {
                    reject(e);
                };
            });
        });
    };

    IndexedDBStorage.prototype.clearManyToManyRelations = function (relTable, key, field) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.promise.then(function (idb) {
                var stack = [];
                var req = idb.transaction(relTable.name, READ_WRITE_FLAG).objectStore(relTable.name).index(field).openCursor(IDBKeyRange.only(key));

                req.onsuccess = function (e) {
                    var cursor = e.target.result;
                    if (cursor) {
                        var ureq = cursor.delete();
                        stack.push(new Promise(function (res, rej) {
                            ureq.onsuccess = function (e) {
                                res(true);
                            };
                            ureq.onerror = function (e) {
                                rej(false);
                            };
                        }));
                        cursor.continue();
                    } else {
                        Promise.all(stack).then(function (e) {
                            return resolve(true);
                        });
                    }
                };
                req.onerror = function (e) {
                    reject(e);
                };
            });
        });
    };

    IndexedDBStorage.prototype.createManyToManyRelations = function (relTable, relations) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.promise.then(function (idb) {
                var stack = relations.map(function (relation) {
                    return new Promise(function (res, rej) {
                        relation['id'] = UUID.guid();
                        var req = idb.transaction(relTable.name, READ_WRITE_FLAG).objectStore(relTable.name).add(relation);
                        req.onsuccess = function (e) {
                            return res(relation);
                        };
                        req.onerror = function (e) {
                            return reject(e);
                        };
                    });
                });

                Promise.all(stack).then(function (results) {
                    return resolve(results);
                });
            });
        });
    };

    IndexedDBStorage.prototype.save = function (model, record, opts) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var saveRecord = _this.toIndexedDBRecord(model, record);

            _this.promise.then(function (idb) {
                var checkPromise = new Promise(function (chResolve, chReject) {
                    var req = idb.transaction(model.name).objectStore(model.name).index(model.primaryKey).openCursor(IDBKeyRange.only(saveRecord[model.primaryKey]));
                    req.onsuccess = function (e) {
                        chResolve(e.target.result ? 0 /* UPDATE_RECORD */ : 1 /* ADD_RECORD */);
                    };
                    req.onerror = function (e) {
                        chReject(e);
                    };
                });

                checkPromise.then(function (action) {
                    switch (action) {
                        case 1 /* ADD_RECORD */:
                            var req = idb.transaction(model.name, READ_WRITE_FLAG).objectStore(model.name).add(saveRecord, saveRecord[model.primaryKey]);
                            req.onsuccess = function (e) {
                                resolve(saveRecord);
                            };
                            req.onerror = function (e) {
                                reject(e);
                            };
                            break;

                        case 0 /* UPDATE_RECORD */:
                            var req = idb.transaction(model.name, READ_WRITE_FLAG).objectStore(model.name).index(model.primaryKey).openCursor(IDBKeyRange.only(saveRecord[model.primaryKey]));
                            req.onsuccess = function (e) {
                                var doSave = e.target.result.update(saveRecord);
                                doSave.onsuccess = function (e) {
                                    resolve(saveRecord);
                                };
                                doSave.onerror = function (e) {
                                    reject(e);
                                };
                            };
                            req.onerror = function (e) {
                                reject(e);
                            };
                            break;
                    }
                });
            });
        });
    };

    IndexedDBStorage.prototype.delete = function (model, key, opts) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.promise.then(function (idb) {
                var req = idb.transaction(model.name, READ_WRITE_FLAG).objectStore(model.name).index(model.primaryKey).openCursor(IDBKeyRange.only(key));
                req.onsuccess = function (e) {
                    var cursor = e.target.result;
                    if (cursor) {
                        var ureq = cursor.delete();
                        ureq.onsuccess = function (e) {
                            resolve(true);
                        };
                        ureq.onerror = function (e) {
                            reject(e);
                        };
                    } else {
                        resolve(true);
                    }
                };

                req.onerror = function (e) {
                    reject(e);
                };
            });
        });
    };

    IndexedDBStorage.prototype.evaluateQuery = function (model, query, opts) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.promise.then(function (idb) {
                if (query.hasStep(1 /* GET */)) {
                    var lookups = query.getStep(1 /* GET */).lookups;
                    _this.getQuery(idb, model, lookups, opts).then(function (r) {
                        return resolve(r);
                    });
                } else if (query.hasStep(0 /* ALL */)) {
                    _this.getAllQuery(idb, model, opts).then(function (r) {
                        return resolve(r);
                    });
                } else {
                    _this.executeQuery(idb, model, query.steps, opts).then(function (r) {
                        return resolve(r);
                    });
                }
            });
        });
    };

    IndexedDBStorage.prototype.getQuery = function (idb, model, lookups, opts) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var matchStack = [];

            if (lookups[0].fields.length == 1 && hasLookupBound(lookups[0])) {
                var leadLookup = lookups.shift();
                var req = idb.transaction(model.name).objectStore(model.name).index(getLookupIndex(leadLookup)).openCursor(getLookupBound(leadLookup));
            } else {
                var req = idb.transaction(model.name).objectStore(model.name).index(model.primaryKey).openCursor();
            }

            req.onsuccess = function (e) {
                var cursor = e.target.result;
                if (cursor) {
                    matchStack.push(matchLookups(lookups, cursor.value));
                    cursor.continue();
                } else {
                    Promise.all(matchStack).then(function (results) {
                        results = results.filter(function (r) {
                            return r != null;
                        });
                        if (results.length > 0) {
                            resolve(_this.fromIndexedDBRecord(model, results[0]));
                        } else {
                            resolve(null);
                        }
                    });
                }
            };
            req.onerror = function (e) {
                reject(e);
            };
        });
    };

    IndexedDBStorage.prototype.executeQuery = function (idb, model, steps, opts) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var stack = [];
            var leadStep = steps[0];
            var willExclude = leadStep.type == 3 /* EXCLUDE */;
            var canBeBound = (leadStep.lookups[0].fields.length == 1 && hasLookupBound(leadStep.lookups[0]));

            if (!canBeBound || willExclude) {
                var req = idb.transaction(model.name).objectStore(model.name).index(model.primaryKey).openCursor();
            } else {
                var leadLookup = leadStep.lookups.shift();

                var req = idb.transaction(model.name).objectStore(model.name).index(getLookupIndex(leadLookup)).openCursor(getLookupBound(leadLookup));
            }

            req.onsuccess = function (e) {
                var cursor = e.target.result;
                if (cursor) {
                    stack.push(matchSteps(steps, cursor.value));
                    cursor.continue();
                } else {
                    Promise.all(stack).then(function (results) {
                        var records = results.filter(function (r) {
                            return r != false;
                        });
                        resolve(records.map(function (r) {
                            return _this.fromIndexedDBRecord(model, r);
                        }));
                    });
                }
            };

            req.onerror = function (e) {
                reject(e);
            };
        });
    };

    IndexedDBStorage.prototype.getAllQuery = function (idb, model, opts) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var results = [];

            var req = idb.transaction(model.name).objectStore(model.name).index(model.primaryKey).openCursor();

            req.onsuccess = function (e) {
                var cursor = e.target.result;
                if (cursor) {
                    results.push(_this.fromIndexedDBRecord(model, cursor.value));
                    cursor.continue();
                } else {
                    resolve(results);
                }
            };
            req.onerror = function (e) {
                reject(e);
            };
        });
    };
    IndexedDBStorage.IndexedDBMapping = IndexedDBMapping;
    return IndexedDBStorage;
})(BaseStorage);

var matchSteps = function (steps, record) {
    return new Promise(function (resolve, reject) {
        var stack = steps.map(function (step) {
            return matchLookups(step.lookups, record);
        });

        Promise.all(stack).then(function (results) {
            var depth = 0;
            for (var ix = 0; ix < steps.length; ix++) {
                if (steps[ix].type == 3 /* EXCLUDE */) {
                    if (results[ix]) {
                        return resolve(false);
                    } else {
                        depth++;
                    }
                }

                if (steps[ix].type == 2 /* FILTER */ && results[ix]) {
                    depth++;
                }
            }

            if (depth == steps.length) {
                resolve(record);
            } else {
                resolve(false);
            }
        });
    });
};

var matchLookups = function (lookups, record) {
    return new Promise(function (resolve, reject) {
        if (lookups.length == 0) {
            resolve(record);
        } else {
            var stack = lookups.map(function (lookup) {
                return testIDBValue(lookup, record);
            });
            Promise.all(stack).then(function (results) {
                if (results.every(function (tf) {
                    return tf;
                })) {
                    resolve(record);
                } else {
                    resolve(false);
                }
            });
        }
    });
};

var getLookupTestValue = function (lookup, term) {
    var model;
    if (lookup.fields.length == 1) {
        var field = lookup.fields[0];
        model = lookup.model;
    } else {
        var field = lookup.fields[lookup.fields.length - 1];
        model = lookup.fields.reduce(function (lastModel, nextField) {
            if (lastModel.relations[nextField]) {
                return lastModel.relations[nextField].model;
            } else {
                return lastModel;
            }
        }, lookup.model);
    }

    var mapping = IndexedDBMapping.getMappingFor(model.name, model.fields[field]);
    return mapping.toIDBValue(term);
};

var testIDBValue = function (lookup, record) {
    return new Promise(function (resolve, reject) {
        getLookupRecords(lookup, record).then(function (args) {
            var records = args[0], fieldName = args[1];

            if (fieldName == '__neverReached')
                return resolve(false);

            return resolve(records.some(function (record) {
                var term = getLookupTestValue(lookup, lookup.value);
                var val = record[fieldName];
                switch (lookup.type) {
                    case 0 /* EXACT */:
                        return term === val;

                    case 1 /* GT */:
                        return val > term;

                    case 2 /* GTE */:
                        return val >= term;

                    case 3 /* LT */:
                        return val < term;

                    case 4 /* LTE */:
                        return val <= term;

                    case 5 /* IN */:
                        return term.indexOf(val) != -1;
                }
            }));
        });
    });
};

var getLookupRecords = function (lookup, record) {
    return new Promise(function (resolve, reject) {
        if (lookup.fields.length == 1) {
            resolve([[record], lookup.fields[0]]);
        } else {
            var fields = _.clone(lookup.fields);
            recurseLookupFields(fields, [record], lookup.model).then(function (results) {
                resolve(results);
            });
        }
    });
};

var recurseLookupFields = function (fields, records, model) {
    return new Promise(function (resolve, reject) {
        var field = fields.shift();
        if (fields.length == 0)
            return resolve([records, field]);

        var stack = [];
        var rel = model.relations[field];
        if (rel.type == 'ForeignKey') {
            stack = records.filter(function (r) {
                return !!r[field];
            }).map(function (record) {
                var query = {};
                query[rel.model.primaryKey] = record[field];
                return rel.model.query().get(query).evaluate({ depth: 0 });
            });

            if (stack.length == 0) {
                resolve([[records], '__neverReached']);
            } else {
                Promise.all(stack).then(function (nextRecords) {
                    recurseLookupFields(fields, nextRecords, rel.model).then(function (args) {
                        resolve(args);
                    });
                });
            }
        } else if (rel.type == 'OneToMany') {
            stack = records.map(function (record) {
                var query = {};
                query[rel.relatedName] = record[model.primaryKey];
                return rel.model.query().filter(query).evaluate({ depth: 0 });
            });

            Promise.all(stack).then(function (nextRecordArrays) {
                recurseLookupFields(fields, _.flatten(nextRecordArrays), rel.model).then(function (args) {
                    resolve(args);
                });
            });
        }
    });
};

var getLookupIndex = function (lookup) {
    return lookup.fields[0];
};

var hasLookupBound = function (lookup) {
    try  {
        getLookupBound(lookup);
        return true;
    } catch (e) {
        return false;
    }
};

var getLookupBound = function (lookup) {
    switch (lookup.type) {
        case 0 /* EXACT */:
            return IDBKeyRange.only(lookup.value);

        case 1 /* GT */:
            return IDBKeyRange.lowerBound(lookup.value, true);

        case 2 /* GTE */:
            return IDBKeyRange.lowerBound(lookup.value);

        case 3 /* LT */:
            return IDBKeyRange.upperBound(lookup.value, true);

        case 4 /* LTE */:
            return IDBKeyRange.upperBound(lookup.value);

        default:
            throw "No valid lookup found for " + Queries.LookupType[lookup.type];
    }
    ;
};

module.exports = IndexedDBStorage;
