///<reference path="../../typedefs/node.d.ts"/>
///<reference path="../../typedefs/bluebird.d.ts"/>
///<reference path="../../typedefs/moment.d.ts"/>
///<reference path="../../typedefs/lodash.d.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Promise = require('bluebird');

var _ = require('lodash');
var moment = require('moment');

var SQLMapping = (function () {
    function SQLMapping(tableName, name, field) {
        this.tableName = tableName;
        this.name = name;
        this.field = field;
    }
    SQLMapping.prototype.createsValue = function () {
        return true;
    };
    SQLMapping.prototype.createsTable = function () {
        return false;
    };
    SQLMapping.prototype.changesRecord = function () {
        return false;
    };

    SQLMapping.prototype.getConstraints = function () {
        if (this.field.primaryKey)
            return " PRIMARY KEY";
        if (this.field.unique)
            return " UNIQUE";
        return "";
    };

    SQLMapping.prototype.makeSQLField = function () {
        return this.name + " TEXT" + this.getConstraints();
    };

    SQLMapping.prototype.createSQLTable = function (idb) {
        throw new Error("createSQLTable not implemented");
    };

    SQLMapping.prototype.deleteSQLTable = function (idb) {
        throw new Error("deleteSQLTable not implemented");
    };

    SQLMapping.prototype.toSQLValue = function (value) {
        return _.isUndefined(value) ? null : value;
    };
    SQLMapping.prototype.fromSQLValue = function (value) {
        return _.isNull(value) ? undefined : value;
    };

    SQLMapping.prototype.toSQLRecord = function (record, sqlRecord) {
        throw new Error("changes record is true, yet toSQLRecord not implemented");
    };
    SQLMapping.prototype.fromSQLRecord = function (sqlRecord, record) {
        throw new Error("changes record is true, yet fromSQLRecord not implemented");
    };
    return SQLMapping;
})();
exports.SQLMapping = SQLMapping;

var StringSQLMapping = (function (_super) {
    __extends(StringSQLMapping, _super);
    function StringSQLMapping() {
        _super.apply(this, arguments);
    }
    StringSQLMapping.prototype.makeSQLField = function () {
        return this.name + " TEXT" + this.getConstraints();
    };
    return StringSQLMapping;
})(SQLMapping);
exports.StringSQLMapping = StringSQLMapping;

var IntSQLMapping = (function (_super) {
    __extends(IntSQLMapping, _super);
    function IntSQLMapping() {
        _super.apply(this, arguments);
    }
    IntSQLMapping.prototype.makeSQLField = function () {
        return this.name + " INT" + this.getConstraints();
    };
    return IntSQLMapping;
})(SQLMapping);
exports.IntSQLMapping = IntSQLMapping;

var FloatSQLMapping = (function (_super) {
    __extends(FloatSQLMapping, _super);
    function FloatSQLMapping() {
        _super.apply(this, arguments);
    }
    FloatSQLMapping.prototype.makeSQLField = function () {
        return this.name + " FLOAT" + this.getConstraints();
    };
    return FloatSQLMapping;
})(SQLMapping);
exports.FloatSQLMapping = FloatSQLMapping;

var ForeignKeySQLMapping = (function (_super) {
    __extends(ForeignKeySQLMapping, _super);
    function ForeignKeySQLMapping() {
        _super.apply(this, arguments);
    }
    ForeignKeySQLMapping.prototype.makeSQLField = function () {
        return this.name + " TEXT" + this.getConstraints();
    };
    return ForeignKeySQLMapping;
})(SQLMapping);
exports.ForeignKeySQLMapping = ForeignKeySQLMapping;

var ManyToManySQLMapping = (function (_super) {
    __extends(ManyToManySQLMapping, _super);
    function ManyToManySQLMapping() {
        _super.apply(this, arguments);
    }
    ManyToManySQLMapping.prototype.createsTable = function () {
        return true;
    };
    ManyToManySQLMapping.prototype.changesRecord = function () {
        return true;
    };

    ManyToManySQLMapping.prototype.mappingTableName = function () {
        return this.tableName + '_' + this.field.name + '__' + this.field.relation + '_' + this.field.relatedName;
    };

    ManyToManySQLMapping.prototype.createSQLTable = function (tx) {
        var fromName = this.tableName + '_' + this.field.name;
        var toName = this.field.relation + '_' + this.field.relatedName;
        var fieldSql = [
            "id TEXT PRIMARY KEY",
            fromName + " TEXT", toName + " TEXT"
        ];

        var sql = "CREATE TABLE IF NOT EXISTS " + fromName + '__' + toName;
        sql += " (" + fieldSql.join(', ') + ");";
        tx.executeSql(sql, null);
    };

    ManyToManySQLMapping.prototype.toSQLRecord = function (record, sqlRecord) {
        delete sqlRecord[this.field.name];
        return sqlRecord;
    };
    ManyToManySQLMapping.prototype.fromSQLRecord = function (sqlRecord, record) {
        return record;
    };
    return ManyToManySQLMapping;
})(SQLMapping);
exports.ManyToManySQLMapping = ManyToManySQLMapping;

var dateReviver = function (key, value) {
    if (typeof value == 'string') {
        var a = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
        if (a)
            return Date.parse(value);
    }
    ;
    return value;
};

var JSONSQLMapping = (function (_super) {
    __extends(JSONSQLMapping, _super);
    function JSONSQLMapping() {
        _super.apply(this, arguments);
    }
    JSONSQLMapping.prototype.makeSQLField = function () {
        return this.name + " TEXT" + this.getConstraints();
    };

    JSONSQLMapping.prototype.toSQLValue = function (value) {
        return _.isUndefined(value) ? null : JSON.stringify(value);
    };

    JSONSQLMapping.prototype.fromSQLValue = function (value) {
        return _.isNull(value) ? undefined : JSON.parse(value, dateReviver);
    };
    return JSONSQLMapping;
})(SQLMapping);
exports.JSONSQLMapping = JSONSQLMapping;

var DATETIME_FORMAT = 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]ZZ';
var DATE_FORMAT = 'YYYY-MM-DD';
var TIME_FORMAT = 'HH:mm:ss.SSS[Z]ZZ';
var DateTimeSQLMapping = (function (_super) {
    __extends(DateTimeSQLMapping, _super);
    function DateTimeSQLMapping() {
        _super.apply(this, arguments);
    }
    DateTimeSQLMapping.prototype.toSQLValue = function (value) {
        return _.isUndefined(value) ? null : moment(value).format(DATETIME_FORMAT);
    };

    DateTimeSQLMapping.prototype.fromSQLValue = function (value) {
        return _.isNull(value) ? undefined : moment(value, DATETIME_FORMAT);
    };
    return DateTimeSQLMapping;
})(SQLMapping);
exports.DateTimeSQLMapping = DateTimeSQLMapping;

var DateSQLMapping = (function (_super) {
    __extends(DateSQLMapping, _super);
    function DateSQLMapping() {
        _super.apply(this, arguments);
    }
    DateSQLMapping.prototype.toSQLValue = function (value) {
        return _.isUndefined(value) ? null : moment(value).format(DATE_FORMAT);
    };

    DateSQLMapping.prototype.fromSQLValue = function (value) {
        return _.isNull(value) ? undefined : moment(value, DATE_FORMAT);
    };
    return DateSQLMapping;
})(SQLMapping);
exports.DateSQLMapping = DateSQLMapping;

var TimeSQLMapping = (function (_super) {
    __extends(TimeSQLMapping, _super);
    function TimeSQLMapping() {
        _super.apply(this, arguments);
    }
    TimeSQLMapping.prototype.toSQLValue = function (value) {
        return _.isUndefined(value) ? null : moment(value).format(TIME_FORMAT);
    };

    TimeSQLMapping.prototype.fromSQLValue = function (value) {
        return _.isNull(value) ? undefined : moment(value, TIME_FORMAT);
    };
    return TimeSQLMapping;
})(SQLMapping);
exports.TimeSQLMapping = TimeSQLMapping;

var BooleanSQLMapping = (function (_super) {
    __extends(BooleanSQLMapping, _super);
    function BooleanSQLMapping() {
        _super.apply(this, arguments);
    }
    BooleanSQLMapping.prototype.makeSQLField = function () {
        return this.name + " TINYINT" + this.getConstraints();
    };

    BooleanSQLMapping.prototype.toSQLValue = function (value) {
        switch (value) {
            case true:
                return 1;
            case false:
                return 0;
            default:
                return null;
        }
    };

    BooleanSQLMapping.prototype.fromSQLValue = function (value) {
        switch (value) {
            case 1:
                return true;
            case 0:
                return false;
            default:
                return undefined;
        }
    };
    return BooleanSQLMapping;
})(SQLMapping);
exports.BooleanSQLMapping = BooleanSQLMapping;

var mappingRegister = {
    'String': StringSQLMapping,
    'Int': IntSQLMapping,
    'Float': FloatSQLMapping,
    'ForeignKey': ForeignKeySQLMapping,
    'JSON': JSONSQLMapping,
    'Boolean': BooleanSQLMapping,
    'DateTime': DateTimeSQLMapping,
    'Time': TimeSQLMapping,
    'Date': DateSQLMapping,
    'ManyToMany': ManyToManySQLMapping
};

function getMappingFor(tableName, field) {
    return new mappingRegister[field.type](tableName, field.name, field);
}
exports.getMappingFor = getMappingFor;

function addMapping(fieldType, mapper) {
    mappingRegister[fieldType] = mapper;
}
exports.addMapping = addMapping;
