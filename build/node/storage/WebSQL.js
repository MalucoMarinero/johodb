var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
///<reference path="../../typedefs/node.d.ts"/>
///<reference path="../../typedefs/bluebird.d.ts"/>
///<reference path="../../typedefs/lodash.d.ts"/>
var _ = require('lodash');

var Promise = require('bluebird');

var READ_WRITE_FLAG = "readwrite";
var BaseStorage = require('./BaseStorage');
var WebSQLMapping = require('./WebSQLMapping');
var Schema = require('../Schema');

var Queries = require('../Queries');

var UUID = require('../utils/UUID');

var WebSQLStorage = (function (_super) {
    __extends(WebSQLStorage, _super);
    function WebSQLStorage() {
        _super.apply(this, arguments);
    }
    WebSQLStorage.prototype.initDB = function (tableSchema, migrations, clobber) {
        var _this = this;
        var db = window.openDatabase(this.name, migrations.length, 'Joho Database', 2 * 1024 * 1024);

        if (clobber) {
            this.promise = this.promise.then(function () {
                return new Promise(function (resolve, reject) {
                    console.warn('Clobbering WebSQL', _this.name);
                    db.transaction(function (tx) {
                        _this.dropAllTables(tx, tableSchema);
                    }, function (tx, error) {
                        reject(error);
                        console.error("Failed clobber of WebSQL", error);
                    }, function (tx, success) {
                        console.warn('Clobbered WebSQL', _this.name);
                        resolve(success);
                    });
                });
            });
        }

        this.promise = this.promise.then(function () {
            return new Promise(function (resolve, reject) {
                db.transaction(function (tx) {
                    _this.applyMigrations(tx, migrations);
                }, function (tx, error) {
                    reject(error);
                    console.error("Failed migration of WebSQL", tx, error);
                }, function (tx, success) {
                    console.warn('Migrated WebSQL', _this.name);
                    resolve(db);
                });
            });
        });

        return this.promise;
    };

    WebSQLStorage.prototype.dropAllTables = function (tx, tableSchema) {
        _.forOwn(tableSchema.models, function (model, modelName) {
            tx.executeSql("DROP TABLE IF EXISTS " + model.name + ';');
        });
    };

    WebSQLStorage.prototype.applyMigrations = function (tx, migrations) {
        var _this = this;
        migrations.forEach(function (migration) {
            migration.actions.forEach(function (action) {
                _this.applyMigrationAction(tx, action, migration.frozenDatabaseModel);
            });
        });
    };

    WebSQLStorage.prototype.applyMigrationAction = function (tx, action, dbModel) {
        switch (action.type) {
            case Schema.MigrationActionTypes.ADD_TABLE:
                var key = dbModel.models[action.args.tableName].primaryKey;
                var pField = dbModel.models[action.args.tableName].fields[key];

                // patched in as table NEEDS a primary column
                pField['sqlUpdatedEarly'] = true;
                var mapping = WebSQLMapping.getMappingFor(action.args.tableName, pField);
                tx.executeSql("CREATE TABLE IF NOT EXISTS " + action.args.tableName + " (" + mapping.makeSQLField() + ");");
                break;
            case Schema.MigrationActionTypes.ADD_COLUMN:
                var field = dbModel.models[action.args.tableName].fields[action.args.columnName];

                if (field['sqlUpdatedEarly'])
                    return;

                var mapping = WebSQLMapping.getMappingFor(action.args.tableName, field);
                if (mapping.createsTable()) {
                    mapping.createSQLTable(tx);
                } else {
                    tx.executeSql("ALTER TABLE " + action.args.tableName + " ADD COLUMN " + mapping.makeSQLField());
                }
                break;
            case Schema.MigrationActionTypes.REMOVE_COLUMN:
                throw new Error('not implemented yet');
                break;
            case Schema.MigrationActionTypes.ALTER_COLUMN:
                throw new Error('not implemented yet');
                break;
            case Schema.MigrationActionTypes.REMOVE_TABLE:
                tx.executeSql("DROP TABLE IF EXISTS " + action.args.tableName);
                break;
        }
    };

    WebSQLStorage.prototype.toWebSQLRecord = function (model, record) {
        var sqlRecord = {};
        _.forOwn(model.fields, function (field, name) {
            var mapping = WebSQLMapping.getMappingFor(model.name, field);
            if (mapping.createsValue()) {
                if (mapping.changesRecord()) {
                    sqlRecord = mapping.toSQLRecord(record, sqlRecord);
                } else {
                    sqlRecord[name] = mapping.toSQLValue(record[name]);
                }
            }
        });

        return sqlRecord;
    };

    WebSQLStorage.prototype.fromWebSQLRecord = function (model, sqlRecord) {
        var record = {};
        _.forOwn(model.fields, function (field, name) {
            var mapping = WebSQLMapping.getMappingFor(model.name, field);
            if (mapping.createsValue()) {
                if (mapping.changesRecord()) {
                    record = mapping.fromSQLRecord(sqlRecord, record);
                } else {
                    record[name] = mapping.fromSQLValue(sqlRecord[name]);
                }
            }
        });

        return record;
    };

    WebSQLStorage.prototype.clearForeignKeys = function (model, recordKey, field, exceptions) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            exceptions = exceptions.filter(function (ex) {
                return !!ex;
            });

            _this.promise.then(function (db) {
                db.transaction(function (tx) {
                    var sql = "UPDATE " + model.name + " SET " + field + " = ? ";
                    sql += "WHERE " + field + " = ?";

                    if (exceptions.length > 0) {
                        var places = exceptions.map(function () {
                            return '?';
                        }).join(',');
                        sql += " AND " + model.primaryKey;
                        sql += " not in (" + places + ");";
                        tx.executeSql(sql, [null, recordKey].concat(exceptions));
                    } else {
                        sql += ';';
                        tx.executeSql(sql, [null, recordKey]);
                    }
                }, function (tx, error) {
                    reject(error);
                }, function (tx, success) {
                    resolve(recordKey);
                });
            });
        });
    };

    WebSQLStorage.prototype.updateForeignKey = function (model, recordKey, field, reverseKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.promise.then(function (db) {
                db.transaction(function (tx) {
                    var sql = "UPDATE " + model.name + " SET " + field + " = ? ";
                    sql += "WHERE " + model.primaryKey + " = ?;";

                    tx.executeSql(sql, [reverseKey, recordKey]);
                }, function (tx, error) {
                    reject(error);
                }, function (tx, success) {
                    resolve(recordKey);
                });
            });
        });
    };

    WebSQLStorage.prototype.collectManyToManyRelations = function (relation, key, opts) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var rel = relation;

            var stack = [];
            _this.promise.then(function (db) {
                db.transaction(function (tx) {
                    var sql = "SELECT * FROM " + rel.relationTableName + " WHERE ";
                    sql += rel.getRelKey() + " IN (SELECT ";
                    sql += rel.relationTableName + "." + rel.getRelKey();
                    sql += " FROM " + rel.relationTableName + " WHERE ";
                    sql += rel.relationTableName + "." + rel.getOwnKey() + " = ?);";

                    tx.executeSql(sql, [key], function (tx, res) {
                        stack = _.range(res.rows.length).map(function (ix) {
                            var record = res.rows.item(ix);
                            var recKey = record[rel.getRelKey()];
                            var query = {};
                            query[rel.model.primaryKey] = recKey;
                            return rel.model.query().get(query).evaluate(opts);
                        });
                    }, function (tx, error) {
                        console.error(tx, error);
                        reject([tx, error]);
                    });
                }, function (error) {
                    console.error(error);
                    reject(error);
                }, function (tx, success) {
                    Promise.all(stack).then(function (results) {
                        return resolve(results);
                    });
                });
            });
        });
    };

    WebSQLStorage.prototype.clearManyToManyRelations = function (relTable, key, field) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.promise.then(function (db) {
                db.transaction(function (tx) {
                    var sql = "DELETE FROM " + relTable.name + " WHERE " + field;
                    sql += " IN (SELECT " + relTable.name + "." + field + " ";
                    sql += "FROM " + relTable.name + " WHERE ";
                    sql += relTable.name + "." + field + " = ?);";
                    tx.executeSql(sql, [key]);
                }, function (error) {
                    console.error(error);
                    reject(error);
                }, function (tx, success) {
                    resolve(key);
                });
            });
        });
    };

    WebSQLStorage.prototype.createManyToManyRelations = function (relTable, relations) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.promise.then(function (db) {
                db.transaction(function (tx) {
                    relations.forEach(function (relation) {
                        relation['id'] = UUID.guid();
                        var fieldList = [];
                        var valueList = [];
                        var shroud = [];
                        _.forOwn(relation, function (value, key) {
                            fieldList.push(key);
                            valueList.push(value);
                            shroud.push('?');
                        });

                        var sql = "INSERT INTO " + relTable.name + " (";
                        sql += fieldList.join(",") + ") VALUES (";
                        sql += shroud.join(",") + ")";

                        tx.executeSql(sql, valueList);
                    });
                }, function (error) {
                    console.error(error);
                    reject(error);
                }, function (tx, success) {
                    resolve(relations);
                });
            });
        });
    };

    WebSQLStorage.prototype.save = function (model, record, opts) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var saveRecord = _this.toWebSQLRecord(model, record);

            _this.promise.then(function (db) {
                db.transaction(function (tx) {
                    var fieldList = [];
                    var valueList = [];
                    var shroud = [];

                    _.forOwn(saveRecord, function (value, key) {
                        fieldList.push(key);
                        valueList.push(value);
                        shroud.push('?');
                    });

                    var sql = "INSERT OR REPLACE INTO " + model.name + " (";
                    sql += fieldList.join(',') + ') VALUES (';
                    sql += shroud.join(',') + ')';

                    tx.executeSql(sql, valueList);
                }, function (error) {
                    console.error(error);
                    reject(error);
                }, function (tx, success) {
                    resolve(saveRecord);
                });
            });
        });
    };

    WebSQLStorage.prototype.delete = function (model, key, opts) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.promise.then(function (db) {
                db.transaction(function (tx) {
                    var sql = "DELETE FROM " + model.name + " WHERE ";
                    sql += model.primaryKey + ' = ?';
                    tx.executeSql(sql, [key]);
                }, function (error) {
                    console.error(error);
                    reject(error);
                }, function (tx, success) {
                    resolve(true);
                });
            });
        });
    };

    WebSQLStorage.prototype.evaluateQuery = function (model, query, opts) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.promise.then(function (db) {
                if (query.hasStep(1 /* GET */)) {
                    var lookups = query.getStep(1 /* GET */).lookups;
                    _this.getQuery(db, model, lookups, opts).then(function (r) {
                        return resolve(r);
                    });
                } else if (query.hasStep(0 /* ALL */)) {
                    _this.getAllQuery(db, model, opts).then(function (r) {
                        return resolve(r);
                    });
                } else {
                    _this.executeQuery(db, model, query.steps, opts).then(function (r) {
                        return resolve(r);
                    });
                }
            });
        });
    };

    WebSQLStorage.prototype.executeQuery = function (idb, model, steps, opts) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var results;
            _this.promise.then(function (db) {
                db.readTransaction(function (tx) {
                    var sql = "SELECT * FROM " + model.name + " ";
                    sql += stepsToSql(model, steps);
                    sql += ";";

                    tx.executeSql(sql, null, function (tx, res) {
                        results = _.range(res.rows.length).map(function (ix) {
                            return _this.fromWebSQLRecord(model, res.rows.item(ix));
                        });
                    }, function (tx, res) {
                        reject(res);
                    });
                }, function (error) {
                    console.error(error);
                    reject(error);
                }, function (tx, success) {
                    resolve(results);
                });
            });
        });
    };

    WebSQLStorage.prototype.getQuery = function (idb, model, lookups, opts) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var steps = [{
                    type: 1 /* GET */,
                    lookups: lookups
                }];

            var result;

            _this.promise.then(function (db) {
                db.readTransaction(function (tx) {
                    var sql = "SELECT * FROM " + model.name + " ";
                    sql += stepsToSql(model, steps);
                    sql += ";";

                    tx.executeSql(sql, null, function (tx, res) {
                        if (res.rows.length > 0) {
                            result = _this.fromWebSQLRecord(model, res.rows.item(0));
                        }
                    }, function (tx, res) {
                        reject(res);
                    });
                }, function (error) {
                    console.error(error);
                    reject(error);
                }, function (tx, success) {
                    resolve(result || null);
                });
            });
        });
    };

    WebSQLStorage.prototype.getAllQuery = function (idb, model, opts) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var results = [];

            _this.promise.then(function (db) {
                db.readTransaction(function (tx) {
                    tx.executeSql("SELECT * FROM " + model.name + ";", null, function (tx, res) {
                        results = _.range(res.rows.length).map(function (ix) {
                            return _this.fromWebSQLRecord(model, res.rows.item(ix));
                        });
                    }, function (tx, res) {
                        reject(res);
                    });
                }, function (error) {
                    console.error(error);
                }, function (tx, success) {
                    resolve(results);
                });
            });
        });
    };
    return WebSQLStorage;
})(BaseStorage);

var buildStatement = function (clauses) {
    var statement = '';
    if (clauses.joinType) {
        statement += clauses.joinType + " JOIN ";
        statement += clauses.joinTable.join(', ');

        if (clauses.on.length > 0) {
            statement += ' ON ' + clauses.on.join(' AND ');
        }
    }

    if (clauses.where.length > 0) {
        statement += ' WHERE ';
        statement += clauses.where.join(' AND ');
    }

    return statement;
};

var stepsToSql = function (model, steps) {
    var clauses = steps.reduce(function (clauses, step) {
        return processStep(clauses, model, step);
    }, {
        on: [],
        joinTable: [],
        joinType: null,
        where: []
    });

    return buildStatement(clauses);
};

var processStep = function (clauses, model, step) {
    return step.lookups.reduce(function (clauses, lookup) {
        return processLookup(clauses, model, step, lookup);
    }, clauses);
};

var processLookup = function (clauses, model, step, lookup) {
    var fields = _.clone(lookup.fields);
    return recurseJoin(clauses, model, step, lookup, fields);
};

var sType = Queries.QueryStepType;
var recurseJoin = function (clauses, model, step, lookup, fields) {
    var field = fields.shift();

    if (fields.length == 0) {
        var sql = step.type == 3 /* EXCLUDE */ ? 'NOT ' : '';
        sql += model.name + "." + getWhereClause(lookup, field);

        clauses.where.push(sql);
        return clauses;
    } else {
        var rel = model.relations[field];

        if (rel.type == 'ForeignKey') {
            clauses.joinType = 'INNER';
            clauses.joinTable.push(rel.model.name);
            clauses.on.push(rel.ownModel + "." + rel.name + " = " + rel.relatedModel + "." + rel.relatedKey);
            clauses = recurseJoin(clauses, rel.model, step, lookup, fields);
        } else if (rel.type == 'OneToMany') {
            clauses.joinType = 'INNER';
            clauses.joinTable.push(rel.model.name);
            clauses.on.push(rel.ownModel + "." + rel.ownKey + " = " + rel.relatedModel + "." + rel.relatedName);
            clauses = recurseJoin(clauses, rel.model, step, lookup, fields);
        }
        ;

        return clauses;
    }
};

var getLookupTestValue = function (lookup, term) {
    var model;
    if (lookup.fields.length == 1) {
        var field = lookup.fields[0];
        model = lookup.model;
    } else {
        var field = lookup.fields[lookup.fields.length - 1];
        model = lookup.fields.reduce(function (lastModel, nextField) {
            if (lastModel.relations[nextField]) {
                return lastModel.relations[nextField].model;
            } else {
                return lastModel;
            }
        }, lookup.model);
    }

    var mapping = WebSQLMapping.getMappingFor(model.name, model.fields[field]);
    return mapping.toSQLValue(term);
};

var WhereOperatorMap = {};
WhereOperatorMap[0 /* EXACT */] = "=";
WhereOperatorMap[1 /* GT */] = ">";
WhereOperatorMap[2 /* GTE */] = ">=";
WhereOperatorMap[3 /* LT */] = "<";
WhereOperatorMap[4 /* LTE */] = "=<";

var getWhereClause = function (lookup, field) {
    if (lookup.type == 5 /* IN */) {
        var values = lookup.value.map(function (val) {
            return '"' + val + '"';
        }).join(',');
        return field + " IN (" + values + ")";
    }
    ;

    var operator = WhereOperatorMap[lookup.type];
    if (!operator) {
        throw "No valid where query for " + Queries.LookupType[lookup.type];
    }

    return field + " " + operator + ' "' + getLookupTestValue(lookup, lookup.value) + '"';
};

module.exports = WebSQLStorage;
