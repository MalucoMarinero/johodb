///<reference path="../../typedefs/node.d.ts"/>
///<reference path="../../typedefs/bluebird.d.ts"/>
var Promise = require('bluebird');

var BaseStorage = (function () {
    function BaseStorage(name) {
        this.name = name;
        this.promise = Promise.resolve(true);
    }
    BaseStorage.prototype.initDB = function (tableSchema, migrations, clobber) {
        throw "initDB Not implemented";
    };

    BaseStorage.prototype.save = function (model, record, opts) {
        throw "save not implemented";
    };

    BaseStorage.prototype.delete = function (model, key, opts) {
        throw "delete not implemented";
    };

    BaseStorage.prototype.evaluateQuery = function (model, query, opts) {
        throw "evaluateQuery not implemented";
    };

    BaseStorage.prototype.updateForeignKey = function (model, recordKey, field, reverseKey) {
        throw "updateForeignKey not implemented";
    };

    BaseStorage.prototype.clearForeignKeys = function (model, recordKey, field, exceptions) {
        throw "clearForeignKeys not implemented";
    };

    BaseStorage.prototype.collectManyToManyRelations = function (relation, key, opts) {
        throw "collectManyToManyRelations not implemented";
    };

    BaseStorage.prototype.clearManyToManyRelations = function (relTable, key, field) {
        throw "clearManyToManyRelations not implemented";
    };

    BaseStorage.prototype.createManyToManyRelations = function (relTable, relations) {
        throw "createManyToManyRelations not implemented";
    };
    return BaseStorage;
})();

module.exports = BaseStorage;
