///<reference path="../../typedefs/node.d.ts"/>
///<reference path="../../typedefs/moment.d.ts"/>
///<reference path="../../typedefs/bluebird.d.ts"/>
///<reference path="../../typedefs/lodash.d.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Promise = require('bluebird');

var READ_WRITE_FLAG = "readwrite";

var _ = require('lodash');
var moment = require('moment');

var IDBMapping = (function () {
    function IDBMapping(tableName, name, field) {
        this.tableName = tableName;
        this.name = name;
        this.field = field;
    }
    IDBMapping.prototype.createsValue = function () {
        return true;
    };
    IDBMapping.prototype.createsStore = function () {
        return false;
    };
    IDBMapping.prototype.changesRecord = function () {
        return false;
    };

    IDBMapping.prototype.makeIDBIndex = function (store) {
        var opts = {};
        opts.unique = this.field.primaryKey ? true : false;
        if (this.field.unique)
            opts.unique = true;

        store.createIndex(this.field.name, this.field.name, opts);
    };

    IDBMapping.prototype.removeIDBIndex = function (store) {
        store.deleteIndex(this.field.name);
    };

    IDBMapping.prototype.createIDBStore = function (idb) {
        throw new Error("createIDBStore not implemented");
    };

    IDBMapping.prototype.deleteIDBStore = function (idb) {
        throw new Error("deleteIDBStore not implemented");
    };

    IDBMapping.prototype.toIDBRecord = function (record, idbRecord) {
        throw new Error("changes record is true, yet toIDBRecord not implemented");
    };
    IDBMapping.prototype.fromIDBRecord = function (idbRecord, record) {
        throw new Error("changes record is true, yet fromIDBRecord not implemented");
    };

    IDBMapping.prototype.toIDBValue = function (value) {
        return value;
    };
    IDBMapping.prototype.fromIDBValue = function (value) {
        return value;
    };
    return IDBMapping;
})();
exports.IDBMapping = IDBMapping;

var dateReviver = function (key, value) {
    if (typeof value == 'string') {
        var a = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
        if (a)
            return Date.parse(value);
    }
    ;
    return value;
};

var JSONIDBMapping = (function (_super) {
    __extends(JSONIDBMapping, _super);
    function JSONIDBMapping() {
        _super.apply(this, arguments);
    }
    JSONIDBMapping.prototype.makeIDBIndex = function (store) {
        return;
    };
    JSONIDBMapping.prototype.removeIDBIndex = function (store) {
        return;
    };

    JSONIDBMapping.prototype.toIDBValue = function (value) {
        return JSON.stringify(value);
    };
    JSONIDBMapping.prototype.fromIDBValue = function (value) {
        return _.isUndefined(value) ? undefined : JSON.parse(value, dateReviver);
    };
    return JSONIDBMapping;
})(IDBMapping);
exports.JSONIDBMapping = JSONIDBMapping;

var DATETIME_FORMAT = 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]ZZ';
var DATE_FORMAT = 'YYYY-MM-DD';
var TIME_FORMAT = 'HH:mm:ss.SSS[Z]ZZ';
var DateTimeIDBMapping = (function (_super) {
    __extends(DateTimeIDBMapping, _super);
    function DateTimeIDBMapping() {
        _super.apply(this, arguments);
    }
    DateTimeIDBMapping.prototype.toIDBValue = function (value) {
        return _.isUndefined(value) ? undefined : moment(value).format(DATETIME_FORMAT);
    };

    DateTimeIDBMapping.prototype.fromIDBValue = function (value) {
        return _.isUndefined(value) ? undefined : moment(value, DATETIME_FORMAT);
    };
    return DateTimeIDBMapping;
})(IDBMapping);
exports.DateTimeIDBMapping = DateTimeIDBMapping;

var DateIDBMapping = (function (_super) {
    __extends(DateIDBMapping, _super);
    function DateIDBMapping() {
        _super.apply(this, arguments);
    }
    DateIDBMapping.prototype.toIDBValue = function (value) {
        return _.isUndefined(value) ? undefined : moment(value).format(DATE_FORMAT);
    };

    DateIDBMapping.prototype.fromIDBValue = function (value) {
        return _.isUndefined(value) ? undefined : moment(value, DATE_FORMAT);
    };
    return DateIDBMapping;
})(IDBMapping);
exports.DateIDBMapping = DateIDBMapping;

var TimeIDBMapping = (function (_super) {
    __extends(TimeIDBMapping, _super);
    function TimeIDBMapping() {
        _super.apply(this, arguments);
    }
    TimeIDBMapping.prototype.toIDBValue = function (value) {
        return _.isUndefined(value) ? undefined : moment(value).format(TIME_FORMAT);
    };

    TimeIDBMapping.prototype.fromIDBValue = function (value) {
        return _.isUndefined(value) ? undefined : moment(value, TIME_FORMAT);
    };
    return TimeIDBMapping;
})(IDBMapping);
exports.TimeIDBMapping = TimeIDBMapping;

var BooleanIDBMapping = (function (_super) {
    __extends(BooleanIDBMapping, _super);
    function BooleanIDBMapping() {
        _super.apply(this, arguments);
    }
    BooleanIDBMapping.prototype.toIDBValue = function (value) {
        switch (value) {
            case true:
                return 1;
            case false:
                return 0;
            default:
                return undefined;
        }
    };

    BooleanIDBMapping.prototype.fromIDBValue = function (value) {
        switch (value) {
            case 1:
                return true;
            case 0:
                return false;
            default:
                return undefined;
        }
    };
    return BooleanIDBMapping;
})(IDBMapping);
exports.BooleanIDBMapping = BooleanIDBMapping;

var ManyToManyIDBMapping = (function (_super) {
    __extends(ManyToManyIDBMapping, _super);
    function ManyToManyIDBMapping() {
        _super.apply(this, arguments);
    }
    ManyToManyIDBMapping.prototype.createsStore = function () {
        return true;
    };

    ManyToManyIDBMapping.prototype.mappingStoreName = function () {
        return this.tableName + '_' + this.field.name + '__' + this.field.relation + '_' + this.field.relatedName;
    };

    ManyToManyIDBMapping.prototype.createIDBStore = function (tx) {
        tx.db.createObjectStore(this.mappingStoreName(), { keyPath: 'id' });
        var store = tx.objectStore(this.mappingStoreName());
        var fromName = this.tableName + '_' + this.field.name;
        store.createIndex(fromName, fromName, {});

        var toName = this.field.relation + '_' + this.field.relatedName;
        store.createIndex(toName, toName, {});
    };

    ManyToManyIDBMapping.prototype.deleteIDBStore = function (tx) {
        tx.db.deleteObjectStore(this.mappingStoreName());
    };
    return ManyToManyIDBMapping;
})(IDBMapping);
exports.ManyToManyIDBMapping = ManyToManyIDBMapping;

var mappingRegister = {
    'String': IDBMapping,
    'Int': IDBMapping,
    'Float': IDBMapping,
    'JSON': JSONIDBMapping,
    'Boolean': BooleanIDBMapping,
    'ForeignKey': IDBMapping,
    'DateTime': DateTimeIDBMapping,
    'Time': TimeIDBMapping,
    'Date': DateIDBMapping,
    'ManyToMany': ManyToManyIDBMapping
};

function getMappingFor(tableName, field) {
    return new mappingRegister[field.type](tableName, field.name, field);
}
exports.getMappingFor = getMappingFor;

function addMapping(fieldType, mapper) {
    mappingRegister[fieldType] = mapper;
}
exports.addMapping = addMapping;
