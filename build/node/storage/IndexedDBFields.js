///<reference path="../../typedefs/node.d.ts"/>
///<reference path="../../typedefs/bluebird.d.ts"/>
var Promise = require('bluebird');

var READ_WRITE_FLAG = "readwrite";

var IDBMapping = (function () {
    function IDBMapping(name, field) {
        this.name = name;
        this.field = field;
    }
    IDBMapping.prototype.makeIDBIndex = function (store) {
    };
    return IDBMapping;
})();
