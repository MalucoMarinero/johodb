///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/moment.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>
var _ = require('lodash');
var moment = require('moment');
var Promise = require('bluebird');

function isString(value) {
    if (!_.isString(value)) {
        throw 'Value is not a valid string.';
    }
    ;
}
exports.isString = isString;

function isInteger(value) {
    if (value !== +value && value !== (value | 0)) {
        throw 'Value is not an integer.';
    }
    ;
}
exports.isInteger = isInteger;

function isNumber(value) {
    if (!_.isNumber(value) || _.isNaN(value)) {
        throw 'Value is not a valid number.';
    }
    ;
}
exports.isNumber = isNumber;

function isBoolean(value) {
    if (!_.isBoolean(value)) {
        throw 'Value is not a boolean.';
    }
    ;
}
exports.isBoolean = isBoolean;

function isValidDateOrTime(value) {
    if (!moment(value).isValid()) {
        throw 'Value is not a valid date or time.';
    }
}
exports.isValidDateOrTime = isValidDateOrTime;

var emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|ca|info|mobi|name|aero|asia|jobs|museum)\b/;

function isEmail(value) {
    if (!emailRegex.test(value)) {
        throw "Email address is not valid.";
    }
}
exports.isEmail = isEmail;
