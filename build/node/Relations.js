///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var _ = require('lodash');
var Promise = require('bluebird');

var Fields = require('./Fields');

var Relation = (function () {
    function Relation(fromModelName, fromField, noCheck) {
        if (!noCheck) {
            switch (fromField.type) {
                case 'ForeignKey':
                    return new ForeignKeyRelation(fromModelName, fromField, true);
                case 'ManyToMany':
                    return new ManyToManyRelation(fromModelName, fromField, true);
            }
        }
        this.type = fromField.type;
        this.from = {
            modelName: fromModelName,
            fieldName: fromField.name
        };

        if (fromField.type != 'GenericKey') {
            this.to = {
                modelName: fromField.relation,
                fieldName: fromField.relatedName
            };
        }

        this.onDelete = fromField.onDelete ? fromField.onDelete : 'cascade';
    }
    Object.defineProperty(Relation.prototype, "requiresRelationTable", {
        get: function () {
            return this['constructor']['requiresRelationTable'];
        },
        enumerable: true,
        configurable: true
    });

    Relation.prototype.buildRelationManagers = function () {
        throw new Error('buildRelationManagers not implemented');
    };

    Relation.prototype.getTableName = function () {
        throw new Error('getTableName not implemented');
    };

    Relation.prototype.buildRelationTable = function () {
        throw new Error('buildRelationTable not implemented');
    };
    return Relation;
})();
exports.Relation = Relation;

var RelationTable = (function () {
    function RelationTable(name, from, to) {
        this.name = name;
        this.fields = {};
        this.fields['id'] = new Fields.StringField("id", {
            type: "string", primaryKey: true
        });

        this.primaryKey = "id";

        var fromName = from.modelName + '_' + from.primaryKey;
        this.fields[fromName] = new Fields.StringField(fromName, {
            type: "string", required: true
        });

        var toName = to.modelName + '_' + to.primaryKey;
        this.fields[toName] = new Fields.StringField(toName, {
            type: "string", required: true
        });
    }
    return RelationTable;
})();
exports.RelationTable = RelationTable;

var ForeignKeyRelation = (function (_super) {
    __extends(ForeignKeyRelation, _super);
    function ForeignKeyRelation() {
        _super.apply(this, arguments);
    }
    ForeignKeyRelation.prototype.buildRelationManagers = function () {
        return [
            new ForeignKeyRelationManager(this.from.fieldName, this.from.primaryKey, this.from.modelName, this.to.fieldName, this.to.primaryKey, this.to.modelName),
            new OneToManyRelationManager(this.to.fieldName, this.to.primaryKey, this.to.modelName, this.from.fieldName, this.from.primaryKey, this.from.modelName, this.onDelete)
        ];
    };
    ForeignKeyRelation.requiresRelationTable = false;
    return ForeignKeyRelation;
})(Relation);
exports.ForeignKeyRelation = ForeignKeyRelation;

var ManyToManyRelation = (function (_super) {
    __extends(ManyToManyRelation, _super);
    function ManyToManyRelation() {
        _super.apply(this, arguments);
    }
    ManyToManyRelation.prototype.buildRelationManagers = function () {
        return [
            new ManyToManyRelationManager(this.from.fieldName, this.from.primaryKey, this.from.modelName, this.to.fieldName, this.to.primaryKey, this.to.modelName, this.onDelete, this.getTableName()),
            new ManyToManyRelationManager(this.to.fieldName, this.to.primaryKey, this.to.modelName, this.from.fieldName, this.from.primaryKey, this.from.modelName, this.onDelete, this.getTableName())
        ];
    };

    ManyToManyRelation.prototype.getTableName = function () {
        return this.from.modelName + '_' + this.from.fieldName + '__' + this.to.modelName + '_' + this.to.fieldName;
    };

    ManyToManyRelation.prototype.buildRelationTable = function () {
        return new RelationTable(this.getTableName(), this.from, this.to);
    };
    ManyToManyRelation.requiresRelationTable = true;
    return ManyToManyRelation;
})(Relation);
exports.ManyToManyRelation = ManyToManyRelation;

var ModelRelationManager = (function () {
    function ModelRelationManager(name, ownKey, ownModel, relatedName, relatedKey, relatedModel, onDelete, relationTableName) {
        this.name = name;
        this.ownKey = ownKey;
        this.ownModel = ownModel;
        this.relatedName = relatedName;
        this.relatedKey = relatedKey;
        this.relatedModel = relatedModel;
        this.onDelete = onDelete;
        this.relationTableName = relationTableName;
        this.type = this.getRelationType();
    }
    ModelRelationManager.prototype.getRelationType = function () {
        return "None";
    };

    ModelRelationManager.prototype.validate = function (value, opts) {
        throw new Error('Validate not implemented');
    };

    ModelRelationManager.prototype.reduce = function (value) {
        throw new Error('Reduce Relation not implemented');
    };

    ModelRelationManager.prototype.collectRelatedRecords = function (value, reversePk) {
        throw new Error('Collect Related Records not implemented');
    };

    ModelRelationManager.prototype.getOwnKey = function () {
        return this.ownModel + '_' + this.name;
    };
    ModelRelationManager.prototype.getRelKey = function () {
        return this.relatedModel + '_' + this.relatedName;
    };
    return ModelRelationManager;
})();
exports.ModelRelationManager = ModelRelationManager;

var ForeignKeyRelationManager = (function (_super) {
    __extends(ForeignKeyRelationManager, _super);
    function ForeignKeyRelationManager() {
        _super.apply(this, arguments);
    }
    ForeignKeyRelationManager.prototype.getRelationType = function () {
        return "ForeignKey";
    };

    ForeignKeyRelationManager.prototype.validate = function (value, opts) {
        var errors = [];

        if (_.isObject(value)) {
            if (_.isUndefined(value[this.relatedKey])) {
                errors.push("No Primary Key provided for relation '" + this.name + "'");
            }
        }
        if (_.isArray(value)) {
            errors.push("Foreign Key '" + this.name + "' doesn't support arrays.");
        }
        return errors;
    };

    ForeignKeyRelationManager.prototype.reduce = function (value) {
        return _.isObject(value) ? value[this.relatedKey] : value;
    };

    ForeignKeyRelationManager.prototype.collectRelatedRecords = function (value, reversePk) {
        return _.isObject(value) ? [value] : [];
    };
    return ForeignKeyRelationManager;
})(ModelRelationManager);
exports.ForeignKeyRelationManager = ForeignKeyRelationManager;

var OneToManyRelationManager = (function (_super) {
    __extends(OneToManyRelationManager, _super);
    function OneToManyRelationManager() {
        _super.apply(this, arguments);
    }
    OneToManyRelationManager.prototype.getRelationType = function () {
        return "OneToMany";
    };
    OneToManyRelationManager.prototype.reduce = function (value) {
        return undefined;
    };

    OneToManyRelationManager.prototype.validate = function (value, opts) {
        var _this = this;
        var errors = [];

        if (_.isArray(value)) {
            value.forEach(function (rel) {
                if (_.isUndefined(rel[_this.relatedKey]) && (opts.updateRelatedRecords || opts.updateRelationships)) {
                    errors.push("No primary key provided for relation in '" + _this.name + "', yet it's meant to be updated/saved.");
                }
            });
        }
        ;

        return errors;
    };

    OneToManyRelationManager.prototype.collectRelatedRecords = function (value, reversePk) {
        var _this = this;
        return _.isArray(value) ? value.map(function (val) {
            val[_this.relatedName] = reversePk;
            return val;
        }) : [];
    };

    OneToManyRelationManager.prototype.collectKeys = function (value, reversePk) {
        var _this = this;
        return _.isArray(value) ? value.map(function (val) {
            return val[_this.model.primaryKey];
        }) : [];
    };
    return OneToManyRelationManager;
})(ModelRelationManager);
exports.OneToManyRelationManager = OneToManyRelationManager;

var ManyToManyRelationManager = (function (_super) {
    __extends(ManyToManyRelationManager, _super);
    function ManyToManyRelationManager() {
        _super.apply(this, arguments);
    }
    ManyToManyRelationManager.prototype.getRelationType = function () {
        return "ManyToMany";
    };
    ManyToManyRelationManager.prototype.reduce = function (value) {
        return undefined;
    };

    ManyToManyRelationManager.prototype.validate = function (value, opts) {
        var _this = this;
        var errors = [];

        if (_.isArray(value)) {
            value.forEach(function (rel) {
                if (_.isObject(rel) && _.isUndefined(rel[_this.relatedKey]) && opts.updateRelationships) {
                    errors.push("No primary key provided for relation '" + _this.name + "', yet it's meant to be updated/saved.");
                }
            });
        }
        ;

        return errors;
    };

    ManyToManyRelationManager.prototype.collectRelatedRecords = function (value, reversePk) {
        return _.isArray(value) ? value : [];
    };

    ManyToManyRelationManager.prototype.collectKeys = function (value, reversePk) {
        var _this = this;
        return _.isArray(value) ? value.map(function (val) {
            var relRecord = {};
            relRecord[_this.getOwnKey()] = reversePk;
            if (_.isObject(val)) {
                relRecord[_this.getRelKey()] = val[_this.model.primaryKey];
            } else {
                relRecord[_this.getRelKey()] = val;
            }
            return relRecord;
        }) : [];
    };
    return ManyToManyRelationManager;
})(ModelRelationManager);
exports.ManyToManyRelationManager = ManyToManyRelationManager;
