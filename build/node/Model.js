///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>
var _ = require('lodash');
var Promise = require('bluebird');

var Relations = require('./Relations');

var Queries = require('./Queries');

var Model = (function () {
    function Model(name, tableSchema, fieldRegistry) {
        var _this = this;
        this.name = name;
        this.fields = {};
        this.relations = {};
        _.forOwn(tableSchema, function (columnSchema, fieldName) {
            _this.fields[fieldName] = fieldRegistry.createField(fieldName, columnSchema);
        });

        this.primaryKey = this.getPrimaryKey().name;
    }
    Model.prototype.setStore = function (store) {
        this.store = store;
    };

    Model.prototype.getPrimaryKey = function () {
        var field = _.find(this.fields, function (field) {
            return field.primaryKey;
        });
        if (field)
            return field;
        throw new Error("No Primary Key found");
    };

    Model.prototype.getRelations = function () {
        var _this = this;
        var relations = [];
        _.forOwn(this.fields, function (field, fieldName) {
            if (field.type.match(/ForeignKey|ManyToMany/)) {
                relations.push(new Relations.Relation(_this.name, field));
            }
            ;
        });
        return relations;
    };

    Model.prototype.validate = function (record, opts) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var result = {
                isValid: true, errorCount: 0, validating: record, errors: {}
            };

            var validateOnFieldOrRelation = function (field, key) {
                var errors = field.validate(record[key], opts);
                if (errors.length > 0) {
                    result.errorCount += errors.length;
                    result.errors[key] = errors;
                }
                ;
            };

            _.forOwn(_this.fields, validateOnFieldOrRelation);
            _.forOwn(_this.relations, validateOnFieldOrRelation);

            if (result.errorCount > 0)
                result.isValid = false;
            resolve(result);
        });
    };

    Model.prototype.reduceRecord = function (record) {
        var saveRecord = {};
        _.forOwn(this.fields, function (field, key) {
            if (field.defaultValue && _.isUndefined(record[key])) {
                saveRecord[key] = field.defaultValue;
            } else {
                saveRecord[key] = record[key];
            }
        });

        _.forOwn(this.relations, function (relation, key) {
            if (relation.type == 'ForeignKey') {
                saveRecord[key] = relation.reduce(record[key]);
            } else if (relation.type == 'ManyToMany') {
                delete saveRecord[key];
            } else if (relation.type == 'OneToMany') {
                delete saveRecord[key];
            }
        });

        return saveRecord;
    };

    Model.prototype.collectRelatedRecordUpdates = function (record, thisKey) {
        var updates = {};

        _.forOwn(this.relations, function (r, key) {
            var records = r.collectRelatedRecords(record[key], thisKey);
            var keys = r.type != 'ForeignKey' ? r.collectKeys(record[key]) : [];

            updates[r.relatedModel] = {
                'model': r.model,
                'records': records,
                'removalExceptions': {}
            };
            updates[r.relatedModel]['removalExceptions'][r.relatedName] = keys;
        });

        return updates;
    };

    Model.prototype.collectForeignKeyUpdates = function (record, thisKey) {
        var updates = {};
        _.forOwn(this.relations, function (r, key) {
            updates[r.name] = { model: r.model, name: r.relatedName, ids: [] };

            if (r.type == 'OneToMany') {
                updates[r.name].ids = r.collectKeys(record[key]);
            }
        });

        return updates;
    };

    Model.prototype.collectManyToManyUpdates = function (record, thisKey) {
        var updates = {};
        _.forOwn(this.relations, function (r, key) {
            if (r.type == "ManyToMany") {
                updates[r.relationTableName] = {
                    name: r.name,
                    model: r.model,
                    updates: r.collectKeys(record[key], thisKey),
                    ownField: r.getOwnKey()
                };
            }
        });

        return updates;
    };

    Model.prototype.updateForeignKeyRelation = function (recordKey, field, reverseKey) {
        return this.store.updateForeignKey(this, recordKey, field, reverseKey);
    };

    Model.prototype.clearForeignKeyRelations = function (recordKey, field, exceptions) {
        return this.store.clearForeignKeys(this, recordKey, field, exceptions);
    };

    Model.prototype.updateRelations = function (extraUpdates, keyUpdates, m2mUpdates, thisKey) {
        var _this = this;
        var stack = [];

        if (extraUpdates) {
            _.forOwn(extraUpdates, function (update, table) {
                _.forOwn(update.removalExceptions, function (exceptions, field) {
                    if (update.model.relations[field].type == 'ForeignKey') {
                        update.model.clearForeignKeyRelations(thisKey, field, exceptions);
                    }
                    ;
                });

                stack = stack.concat(update.records.map(function (record) {
                    return update.model.save(record);
                }));
            });
        }

        if (keyUpdates) {
            _.forOwn(keyUpdates, function (update, table) {
                if (update.model.relations[update.name].type == 'ForeignKey') {
                    update.model.clearForeignKeyRelations(thisKey, update.name, update.ids);
                }
                ;

                stack = stack.concat(update.ids.map(function (ID) {
                    return update.model.updateForeignKeyRelation(ID, update.name, thisKey);
                }));
            });
        }

        if (m2mUpdates) {
            _.forOwn(m2mUpdates, function (update, table) {
                var relTable = _this.relations[update.name].relationTable;
                var p = _this.store.clearManyToManyRelations(relTable, thisKey, update.ownField);
                p = p.then(function () {
                    return _this.store.createManyToManyRelations(relTable, update.updates);
                });

                stack.push(p);
            });
        }

        return Promise.all(stack);
    };

    Model.prototype.save = function (record, opts) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            opts = opts || {};

            var extraUpdates = null;
            var keyUpdates = null;
            var m2mUpdates = null;
            var thisKey;

            var promise = _this.validate(record, opts);
            promise.then(function (validationResult) {
                if (validationResult.isValid || opts.ignoreValidation) {
                    return _this.reduceRecord(record);
                } else {
                    throw validationResult;
                }
            }).then(function (saveRecord) {
                thisKey = saveRecord[_this.primaryKey];

                // COLLECTING EXTRA UPDATES
                if (opts.updateRelatedRecords) {
                    extraUpdates = _this.collectRelatedRecordUpdates(record, thisKey);
                    m2mUpdates = _this.collectManyToManyUpdates(record, thisKey);
                }

                if (!opts.updateRelatedRecords && opts.updateRelations) {
                    keyUpdates = _this.collectForeignKeyUpdates(record, thisKey);
                    m2mUpdates = _this.collectManyToManyUpdates(record, thisKey);
                }

                return _this.store.save(_this, saveRecord, opts);
            }).then(function (saveResult) {
                if (extraUpdates || keyUpdates || m2mUpdates) {
                    _this.updateRelations(extraUpdates, keyUpdates, m2mUpdates, thisKey).then(function (relatedRecords) {
                        resolve(saveResult);
                    });
                } else {
                    resolve(saveResult);
                }
            }).catch(function (error) {
                console.error(error);
                reject(error);
            });
        });
    };

    Model.prototype.delete = function (record, opts) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            opts = opts || {};

            if (!record[_this.primaryKey]) {
                console.error("Record was not provided with a primary key", record);
                return reject("Record was not provided with a primary key");
            }

            var query = {};
            query[_this.primaryKey] = record[_this.primaryKey];
            _this.query().get(query).evaluate().then(function (trueRecord) {
                var stack = [];

                _.forOwn(_this.relations, function (rel, key) {
                    var relVal = trueRecord[key];
                    if (rel.type == 'OneToMany') {
                        if (rel.onDelete == 'cascade' && relVal != undefined) {
                            stack = stack.concat(relVal.map(function (relRecord) {
                                return rel.model.delete(relRecord);
                            }));
                        }

                        if (rel.onDelete == 'setNull' && relVal != undefined) {
                            stack = stack.concat(relVal.map(function (relRecord) {
                                relRecord[rel.relatedName] = undefined;
                                return rel.model.save(relRecord);
                            }));
                        }
                    }

                    if (rel.type == 'ManyToMany') {
                        throw new Error("ManyToMany Relations");
                    }
                });

                stack.push(_this.store.delete(_this, trueRecord[_this.primaryKey], {}));

                Promise.all(stack).then(function () {
                    return resolve(true);
                });
            });
        });
    };

    Model.prototype.query = function () {
        return new Queries.Query(this);
    };
    Model.prototype.all = function () {
        return this.query().all();
    };

    Model.prototype.get = function (args) {
        return this.query().get(args);
    };

    Model.prototype.filter = function (args) {
        return this.query().filter(args);
    };

    Model.prototype.exclude = function (args) {
        return this.query().exclude(args);
    };
    return Model;
})();

module.exports = Model;
