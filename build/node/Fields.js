///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Promise = require('bluebird');

var Queries = require('./Queries');
var _ = require('lodash');
var Validation = require('./Validation');

var Field = (function () {
    function Field(name, column) {
        var _this = this;
        this.name = name;
        _.forOwn(column, function (val, key) {
            if (key != 'type')
                _this[key] = val;
        });
        this.type = this['constructor']['type'];
    }
    Field.prototype.validate = function (value, opts) {
        var errors = [];

        if (this.required && (value == null || value == undefined)) {
            errors.push('Field requires a value.');
        }

        if (value != null && value != undefined) {
            this['constructor']['validators'].forEach(function (validator) {
                try  {
                    validator(value);
                } catch (errMessage) {
                    errors.push(errMessage);
                }
            });
        }
        ;
        return errors;
    };

    // creating lookups usually defaults to a single, but in the case of
    // relationships this ends up changing.
    Field.prototype.queryArgToLookups = function (key, val, model) {
        return [new Queries.FieldLookup(key, val, model)];
    };

    Field.validators = [];
    return Field;
})();
exports.Field = Field;

// PRIMITIVE TYPES
var IntField = (function (_super) {
    __extends(IntField, _super);
    function IntField() {
        _super.apply(this, arguments);
    }
    IntField.type = "Int";
    IntField.validators = [Validation.isNumber, Validation.isInteger];
    return IntField;
})(Field);
exports.IntField = IntField;

var FloatField = (function (_super) {
    __extends(FloatField, _super);
    function FloatField() {
        _super.apply(this, arguments);
    }
    FloatField.type = "Float";
    FloatField.validators = [Validation.isNumber];
    return FloatField;
})(Field);
exports.FloatField = FloatField;

var StringField = (function (_super) {
    __extends(StringField, _super);
    function StringField() {
        _super.apply(this, arguments);
    }
    StringField.type = "String";
    StringField.validators = [Validation.isString];
    return StringField;
})(Field);
exports.StringField = StringField;

var BooleanField = (function (_super) {
    __extends(BooleanField, _super);
    function BooleanField() {
        _super.apply(this, arguments);
    }
    BooleanField.type = "Boolean";
    BooleanField.validators = [Validation.isBoolean];
    return BooleanField;
})(Field);
exports.BooleanField = BooleanField;

// TEMPORAL TYPES
var DateTimeField = (function (_super) {
    __extends(DateTimeField, _super);
    function DateTimeField() {
        _super.apply(this, arguments);
    }
    DateTimeField.type = "DateTime";
    DateTimeField.validators = [Validation.isValidDateOrTime];
    return DateTimeField;
})(Field);
exports.DateTimeField = DateTimeField;

var DateField = (function (_super) {
    __extends(DateField, _super);
    function DateField() {
        _super.apply(this, arguments);
    }
    DateField.type = "Date";
    DateField.validators = [Validation.isValidDateOrTime];
    return DateField;
})(Field);
exports.DateField = DateField;

var TimeField = (function (_super) {
    __extends(TimeField, _super);
    function TimeField() {
        _super.apply(this, arguments);
    }
    TimeField.type = "Time";
    TimeField.validators = [Validation.isValidDateOrTime];
    return TimeField;
})(Field);
exports.TimeField = TimeField;

// COMPLEX TYPES
var JSONField = (function (_super) {
    __extends(JSONField, _super);
    function JSONField() {
        _super.apply(this, arguments);
    }
    JSONField.type = "JSON";
    return JSONField;
})(Field);
exports.JSONField = JSONField;

var RelationField = (function (_super) {
    __extends(RelationField, _super);
    function RelationField() {
        _super.apply(this, arguments);
    }
    return RelationField;
})(Field);
exports.RelationField = RelationField;

// RELATION TYPES
var ForeignKeyField = (function (_super) {
    __extends(ForeignKeyField, _super);
    function ForeignKeyField() {
        _super.apply(this, arguments);
    }
    ForeignKeyField.type = "ForeignKey";
    return ForeignKeyField;
})(RelationField);
exports.ForeignKeyField = ForeignKeyField;

var ManyToManyField = (function (_super) {
    __extends(ManyToManyField, _super);
    function ManyToManyField() {
        _super.apply(this, arguments);
    }
    ManyToManyField.type = "ManyToMany";
    return ManyToManyField;
})(RelationField);
exports.ManyToManyField = ManyToManyField;

var FieldRegistry = (function () {
    function FieldRegistry(registry) {
        if (typeof registry === "undefined") { registry = []; }
        this.registry = registry;
    }
    FieldRegistry.prototype.register = function (matcher, field) {
        this.registry.push({ matcher: matcher, field: field });
    };

    FieldRegistry.prototype.createField = function (name, column) {
        var field = _.find(this.registry, function (regEntry) {
            return column.type.match(regEntry.matcher);
        });

        if (field) {
            return new field.field(name, column);
        } else {
            throw "Field type " + column.type + " was not found";
        }
    };
    return FieldRegistry;
})();
exports.FieldRegistry = FieldRegistry;

exports.defaultFieldRegistry = new FieldRegistry([
    { matcher: /^(int|integer)/, field: IntField },
    { matcher: /^(float)/, field: FloatField },
    { matcher: /^(str|string|text)/, field: StringField },
    { matcher: /^(bool|boolean)/, field: BooleanField },
    { matcher: /^(json)/, field: JSONField },
    { matcher: /^(datetime)/, field: DateTimeField },
    { matcher: /^(time)/, field: TimeField },
    { matcher: /^(date)/, field: DateField },
    { matcher: /^(fk|foreignkey)/, field: ForeignKeyField },
    { matcher: /^(many2many|m2m|manytomany)/, field: ManyToManyField }
]);
