///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>
var _ = require('lodash');
var diff = require('deep-diff');

function validateTable(table) {
    _.forOwn(table, function (val, key) {
        exports.validateColumn(val);
    });
    return true;
}
exports.validateTable = validateTable;

function validateColumn(column) {
    if (!column.type)
        throw new Error("Column missing type - " + column);
    return true;
}
exports.validateColumn = validateColumn;

(function (MigrationActionTypes) {
    MigrationActionTypes.ADD_TABLE = "add_table";
    MigrationActionTypes.ADD_COLUMN = "add_column";
    MigrationActionTypes.REMOVE_TABLE = "remove_table";
    MigrationActionTypes.REMOVE_COLUMN = "remove_column";
    MigrationActionTypes.ALTER_COLUMN = "alter_column";
    MigrationActionTypes.CUSTOM = "custom";
})(exports.MigrationActionTypes || (exports.MigrationActionTypes = {}));
var MigrationActionTypes = exports.MigrationActionTypes;

var MigrationAction = (function () {
    function MigrationAction(type, args) {
        this.type = type;
        this.args = args;
    }
    MigrationAction.prototype.apply = function (schema) {
        switch (this.type) {
            case MigrationActionTypes.ADD_TABLE:
                schema[this.args.tableName] = {};
                break;

            case MigrationActionTypes.ADD_COLUMN:
                schema[this.args.tableName][this.args.columnName] = this.args.columnTraits;
                break;

            case MigrationActionTypes.ALTER_COLUMN:
                schema[this.args.tableName][this.args.columnName][this.args.columnTrait] = this.args.columnValue;
                break;

            case MigrationActionTypes.REMOVE_COLUMN:
                delete schema[this.args.tableName][this.args.columnName];
                break;

            case MigrationActionTypes.REMOVE_TABLE:
                delete schema[this.args.tableName];
                break;

            case MigrationActionTypes.CUSTOM:
                if (this.args.changeSchema)
                    this.args.changeSchema(schema);
                break;
        }
        return schema;
    };

    MigrationAction.prototype.toSuggestion = function () {
        return ('new JohoDB.MigrationAction("' + this.type + '", ' + JSON.stringify(this.args) + ')');
    };
    return MigrationAction;
})();
exports.MigrationAction = MigrationAction;

function cloneAndStripNonMigratoryTraits(schema) {
    schema = _.clone(schema);
    _.forOwn(schema, function (table, key) {
        _.forOwn(table, function (column, key) {
            if (column['validators'])
                delete column['validators'];
        });
    });

    return schema;
}
exports.cloneAndStripNonMigratoryTraits = cloneAndStripNonMigratoryTraits;

function compareSchemas(schema, migratedSchema) {
    schema = exports.cloneAndStripNonMigratoryTraits(schema);
    migratedSchema = exports.cloneAndStripNonMigratoryTraits(migratedSchema);
    var schemaDiff = diff(migratedSchema, schema);

    if (!schemaDiff)
        return true;

    var suggestedMigration = "";

    var actions = schemaDiff.reduce(function (acts, diffNode) {
        return acts.concat(exports.diffToActions(diffNode));
    }, []);

    var message = ("db.addMigration({ actions: [\n" + actions.map(function (a) {
        return '    ' + a.toSuggestion();
    }).join(',\n') + "\n]});");
    message = "UPDATED MIGRATION REQUIRED: \n\n" + message;
    console.warn(message);
    throw new Error(message);
}
exports.compareSchemas = compareSchemas;

function diffToActions(diff) {
    var actions = [];

    // diff is a new table
    if (diff.path.length == 1 && diff.kind == "N") {
        actions.push(new MigrationAction(MigrationActionTypes.ADD_TABLE, { tableName: diff.path[0] }));
        _.forOwn(diff.rhs, function (val, key) {
            actions.push(new MigrationAction(MigrationActionTypes.ADD_COLUMN, { tableName: diff.path[0], columnName: key, columnTraits: val }));
        });
        return actions;
    }

    // diff is a table that should be removed
    if (diff.path.length == 1 && diff.kind == "D") {
        actions.push(new MigrationAction(MigrationActionTypes.REMOVE_TABLE, { tableName: diff.path[0] }));
    }

    // diff is a new column
    if (diff.path.length == 2 && diff.kind == "N") {
        actions.push(new MigrationAction(MigrationActionTypes.ADD_COLUMN, {
            tableName: diff.path[0], columnName: diff.path[1],
            columnTraits: diff.rhs }));
        return actions;
    }

    // diff is a column that should be removed
    if (diff.path.length == 2 && diff.kind == "D") {
        actions.push(new MigrationAction(MigrationActionTypes.REMOVE_COLUMN, { tableName: diff.path[0], columnName: diff.path[1] }));
    }

    // diff is a column value that should be altered
    if (diff.path.length == 3 && diff.kind == "E") {
        actions.push(new MigrationAction(MigrationActionTypes.ALTER_COLUMN, {
            tableName: diff.path[0], columnName: diff.path[1],
            columnTrait: diff.path[2], columnValue: diff.rhs }));
    }

    return actions;
}
exports.diffToActions = diffToActions;
