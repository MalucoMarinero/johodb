///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>
var _ = require('lodash');

var Schema = require('./Schema');

var IndexedDBStorage = require('./storage/IndexedDB');
var WebSQLStorage = require('./storage/WebSQL');
var Fields = require('./Fields');
var Validation = require('./Validation');
var Model = require('./Model');

var JohoDB = (function () {
    function JohoDB(name) {
        this.name = name;
        this.schema = {};
        this.migrations = [];
    }
    JohoDB.prototype.addSchema = function (tableName, tableSchema) {
        if (Schema.validateTable(tableSchema)) {
            this.schema[tableName] = tableSchema;
        }
        ;
    };

    JohoDB.prototype.addMigration = function (migration) {
        this.migrations.push(migration);
    };

    JohoDB.prototype.init = function (opts) {
        var _this = this;
        if (typeof opts === "undefined") { opts = {}; }
        if (!opts.storageType)
            opts.storageType = this.detectStorageType();
        if (!opts.registry)
            opts.fieldRegistry = Fields.defaultFieldRegistry;
        this.fieldRegistry = opts.fieldRegistry;

        this.validateAndFreezeMigrationPath(opts.fieldRegistry);

        var setupOutput = this.setupModels(this.schema, opts.fieldRegistry);
        this.models = setupOutput.models;
        this.relationTables = setupOutput.relationTables;

        this.store = new opts.storageType(this.name);

        _.forOwn(this.models, function (model, name) {
            model.setStore(_this.store);
        });

        if (opts.dontLoadStore)
            return;
        return this.store.initDB({
            models: this.models,
            relationTables: this.relationTables
        }, this.migrations, opts.clobber || false);
    };

    JohoDB.prototype.setupModels = function (schema, registry) {
        var models = {};
        _.forOwn(schema, function (tableSchema, modelName) {
            models[modelName] = new Model(modelName, tableSchema, registry);
        });
        return this.setupRelations(models);
    };

    JohoDB.prototype.setupRelations = function (models) {
        var relationTables = {};
        var relations = [];

        // collect relations from models
        _.forOwn(models, function (model, modelName) {
            relations = relations.concat(model.getRelations());
        });

        // link primary keys
        relations.forEach(function (r) {
            if (r.from)
                r.from.primaryKey = models[r.from.modelName].primaryKey;
            if (r.to)
                r.to.primaryKey = models[r.to.modelName].primaryKey;
        });

        // assign relation managers
        relations.forEach(function (r) {
            if (r.requiresRelationTable) {
                relationTables[r.getTableName()] = r.buildRelationTable();
            }

            var managers = r.buildRelationManagers();
            var from = managers[0];
            var to = managers[1];
            if (r.requiresRelationTable) {
                from.relationTable = r.buildRelationTable();
                to.relationTable = r.buildRelationTable();
            }
            if (from) {
                models[from.ownModel].relations[from.name] = from;
                if (to)
                    models[from.ownModel].relations[from.name].model = models[to.ownModel];
            }
            if (to) {
                models[to.ownModel].relations[to.name] = to;
                if (from)
                    models[to.ownModel].relations[to.name].model = models[from.ownModel];
            }
        });

        return {
            models: models,
            relationTables: relationTables
        };
    };

    JohoDB.prototype.detectStorageType = function () {
        return IndexedDBStorage;
    };

    JohoDB.prototype.validateAndFreezeMigrationPath = function (registry) {
        var _this = this;
        var migrations = this.migrations;
        var migratedSchema = migrations.reduce(function (acc, migration) {
            var nextSchema = migration.actions.reduce(function (acc2, action) {
                return action.apply(acc2);
            }, acc);

            migration.frozenDatabaseModel = _this.setupModels(nextSchema, registry);
            return nextSchema;
        }, {});

        Schema.compareSchemas(this.schema, migratedSchema);
    };
    JohoDB.MigrationAction = Schema.MigrationAction;
    JohoDB.MigrationActionTypes = Schema.MigrationActionTypes;
    JohoDB.WebSQLStorage = WebSQLStorage;
    JohoDB.Validation = Validation;
    JohoDB.IndexedDBStorage = IndexedDBStorage;
    JohoDB.defaultFieldRegistry = Fields.defaultFieldRegistry;
    return JohoDB;
})();

if (window)
    window['JohoDB'] = JohoDB;
module.exports = JohoDB;
