///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>

var _ = require('lodash');
var Promise: Bluebird.PromiseStatic = require('bluebird');
import Schema = require('./Schema');
import Fields = require('./Fields');
import Model = require('./Model');


export class Relation {
    public type: string;
    public from: {
        modelName: string;
        fieldName: string;
        primaryKey?: string;
    };
    public to: {
        modelName: string;
        fieldName: string;
        primaryKey?: string;
    };
    public onDelete: string;


    constructor(
        fromModelName: string, fromField: Fields.RelationField, noCheck?: boolean
    ) {
        if (!noCheck) {
            switch (fromField.type) {
            case 'ForeignKey':
                return new ForeignKeyRelation(fromModelName, fromField, true);
            case 'ManyToMany':
                return new ManyToManyRelation(fromModelName, fromField, true);
            }
        }
        this.type = fromField.type;
        this.from = {
            modelName: fromModelName,
            fieldName: fromField.name,
        }

        if (fromField.type != 'GenericKey') {
            this.to = {
                modelName: fromField.relation,
                fieldName: fromField.relatedName,
            }
        }

        this.onDelete = fromField.onDelete ? fromField.onDelete : 'cascade';
    }

    get requiresRelationTable(): boolean {
        return this['constructor']['requiresRelationTable'];
    }

    buildRelationManagers(): ModelRelationManager[] {
        throw new Error('buildRelationManagers not implemented')
    }

    getTableName(): string {
        throw new Error('getTableName not implemented')
    }

    buildRelationTable(): RelationTable {
        throw new Error('buildRelationTable not implemented')
    }
}

export class RelationTable {
    public fields: {[key: string]: Fields.Field}
    public primaryKey: string;
        
    constructor(
        public name: string, from: any, to: any
    ) {
        this.fields = {};
        this.fields['id'] = new Fields.StringField("id", {
            type: "string", primaryKey: true
        });

        this.primaryKey = "id";

        var fromName = from.modelName + '_' + from.primaryKey;
        this.fields[fromName] = new Fields.StringField(fromName, {
            type: "string", required: true
        });

        var toName = to.modelName + '_' + to.primaryKey;
        this.fields[toName] = new Fields.StringField(toName, {
            type: "string", required: true
        });
    }
}


export class ForeignKeyRelation extends Relation {
    static requiresRelationTable = false;

    buildRelationManagers(): ModelRelationManager[] {
        return [
            new ForeignKeyRelationManager(
                this.from.fieldName, this.from.primaryKey, this.from.modelName,
                this.to.fieldName, this.to.primaryKey, this.to.modelName
            ),
            new OneToManyRelationManager(
                this.to.fieldName, this.to.primaryKey, this.to.modelName,
                this.from.fieldName, this.from.primaryKey, this.from.modelName,
                this.onDelete
            ),
        ];
    }
}


export class ManyToManyRelation extends Relation {
    static requiresRelationTable = true;

    buildRelationManagers(): ModelRelationManager[] {
        return [
            new ManyToManyRelationManager(
                this.from.fieldName, this.from.primaryKey, this.from.modelName,
                this.to.fieldName, this.to.primaryKey, this.to.modelName,
                this.onDelete, this.getTableName()
            ),
            new ManyToManyRelationManager(
                this.to.fieldName, this.to.primaryKey, this.to.modelName,
                this.from.fieldName, this.from.primaryKey, this.from.modelName,
                this.onDelete, this.getTableName()
            ),
        ];
    }

    getTableName(): string {
        return this.from.modelName + '_' + this.from.fieldName + '__' +
               this.to.modelName + '_' + this.to.fieldName;
    }

    buildRelationTable(): RelationTable {
        return new RelationTable(this.getTableName(), this.from, this.to);
    }
}


export class ModelRelationManager {
    public model: Model;
    public type: string;
    public relationTable: RelationTable;

    constructor(
        public name: string,
        public ownKey: string,
        public ownModel: string,
        public relatedName: string,
        public relatedKey: string,
        public relatedModel: string,
        public onDelete?: string,
        public relationTableName?: string
    ) {
        this.type = this.getRelationType();
    }

    getRelationType(): string { return "None" }

    validate(value, opts): string[] {
        throw new Error('Validate not implemented');
    }

    reduce(value) {
        throw new Error('Reduce Relation not implemented');
    }

    collectRelatedRecords(value, reversePk) {
        throw new Error('Collect Related Records not implemented');
    }

    getOwnKey(): string { return this.ownModel + '_' + this.name; }
    getRelKey(): string { return this.relatedModel + '_' + this.relatedName; }
}

export class ForeignKeyRelationManager extends ModelRelationManager {
    getRelationType(): string { return "ForeignKey" }

    validate(value, opts): string[] {
        var errors = [];

        if (_.isObject(value)) {
            if (_.isUndefined(value[this.relatedKey])) {
                errors.push(
                    "No Primary Key provided for relation '" + this.name + "'"
                );
            }
        }
        if (_.isArray(value)) {
            errors.push(
                "Foreign Key '" + this.name + "' doesn't support arrays."
            );
        }
        return errors;

    }

    reduce(value) {
        return _.isObject(value) ? value[this.relatedKey] : value;
    }

    collectRelatedRecords(value, reversePk) {
        return _.isObject(value)
            ? [value]
            : [];
    }
}

export class OneToManyRelationManager extends ModelRelationManager {
    getRelationType(): string { return "OneToMany" }
    reduce(value) { return undefined; }

    validate(value, opts): string[] {
        var errors = [];

        if (_.isArray(value)) {
            value.forEach((rel) => {
                if (_.isUndefined(rel[this.relatedKey]) &&
                    (opts.updateRelatedRecords || opts.updateRelationships)) {
                    errors.push("No primary key provided for relation in '" +
                                this.name +
                                "', yet it's meant to be updated/saved."); }
            });
        };

        return errors;
    }

    collectRelatedRecords(value, reversePk) {
        return _.isArray(value)
            ? value.map((val) => {
                val[this.relatedName] = reversePk;
                return val;
            })
            : [];
    }

    collectKeys(value, reversePk) {
        return _.isArray(value)
            ? value.map((val) => val[this.model.primaryKey])
            : [];
    }
}


export class ManyToManyRelationManager extends ModelRelationManager {
    getRelationType(): string { return "ManyToMany" }
    reduce(value) { return undefined; }


    validate(value, opts): string[] {
        var errors = [];

        if (_.isArray(value)) {
            value.forEach((rel) => {
                if (
                    _.isObject(rel) && _.isUndefined(rel[this.relatedKey]) &&
                    opts.updateRelationships
                ) { errors.push(
                    "No primary key provided for relation '" + this.name + "', yet it's meant to be updated/saved."
                ); }
            });
        };

        return errors;
    }

    collectRelatedRecords(value, reversePk) {
        return _.isArray(value)
            ? value
            : [];
    }

    collectKeys(value, reversePk) {
        return _.isArray(value)
            ? value.map((val) => {
                var relRecord = {};
                relRecord[this.getOwnKey()] = reversePk;
                if (_.isObject(val)) {
                    relRecord[this.getRelKey()] = val[this.model.primaryKey];
                } else {
                    relRecord[this.getRelKey()] = val;
                }
                return relRecord;
            })
            : [];
    }
}

