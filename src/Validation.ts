///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/moment.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>

var _ = require('lodash');
var moment = require('moment');
var Promise: Bluebird.PromiseStatic = require('bluebird');
import Schema = require('./Schema');

export interface ValidationResult {
    isValid: boolean;
    errorCount: number;
    validating: any;
    errors: any;
}

export function isString(value: any) {
    if (!_.isString(value)) {
        throw 'Value is not a valid string.'
    };
}

export function isInteger(value: any) {
    if (value !== +value && value !== (value|0)) {
        throw 'Value is not an integer.'
    };
}


export function isNumber(value: any) {
    if (!_.isNumber(value) || _.isNaN(value)) {
        throw 'Value is not a valid number.'
    };
}

export function isBoolean(value: any) {
    if (!_.isBoolean(value)) {
        throw 'Value is not a boolean.'
    };
}

export function isValidDateOrTime(value: any) {
    if (!moment(value).isValid()) {
        throw 'Value is not a valid date or time.'
    }
}

var emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|ca|info|mobi|name|aero|asia|jobs|museum)\b/;

export function isEmail(value: any) {
    if (!emailRegex.test(value)) {
        throw "Email address is not valid.";
    }
}
