///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>

var _ = require('lodash');
var Promise: Bluebird.PromiseStatic = require('bluebird');
import Schema = require('./Schema');
import BaseStorage = require('./storage/BaseStorage');
import Fields = require('./Fields');
import Relations = require('./Relations');
import Validation = require('./Validation');
import Queries = require('./Queries');

class Model {
    public fields: {[key:string]: Fields.Field};
    public store: BaseStorage;
    public primaryKey: string;
    public relations: {[key:string]: Relations.ModelRelationManager};

    constructor(
        public name: string,
        tableSchema: Schema.Table,
        fieldRegistry: Fields.FieldRegistry
    ) {
        this.fields = {};
        this.relations = {};
        _.forOwn(tableSchema, (columnSchema, fieldName) => {
            this.fields[fieldName] = fieldRegistry.createField(
                fieldName, columnSchema
            );
        });

        this.primaryKey = this.getPrimaryKey().name;
    }

    setStore(store: BaseStorage) {
        this.store = store;
    }

    getPrimaryKey(): Fields.Field {
        var field = _.find(this.fields, (field) => field.primaryKey);
        if (field) return field;
        throw new Error("No Primary Key found");
    }

    getRelations(): Relations.Relation[] {
        var relations = [];
        _.forOwn(this.fields, (field: Fields.RelationField, fieldName) => {
            if (field.type.match(/ForeignKey|ManyToMany/)) {
                relations.push(new Relations.Relation(this.name, field));
            };
        });
        return relations;
    }

    validate(record: any, opts?: any): Bluebird.Promise {
        return new Promise((resolve, reject) => {

        var result = {
            isValid: true, errorCount: 0, validating: record, errors: {}
        };

        var validateOnFieldOrRelation = (field, key) => {
            var errors = field.validate(record[key], opts);
            if (errors.length > 0) {
                result.errorCount += errors.length;
                result.errors[key] = errors;
            };
        };

        _.forOwn(this.fields, validateOnFieldOrRelation);
        _.forOwn(this.relations, validateOnFieldOrRelation);

        if (result.errorCount > 0) result.isValid = false;
        resolve(result);

        });
    }

    private reduceRecord(record: any): any {
        var saveRecord = {};
        _.forOwn(this.fields, (field, key) => {
            if (field.defaultValue && _.isUndefined(record[key])) {
                saveRecord[key] = field.defaultValue;
            } else {
                saveRecord[key] = record[key];
            }
        });

        _.forOwn(this.relations, (relation, key) => {
            if (relation.type == 'ForeignKey') {
                saveRecord[key] = relation.reduce(record[key])
            } else if (relation.type == 'ManyToMany') {
                delete saveRecord[key];
            } else if (relation.type == 'OneToMany') {
                delete saveRecord[key];
            }
        });

        return saveRecord;
    }

    private collectRelatedRecordUpdates(record: any, thisKey: string): any {
        var updates = {};

        _.forOwn(this.relations, (r, key) => {
            var records = r.collectRelatedRecords(record[key], thisKey);
            var keys = r.type != 'ForeignKey'
                ? r.collectKeys(record[key])
                : [];

            updates[r.relatedModel] = {
                'model': r.model,
                'records': records,
                'removalExceptions': {}
            }
            updates[r.relatedModel]['removalExceptions'][r.relatedName] = keys;
        });

        return updates;
    }

    private collectForeignKeyUpdates(record: any, thisKey: string): any {
        var updates = {};
        _.forOwn(this.relations, (r, key) => {
            updates[r.name] = {model: r.model, name: r.relatedName, ids: []};

            if (r.type == 'OneToMany') {
                updates[r.name].ids = r.collectKeys(record[key])
            }
        });

        return updates;
    }

    private collectManyToManyUpdates(record: any, thisKey: string): any {
        var updates = {};
        _.forOwn(this.relations, (r, key) => {
            if (r.type == "ManyToMany") {
                updates[r.relationTableName] = {
                    name: r.name,
                    model: r.model,
                    updates: r.collectKeys(record[key], thisKey),
                    ownField: r.getOwnKey()
                };
            }
        });

        return updates;
    }

    updateForeignKeyRelation(
        recordKey: string, field: string, reverseKey: string
    ): Bluebird.Promise {
        return this.store.updateForeignKey(
            this, recordKey, field, reverseKey
        );
    }

    clearForeignKeyRelations(
        recordKey: string, field: string, exceptions: string[]
    ): Bluebird.Promise {
        return this.store.clearForeignKeys(
            this, recordKey, field, exceptions
        );
    }

    private updateRelations(
        extraUpdates: any, keyUpdates: any, m2mUpdates: any, thisKey: string
    ): Bluebird.Promise {
        var stack = [];

        if (extraUpdates) {
            _.forOwn(extraUpdates, (update, table) => {
                _.forOwn(update.removalExceptions, (exceptions, field) => {
                    if (update.model.relations[field].type == 'ForeignKey') {
                        update.model.clearForeignKeyRelations(
                            thisKey, field, exceptions
                        );
                    };
                });

                stack = stack.concat(update.records.map((record) => {
                    return update.model.save(record);
                }));
            });
        }

        if (keyUpdates) {
            _.forOwn(keyUpdates, (update, table) => {
                if (update.model.relations[update.name].type == 'ForeignKey') {
                    update.model.clearForeignKeyRelations(
                        thisKey, update.name, update.ids
                    );
                };

                stack = stack.concat(update.ids.map((ID) => {
                    return update.model.updateForeignKeyRelation(
                        ID, update.name, thisKey
                    );
                }));
            });
        }

        if (m2mUpdates) {
            _.forOwn(m2mUpdates, (update, table) => {
                var relTable = this.relations[update.name].relationTable;
                var p = this.store.clearManyToManyRelations(
                    relTable, thisKey, update.ownField
                );
                p = p.then(() => {
                    return this.store.createManyToManyRelations(
                        relTable, update.updates
                    )
                });

                stack.push(p);
            });
        }

        return Promise.all(stack);
    }

    save(record: any, opts?: any): Bluebird.Promise {
        return new Promise((resolve, reject) => {

        opts = opts || {};

        var extraUpdates = null;
        var keyUpdates = null;
        var m2mUpdates = null;
        var thisKey;

        var promise = this.validate(record, opts)
        promise.then((validationResult) => {
            if (validationResult.isValid || opts.ignoreValidation) {
                return this.reduceRecord(record);
            } else {
                throw validationResult;
            }
        }).then((saveRecord) => {
            thisKey = saveRecord[this.primaryKey];

            // COLLECTING EXTRA UPDATES
            if (opts.updateRelatedRecords) {
                extraUpdates = this.collectRelatedRecordUpdates(record, thisKey);
                m2mUpdates = this.collectManyToManyUpdates(record, thisKey);
            }

            if (!opts.updateRelatedRecords && opts.updateRelations) {
                keyUpdates = this.collectForeignKeyUpdates(record, thisKey);
                m2mUpdates = this.collectManyToManyUpdates(record, thisKey);
            }

            return this.store.save(this, saveRecord, opts);
        }).then((saveResult) => {
            if (extraUpdates || keyUpdates || m2mUpdates) {
                this.updateRelations(
                    extraUpdates, keyUpdates, m2mUpdates, thisKey
                ).then((relatedRecords) => {
                    resolve(saveResult);
                });
            } else {
                resolve(saveResult);
            }
        }).catch((error) => {
            console.error(error);
            reject(error);
        });

        });
    }

    delete(record: any, opts?: any): Bluebird.Promise {
        return new Promise((resolve, reject) => {

        opts = opts || {};
        
        if (!record[this.primaryKey]) {
            console.error("Record was not provided with a primary key", record)
            return reject("Record was not provided with a primary key");
        }

        var query: any = {};
        query[this.primaryKey] = record[this.primaryKey];
        this.query().get(query).evaluate().then((trueRecord) => {
            var stack = [];

            _.forOwn(this.relations, (rel, key) => {
                var relVal = trueRecord[key];
                if (rel.type == 'OneToMany') {
                    if (rel.onDelete == 'cascade' && relVal != undefined) {
                        stack = stack.concat(relVal.map((relRecord) => {
                            return rel.model.delete(relRecord);
                        }));
                    }

                    if (rel.onDelete == 'setNull' && relVal != undefined) {
                        stack = stack.concat(relVal.map((relRecord) => {
                            relRecord[rel.relatedName] = undefined;
                            return rel.model.save(relRecord);
                        }));
                    }
                }

                if (rel.type == 'ManyToMany') {
                    throw new Error("ManyToMany Relations");
                }
            });

            stack.push(this.store.delete(this, trueRecord[this.primaryKey], {}));

            Promise.all(stack).then(() => resolve(true));
        });

        });
    }

    query(): Queries.Query { return new Queries.Query(this); }
    all(): Queries.Query {
        return this.query().all();
    }

    get(args: {[key: string]: any}): Queries.Query {
        return this.query().get(args);
    }

    filter(args: {[key: string]: any}): Queries.Query {
        return this.query().filter(args);
    }

    exclude(args: {[key: string]: any}): Queries.Query {
        return this.query().exclude(args);
    }

}

export = Model;
