///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>

var Promise: Bluebird.PromiseStatic = require('bluebird');
import Schema = require('./Schema');
import Model = require('./Model');
import Queries = require('./Queries');
var _ = require('lodash');
import Validation = require('./Validation');


export class Field {
    public type: string;
    public primaryKey: boolean;
    public required: boolean;
    public unique: boolean;
    public defaultValue: any;
    public relation: string;
    public relatedName: string;
    public onDelete: string;

    constructor(public name: string, column: Schema.Column) {
        _.forOwn(column, (val, key) => {
            if (key != 'type') this[key] = val;
        });
        this.type = this['constructor']['type'];
    }

    validate(value: any, opts?: any) {
        var errors = [];

        if (this.required && (value == null || value == undefined)) {
            errors.push('Field requires a value.')
        }

        if (value != null && value != undefined) {
            this['constructor']['validators'].forEach((validator) => {
                try {
                    validator(value);
                } catch (errMessage) {
                    errors.push(errMessage);
                }
            });
        };
        return errors;
    }

    // creating lookups usually defaults to a single, but in the case of
    // relationships this ends up changing.
    queryArgToLookups(
        key: string, val: any, model: Model
    ): Queries.FieldLookup[] {
        return [new Queries.FieldLookup(key, val, model)];
    }

    static validators = [];
}

// PRIMITIVE TYPES
export class IntField extends Field {
    static type = "Int";
    static validators = [Validation.isNumber, Validation.isInteger];
}

export class FloatField extends Field {
    static type = "Float";
    static validators = [Validation.isNumber];
}

export class StringField extends Field {
    static type = "String";
    static validators = [Validation.isString];
}

export class BooleanField extends Field {
    static type = "Boolean";
    static validators = [Validation.isBoolean];
}

// TEMPORAL TYPES
export class DateTimeField extends Field {
    static type = "DateTime";
    static validators = [Validation.isValidDateOrTime];
}

export class DateField extends Field {
    static type = "Date";
    static validators = [Validation.isValidDateOrTime];
}

export class TimeField extends Field {
    static type = "Time";
    static validators = [Validation.isValidDateOrTime];
}


// COMPLEX TYPES
export class JSONField extends Field {
    static type = "JSON";
}


export class RelationField extends Field {
    public relation: string;
    public relatedName: string;
    public onDelete: string;
}

// RELATION TYPES
export class ForeignKeyField extends RelationField {
    static type = "ForeignKey";
}

export class ManyToManyField extends RelationField {
    static type = "ManyToMany";
}



export interface FieldRegistryEntry {
    matcher: RegExp;
    field: typeof Field;
}

export class FieldRegistry {
    constructor(public registry: FieldRegistryEntry[] = []) {
    }

    register(matcher: RegExp, field: typeof Field) {
        this.registry.push({matcher: matcher, field: field});
    }

    createField(name: string, column: Schema.Column) {
        var field = _.find(this.registry, (regEntry) => {
            return column.type.match(regEntry.matcher)
        });

        if (field) {
            return new field.field(name, column);
        } else {
            throw "Field type " + column.type + " was not found";
        }
    }
}


export var defaultFieldRegistry = new FieldRegistry([
    {matcher: /^(int|integer)/, field: IntField},
    {matcher: /^(float)/, field: FloatField},
    {matcher: /^(str|string|text)/, field: StringField},
    {matcher: /^(bool|boolean)/, field: BooleanField},
    {matcher: /^(json)/, field: JSONField},
    {matcher: /^(datetime)/, field: DateTimeField},
    {matcher: /^(time)/, field: TimeField},
    {matcher: /^(date)/, field: DateField},
    {matcher: /^(fk|foreignkey)/, field: ForeignKeyField},
    {matcher: /^(many2many|m2m|manytomany)/, field: ManyToManyField},
]);
