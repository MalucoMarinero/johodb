///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>
var _ = require('lodash');

import Schema = require('./Schema');
import BaseStorage = require('./storage/BaseStorage');
import IndexedDBStorage = require('./storage/IndexedDB');
import WebSQLStorage = require('./storage/WebSQL');
import Fields = require('./Fields');
import Validation = require('./Validation');
import Model = require('./Model');

interface DatabaseModelling {
    models: {[key:string]: Model};
    relationTables: any;
}

interface InitOptions {
    storageType?: typeof BaseStorage;
    fieldRegistry?: Fields.FieldRegistry;
    clobber?: boolean;
    dontLoadStore?: boolean;
}

class JohoDB {
    static MigrationAction = Schema.MigrationAction;
    static MigrationActionTypes = Schema.MigrationActionTypes;
    static WebSQLStorage = WebSQLStorage;
    static Validation = Validation;
    static IndexedDBStorage = IndexedDBStorage;
    static defaultFieldRegistry = Fields.defaultFieldRegistry;
    private schema: Schema.Schema;
    private migrations: Schema.Migration[];
    public fieldRegistry: Fields.FieldRegistry;
    public store: BaseStorage;
    public models: {[key:string]: Model};
    public relationTables: {[key: string]: any};

    constructor(public name: string) {
        this.schema = {};
        this.migrations = [];
    }

    addSchema(tableName: string, tableSchema: Schema.Table) {
        if (Schema.validateTable(tableSchema)) {
            this.schema[tableName] = tableSchema;
        };
    }

    addMigration(migration: Schema.Migration) {
        this.migrations.push(migration);
    }

    init(opts: any = {}): Bluebird.Promise {
        if (!opts.storageType) opts.storageType = this.detectStorageType();
        if (!opts.registry) opts.fieldRegistry = Fields.defaultFieldRegistry;
        this.fieldRegistry = opts.fieldRegistry;

        this.validateAndFreezeMigrationPath(opts.fieldRegistry);

        var setupOutput = this.setupModels(this.schema, opts.fieldRegistry);
        this.models = setupOutput.models
        this.relationTables = setupOutput.relationTables

        this.store = new opts.storageType(this.name);

        _.forOwn(this.models, (model, name) => {
            model.setStore(this.store);
        });

        if (opts.dontLoadStore) return; 
        return this.store.initDB({
            models: this.models,
            relationTables: this.relationTables
        }, this.migrations, opts.clobber || false)
    }


    private setupModels(
        schema: Schema.Schema, registry: Fields.FieldRegistry
    ): DatabaseModelling {
        var models: {[key:string]: Model} = {};
        _.forOwn(schema, (tableSchema, modelName) => {
            models[modelName] = new Model(modelName, tableSchema, registry);
        });
        return this.setupRelations(models);
    }

    private setupRelations(
        models: {[key: string]: Model}
    ): DatabaseModelling {
        var relationTables = {};
        var relations = [];

        // collect relations from models
        _.forOwn(models, (model, modelName) => {
            relations = relations.concat(model.getRelations());
        });

        // link primary keys
        relations.forEach((r) => {
            if (r.from) r.from.primaryKey = models[r.from.modelName].primaryKey;
            if (r.to) r.to.primaryKey = models[r.to.modelName].primaryKey;
        });

        // assign relation managers
        relations.forEach((r) => {
            if (r.requiresRelationTable) {
                relationTables[r.getTableName()] = r.buildRelationTable();
            }

            var managers = r.buildRelationManagers();
            var from = managers[0];
            var to = managers[1];
            if (r.requiresRelationTable) {
                from.relationTable = r.buildRelationTable();
                to.relationTable = r.buildRelationTable();
            }
            if (from) {
                models[from.ownModel].relations[from.name] = from;
                if (to) models[from.ownModel].relations[from.name].model = models[to.ownModel];
            }
            if (to) {
                models[to.ownModel].relations[to.name] = to;
                if (from) models[to.ownModel].relations[to.name].model = models[from.ownModel];
            }
        });

        return {
            models: models,
            relationTables: relationTables
        };

    }


    detectStorageType(): typeof BaseStorage {
        return IndexedDBStorage;
    }
    
    validateAndFreezeMigrationPath(registry: Fields.FieldRegistry) {
        var migrations = this.migrations;
        var migratedSchema = migrations.reduce((acc: Schema.Schema, migration) => {
            var nextSchema = migration.actions.reduce((acc2: Schema.Schema, action) => {
                return action.apply(acc2);
            }, acc);

            migration.frozenDatabaseModel = this.setupModels(nextSchema,
                                                             registry);
            return nextSchema;
        }, {});

        Schema.compareSchemas(this.schema, migratedSchema);
    }
}

if (window) window['JohoDB'] = JohoDB;
export = JohoDB;
