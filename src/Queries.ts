///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>

var _ = require('lodash');
var Promise: Bluebird.PromiseStatic = require('bluebird');
import Schema = require('./Schema');
import BaseStorage = require('./storage/BaseStorage');
import Fields = require('./Fields');
import Relations = require('./Relations');
import Validation = require('./Validation');
import Model = require('./Model');

export enum QueryStepType {
    ALL, GET, FILTER, EXCLUDE
}

export interface QueryStep {
    type: QueryStepType;
    lookups: FieldLookup[];
}

export class Query {
    constructor(public model: Model, public steps: QueryStep[] = []) {
    }

    private clone(): Query {
        return new Query(this.model, _.clone(this.steps));
    }

    private buildLookups(args: {[key: string]: any}): FieldLookup[] {
        var lookups = [];
        _.forOwn(args, (val, key) => {
            var field = key.split('__')[0];
            if (this.model.fields[field]) {
                lookups = lookups.concat(
                    this.model.fields[field].queryArgToLookups(key, val, this.model)
                );
            } else {
                lookups.push(new FieldLookup(key, val, this.model));
            }
        });

        return lookups;
    }

    private addStep(stepType: QueryStepType, args: {[key: string]: any}): Query {
        if (_.keys(args).length < 1 && stepType != QueryStepType.ALL) {
            throw "No arguments provided for " + QueryStepType[stepType];
        }

        var newStep: QueryStep = {
            type: stepType,
            lookups: this.buildLookups(args)
        }
        this.steps.push(newStep);
        return this;
    }

    hasStep(stepType: QueryStepType): boolean {
        return this.steps.some((step) => step.type == stepType);
    }

    getStep(stepType: QueryStepType): QueryStep {
        return this.steps.filter((step) => step.type == stepType)[0];
    }

    all(): Query {
        var newQuery = this.clone();
        newQuery.addStep(
            QueryStepType.ALL, {}
        );
        return newQuery;
    }

    get(args: {[key: string]: any}): Query {
        return this.clone().addStep(QueryStepType.GET, args);
    }

    filter(args: {[key: string]: any}): Query {
        return this.clone().addStep(QueryStepType.FILTER, args);
    }

    exclude(args: {[key: string]: any}): Query {
        return this.clone().addStep(QueryStepType.EXCLUDE, args);
    }

    evaluate(opts?: any): Bluebird.Promise {
        if (!opts) opts = {};
        if (opts.depth == undefined) opts.depth = 1;
        if (opts.only) {
          var max = Math.max.apply(null, opts.only.map(
            (n) => n.split("__").length)
          );
          if (max > opts.depth) {
            opts.depth = max;
          }
        }

        return new Promise((resolve, reject) => {

        var p = this.model.store.evaluateQuery(this.model, this, opts);
        p = p.then((result) => {
            if (opts.depth > 0 && result) {
                if (_.isArray(result)) {
                    var pStack = result.map((record) => {
                        return this.getRelatedRecords(record, opts);
                    });

                    var p2 = Promise.all(pStack);
                } else {
                    var p2 = this.getRelatedRecords(result, opts);
                }
                p2.then((relatedRecords) => {
                    this.zipRelatedRecords(result, relatedRecords);
                    resolve(result);
                });
                return p2;
            } else {
                resolve(result);
            }
        });

        p.catch((error) => { reject(error); });

        });
    }

    private reduceRelationOptions(name: string, opts: any): any {
        var newOpts: any = {};
        if (opts.only) {
            newOpts.only = opts.only
                .filter((n) => n.indexOf(name) == 0)
                .map((n) => n.split("__").slice(1).join("__"));
        };
        newOpts.depth = opts.depth - 1;
        return newOpts;
    }

    private getRelatedRecords(record: any, oldOpts: any): Bluebird.Promise {
        var stack = [];

        _.forOwn(this.model.relations, (relation, name) => {
            var opts = this.reduceRelationOptions(name, oldOpts);

            if (oldOpts.only) {
                var firsts = oldOpts.only.map((n) => n.split("__")[0]);
                if (firsts.indexOf(name) == -1) {
                    stack.push(Promise.resolve(undefined));
                    return;
                }
            }

            if (relation.type == 'ForeignKey' && record[name] != undefined) {
                var query = {};
                query[relation.relatedKey] = record[name];
                stack.push(relation.model.query().get(query).evaluate(opts));
            } else if (relation.type == 'OneToMany') {
                var query = {};
                query[relation.relatedName] = record[this.model.primaryKey];
                stack.push(relation.model.query().filter(query).evaluate(opts));
            } else if (relation.type == 'ManyToMany') {
                stack.push(this.model.store.collectManyToManyRelations(
                    relation, record[this.model.primaryKey], opts
                ));
            } else {
                stack.push(Promise.resolve(undefined));
            }
        });

        return Promise.all(stack);
    }

    private zipRelatedRecords(result: any, relatedRecords: any) {
        if (_.isArray(result)) {
            result.forEach((record, ix) => {
                this.zipRelatedRecord(record, relatedRecords[ix])
            });
        } else {
            this.zipRelatedRecord(result, relatedRecords);
        }
    }

    private zipRelatedRecord(record: any, relatedRecords: any) {
        var ix = 0;
        _.forOwn(this.model.relations, (relation, name) => {
            record[name] = relatedRecords[ix];
            ix += 1;
        });
    }


}

export enum LookupType {
    EXACT, GT, GTE, LT, LTE, IN
}

export var LookupTypeMap = {
    "exact": LookupType.EXACT,
    "gt": LookupType.GT,
    "gte": LookupType.GTE,
    "lt": LookupType.LT,
    "lte": LookupType.LTE,
    "in": LookupType.IN,
}

export class FieldLookup {
    public type: LookupType;
    public fields: string[];

    constructor(name: string, public value: any, public model: Model) {
        var terms = name.split('__');
        this.type = !!LookupTypeMap[terms[terms.length-1]]
            ? LookupTypeMap[terms.pop()]
            : LookupType.EXACT;
        this.fields = terms;
    }
}
