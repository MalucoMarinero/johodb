///<reference path="../../typedefs/node.d.ts"/>
///<reference path="../../typedefs/bluebird.d.ts"/>
///<reference path="../../typedefs/moment.d.ts"/>
///<reference path="../../typedefs/lodash.d.ts"/>

var Promise: Bluebird.PromiseStatic = require('bluebird');

import Schema = require('../Schema');
import Fields = require('../Fields');
var _ = require('lodash');
var moment = require('moment');

export class SQLMapping {
    constructor(
        public tableName: string,
        public name: string,
        public field: Fields.Field
    ) { }
    createsValue() { return true; }
    createsTable() { return false; }
    changesRecord() { return false; }

    getConstraints() {
        if (this.field.primaryKey) return " PRIMARY KEY";
        if (this.field.unique) return " UNIQUE";
        return "";
    }

    makeSQLField() {
        return this.name + " TEXT" + this.getConstraints();
    }

    createSQLTable(idb: any): any {
        throw new Error("createSQLTable not implemented");
    }

    deleteSQLTable(idb: any): any {
        throw new Error("deleteSQLTable not implemented");
    }

    toSQLValue(value: any): any {
        return _.isUndefined(value) ? null : value;
    }
    fromSQLValue(value: any): any {
        return _.isNull(value) ? undefined : value;
    }

    toSQLRecord(record: any, sqlRecord: any): any {
        throw new Error("changes record is true, yet toSQLRecord not implemented");
    }
    fromSQLRecord(sqlRecord: any, record: any): any {
        throw new Error("changes record is true, yet fromSQLRecord not implemented");
    }
}

export class StringSQLMapping extends SQLMapping {
    makeSQLField() { return this.name + " TEXT" + this.getConstraints(); }
}

export class IntSQLMapping extends SQLMapping {
    makeSQLField() { return this.name + " INT" + this.getConstraints(); }
}

export class FloatSQLMapping extends SQLMapping {
    makeSQLField() { return this.name + " FLOAT" + this.getConstraints(); }
}

export class ForeignKeySQLMapping extends SQLMapping {
    makeSQLField() { return this.name + " TEXT" + this.getConstraints(); }
}

export class ManyToManySQLMapping extends SQLMapping {
    createsTable() { return true; }
    changesRecord() { return true; }

    mappingTableName(): string {
        return this.tableName + '_' + this.field.name + '__' +
               this.field.relation + '_' + this.field.relatedName;
    }

    createSQLTable(tx: any) {
        var fromName = this.tableName + '_' + this.field.name;
        var toName = this.field.relation + '_' + this.field.relatedName;
        var fieldSql = [
            "id TEXT PRIMARY KEY",
            fromName + " TEXT", toName + " TEXT"
        ];

        var sql = "CREATE TABLE IF NOT EXISTS " + fromName + '__' + toName;
        sql += " (" + fieldSql.join(', ') +");";
        tx.executeSql(sql, null);
    }

    toSQLRecord(record: any, sqlRecord: any): any {
        delete sqlRecord[this.field.name];
        return sqlRecord;
    }
    fromSQLRecord(sqlRecord: any, record: any): any {
        return record;
    }
}

var dateReviver = function(key, value) {
  if (typeof value == 'string') {
    var a = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
    if (a) return Date.parse(value);
  };
  return value;
};

export class JSONSQLMapping extends SQLMapping {
    makeSQLField() { return this.name + " TEXT" + this.getConstraints(); }

    toSQLValue(value: any): string {
        return _.isUndefined(value)
            ? null
            : JSON.stringify(value);
    }

    fromSQLValue(value: string): any {
        return _.isNull(value)
            ? undefined
            : JSON.parse(value, dateReviver);
    }
}



var DATETIME_FORMAT = 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]ZZ';
var DATE_FORMAT = 'YYYY-MM-DD';
var TIME_FORMAT = 'HH:mm:ss.SSS[Z]ZZ';
export class DateTimeSQLMapping extends SQLMapping {
    toSQLValue(value?: any): string {
        return _.isUndefined(value)
            ? null
            : moment(value).format(DATETIME_FORMAT);
    }

    fromSQLValue(value?: string): any {
        return _.isNull(value)
            ? undefined
            : moment(value, DATETIME_FORMAT);
    }
}


export class DateSQLMapping extends SQLMapping {
    toSQLValue(value?: any): string {
        return _.isUndefined(value)
            ? null
            : moment(value).format(DATE_FORMAT);
    }

    fromSQLValue(value?: string): any {
        return _.isNull(value)
            ? undefined
            : moment(value, DATE_FORMAT);
    }
}


export class TimeSQLMapping extends SQLMapping {
    toSQLValue(value?: any): string {
        return _.isUndefined(value)
            ? null
            : moment(value).format(TIME_FORMAT);
    }

    fromSQLValue(value?: string): any {
        return _.isNull(value)
            ? undefined
            : moment(value, TIME_FORMAT);
    }
}

export class BooleanSQLMapping extends SQLMapping {
    makeSQLField() { return this.name + " TINYINT" + this.getConstraints(); }

    toSQLValue(value: any): number {
        switch (value) {
            case true: return 1;
            case false: return 0;
            default: return null;
        }
    }

    fromSQLValue(value: any): boolean {
        switch (value) {
            case 1: return true;
            case 0: return false;
            default: return undefined;
        }
    }
}

var mappingRegister = {
    'String': StringSQLMapping,
    'Int': IntSQLMapping,
    'Float': FloatSQLMapping,
    'ForeignKey': ForeignKeySQLMapping,
    'JSON': JSONSQLMapping,
    'Boolean': BooleanSQLMapping,
    'DateTime': DateTimeSQLMapping,
    'Time': TimeSQLMapping,
    'Date': DateSQLMapping,
    'ManyToMany': ManyToManySQLMapping,
}

export function getMappingFor(
    tableName: string, field: Fields.Field
): SQLMapping {
    return new mappingRegister[field.type](
        tableName, field.name, field
    );
}

export function addMapping(fieldType: string, mapper: typeof SQLMapping) {
    mappingRegister[fieldType] = mapper;
}
