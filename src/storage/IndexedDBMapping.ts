///<reference path="../../typedefs/node.d.ts"/>
///<reference path="../../typedefs/moment.d.ts"/>
///<reference path="../../typedefs/bluebird.d.ts"/>
///<reference path="../../typedefs/lodash.d.ts"/>

var Promise: Bluebird.PromiseStatic = require('bluebird');

var READ_WRITE_FLAG = "readwrite";
import Schema = require('../Schema');
import Fields = require('../Fields');

var _ = require('lodash');
var moment = require('moment');

export class IDBMapping {
    constructor(
        public tableName: string,
        public name: string,
        public field: Fields.Field
    ) { }

    createsValue() { return true; }
    createsStore() { return false; }
    changesRecord() { return false; }

    makeIDBIndex(store) {
        var opts: any = {};
        opts.unique = this.field.primaryKey ? true : false;
        if (this.field.unique) opts.unique = true;

        store.createIndex(this.field.name, this.field.name, opts);
    }

    removeIDBIndex(store) {
        store.deleteIndex(this.field.name);
    }

    createIDBStore(idb: any): any {
        throw new Error("createIDBStore not implemented");
    }

    deleteIDBStore(idb: any): any {
        throw new Error("deleteIDBStore not implemented");
    }

    toIDBRecord(record: any, idbRecord: any): any {
        throw new Error("changes record is true, yet toIDBRecord not implemented");
    }
    fromIDBRecord(idbRecord: any, record: any): any {
        throw new Error("changes record is true, yet fromIDBRecord not implemented");
    }
    
    toIDBValue(value: any): any { return value; }
    fromIDBValue(value: any): any { return value; }
}


var dateReviver = function(key, value) {
  if (typeof value == 'string') {
    var a = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
    if (a) return Date.parse(value);
  };
  return value;
};

export class JSONIDBMapping extends IDBMapping {
    makeIDBIndex(store) { return; }
    removeIDBIndex(store) { return; }

    toIDBValue(value: any): string { return JSON.stringify(value) }
    fromIDBValue(value: string): any {
        return _.isUndefined(value)
            ? undefined
            : JSON.parse(value, dateReviver);
    }
}


var DATETIME_FORMAT = 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]ZZ';
var DATE_FORMAT = 'YYYY-MM-DD';
var TIME_FORMAT = 'HH:mm:ss.SSS[Z]ZZ';
export class DateTimeIDBMapping extends IDBMapping {
    toIDBValue(value?: any): string {
        return _.isUndefined(value)
            ? undefined
            : moment(value).format(DATETIME_FORMAT);
    }

    fromIDBValue(value?: string): any {
        return _.isUndefined(value)
            ? undefined
            : moment(value, DATETIME_FORMAT);
    }
}


export class DateIDBMapping extends IDBMapping {
    toIDBValue(value?: any): string {
        return _.isUndefined(value)
            ? undefined
            : moment(value).format(DATE_FORMAT);
    }

    fromIDBValue(value?: string): any {
        return _.isUndefined(value)
            ? undefined
            : moment(value, DATE_FORMAT);
    }
}


export class TimeIDBMapping extends IDBMapping {
    toIDBValue(value?: any): string {
        return _.isUndefined(value)
            ? undefined
            : moment(value).format(TIME_FORMAT);
    }

    fromIDBValue(value?: string): any {
        return _.isUndefined(value)
            ? undefined
            : moment(value, TIME_FORMAT);
    }
}



export class BooleanIDBMapping extends IDBMapping {
    toIDBValue(value?: boolean): number {
        switch (value) {
            case true: return 1;
            case false: return 0;
            default: return undefined;
        }
    }

    fromIDBValue(value?: number): boolean {
        switch (value) {
            case 1: return true;
            case 0: return false;
            default: return undefined;
        }
    }
}


export class ManyToManyIDBMapping extends IDBMapping {
    createsStore() { return true; }

    mappingStoreName(): string {
        return this.tableName + '_' + this.field.name + '__' +
               this.field.relation + '_' + this.field.relatedName;
    }

    createIDBStore(tx: any): any {
        tx.db.createObjectStore(
            this.mappingStoreName(), {keyPath: 'id'}
        );
        var store = tx.objectStore(this.mappingStoreName());
        var fromName = this.tableName + '_' + this.field.name;
        store.createIndex(fromName, fromName, {});

        var toName = this.field.relation + '_' + this.field.relatedName;
        store.createIndex(toName, toName, {});
    }

    deleteIDBStore(tx: any): any {
        tx.db.deleteObjectStore(this.mappingStoreName());
    }

}

var mappingRegister = {
    'String': IDBMapping,
    'Int': IDBMapping,
    'Float': IDBMapping,
    'JSON': JSONIDBMapping,
    'Boolean': BooleanIDBMapping,
    'ForeignKey': IDBMapping,
    'DateTime': DateTimeIDBMapping,
    'Time': TimeIDBMapping,
    'Date': DateIDBMapping,
    'ManyToMany': ManyToManyIDBMapping,
}


export function getMappingFor(
    tableName: string, field: Fields.Field
): IDBMapping {
    return new mappingRegister[field.type](
        tableName, field.name, field
    );
}

export function addMapping(fieldType: string, mapper: typeof IDBMapping) {
    mappingRegister[fieldType] = mapper;
}
