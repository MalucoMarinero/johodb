///<reference path="../../typedefs/node.d.ts"/>
///<reference path="../../typedefs/bluebird.d.ts"/>

var Promise: Bluebird.PromiseStatic = require('bluebird');
import Schema = require('../Schema');
import Model = require('../Model');
import Relations = require('../Relations');


class BaseStorage {
    public promise: Bluebird.Promise;

    constructor(public name: string) {
        this.promise = Promise.resolve(true);
    }

    initDB(tableSchema: any, migrations: Schema.Migration[], clobber?: boolean): Bluebird.Promise {
        throw "initDB Not implemented";
    }

    save(model: Model, record: any, opts: any): Bluebird.Promise {
        throw "save not implemented";
    }

    delete(model: Model, key: string, opts: any): Bluebird.Promise {
        throw "delete not implemented";
    }

    evaluateQuery(model: Model, query: any, opts: any): Bluebird.Promise {
        throw "evaluateQuery not implemented";
    }

    updateForeignKey(
        model: Model, recordKey: string, field: string, reverseKey: string
    ): Bluebird.Promise {
        throw "updateForeignKey not implemented";
    }

    clearForeignKeys(
        model: Model, recordKey: string, field: string, exceptions: string[]
    ): Bluebird.Promise {
        throw "clearForeignKeys not implemented";
    }

    collectManyToManyRelations(
        relation: Relations.ModelRelationManager, key: string, opts: string
    ): Bluebird.Promise {
        throw "collectManyToManyRelations not implemented";
    }

    clearManyToManyRelations(
        relTable: Relations.RelationTable, key: string, field: string
    ): Bluebird.Promise {
        throw "clearManyToManyRelations not implemented";
    }

    createManyToManyRelations(
        relTable: Relations.RelationTable, relations: any[]
    ): Bluebird.Promise {
        throw "createManyToManyRelations not implemented";
    }
}

export = BaseStorage;
