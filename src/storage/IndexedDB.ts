///<reference path="../../typedefs/node.d.ts"/>
///<reference path="../../typedefs/bluebird.d.ts"/>
///<reference path="../../typedefs/lodash.d.ts"/>
var _ = require('lodash');

interface IDBWindow extends Window {
    IDBKeyRange: any;
    IDBTransaction: any;
    webkitIndexedDB: any;
    webkitIDBKeyRange: any;
    webkitIDBTransaction: any;
}
declare var window: IDBWindow;


if (window.webkitIndexedDB) {
    window.indexedDB = window.webkitIndexedDB;
    window.IDBKeyRange = window.webkitIDBKeyRange;
    window.IDBTransaction = window.webkitIDBTransaction;
}

interface IDBEventTarget extends EventTarget {
    result?: any;
    transaction?: any;
}

interface IDBEvent extends Event {
    target: IDBEventTarget;
}

var IDBKeyRange = window.IDBKeyRange;

var Promise: Bluebird.PromiseStatic = require('bluebird');

var READ_WRITE_FLAG = "readwrite";
import BaseStorage = require('./BaseStorage');
import IndexedDBMapping = require('./IndexedDBMapping');
import Schema = require('../Schema');
import Model = require('../Model');
import Queries = require('../Queries');
import Relations = require('../Relations');
import UUID = require('../utils/UUID');

enum SaveAction { UPDATE_RECORD, ADD_RECORD }

class IndexedDBStorage extends BaseStorage {
    static IndexedDBMapping = IndexedDBMapping;

    initDB(tableSchema: any, migrations: Schema.Migration[], clobber?: boolean): Bluebird.Promise {
        if (clobber) {
            this.promise = this.promise.then(() => { return new Promise((resolve, reject) => {
                console.warn('Clobbering IndexedDB', this.name);
                var req = indexedDB.deleteDatabase(this.name);
                req.onsuccess = (e) => {
                    console.warn('Clobbered IndexedDB', this.name);
                    resolve(e);
                }

                req.onerror = (e) => {
                    reject(e);
                    console.error("Failed clobber of IndexedDB", e);
                };
            }); });
        }

        this.promise = this.promise.then(() => {
            return this.openDB(tableSchema, migrations);
        });

        return this.promise;
    }

    private openDB(
        tableSchema: any, migrations: Schema.Migration[]
    ): Bluebird.Promise {
        return new Promise((resolve, reject) => {
            var req = window.indexedDB.open(this.name, migrations.length);
            req.onsuccess = (e: IDBEvent) => {
                var idb = e.target.result;
                resolve(idb);
            };

            req.onupgradeneeded = (e: IDBEvent) => {
                var idb = e.target.result;
                var tx = e.target.transaction;
                var migrationsToDo = (idb.version == 1)
                    ? migrations
                    : migrations.slice(idb.version);

                this.applyMigrations(tx, migrationsToDo);
            };

            req.onerror = (e) => {
                reject(e);
            };

        });
    }


    private applyMigrations( tx, migrations: Schema.Migration[]) {
        migrations.forEach((migration) => {
            migration.actions.forEach((action) => {
                this.applyMigrationAction(tx, action,
                                          migration.frozenDatabaseModel)
            });
        });
    }

    private applyMigrationAction(
        tx, action: Schema.MigrationAction, dbModel: Schema.DatabaseModelling
    ) {
        switch (action.type) {
            case Schema.MigrationActionTypes.ADD_TABLE:
                tx.db.createObjectStore(action.args.tableName);
                break;
            case Schema.MigrationActionTypes.ADD_COLUMN:
                var mapping = IndexedDBMapping.getMappingFor(
                    action.args.tableName,
                    dbModel.models[action.args.tableName]
                           .fields[action.args.columnName]
                );

                if (mapping.createsStore()) {
                    mapping.createIDBStore(tx);
                } else {
                    var store = tx.objectStore(action.args.tableName)
                    mapping.makeIDBIndex(store);
                }
                break;
            case Schema.MigrationActionTypes.REMOVE_COLUMN:
                var mapping = IndexedDBMapping.getMappingFor(
                    action.args.tableName,
                    dbModel.models[action.args.tableName]
                           .fields[action.args.columnName]
                );

                if (mapping.createsStore()) {
                    mapping.deleteIDBStore(tx);
                } else {
                    var store = tx.objectStore(action.args.tableName)
                    mapping.removeIDBIndex(store);
                }
                break;
            case Schema.MigrationActionTypes.ALTER_COLUMN:
                var mapping = IndexedDBMapping.getMappingFor(
                    action.args.tableName,
                    dbModel.models[action.args.tableName]
                           .fields[action.args.columnName]
                );

                if (mapping.createsStore()) {
                    mapping.deleteIDBStore(tx);
                    mapping.createIDBStore(tx);
                } else {
                    var store = tx.objectStore(action.args.tableName)
                    mapping.removeIDBIndex(store);
                    mapping.makeIDBIndex(store);
                }
                break;
            case Schema.MigrationActionTypes.REMOVE_TABLE:
                tx.db.deleteObjectStore(action.args.tableName);
                break;
        }
    }


    private toIndexedDBRecord(model: Model, record: any): any {
        var idbRecord: any = {};
        _.forOwn(model.fields, (field, name) => {
            var mapping = IndexedDBMapping.getMappingFor(model.name,
                                                         field);
            if (mapping.createsValue()) {
                if (mapping.changesRecord()) {
                    idbRecord = mapping.toIDBRecord(record, idbRecord);
                } else {
                    idbRecord[name] = mapping.toIDBValue(record[name]);
                }
            }
        });

        return idbRecord;
    }

    private fromIndexedDBRecord(model: Model, idbRecord: any): any {
        var record: any = {};
        _.forOwn(model.fields, (field, name) => {
            var mapping = IndexedDBMapping.getMappingFor(model.name,
                                                         field);
            if (mapping.createsValue()) {
                if (mapping.changesRecord()) {
                    record = mapping.fromIDBRecord(idbRecord, record);
                } else {
                    record[name] = mapping.fromIDBValue(idbRecord[name]);
                }
            }
        });

        return record;
    }

    updateForeignKey(
        model: Model, recordKey: string, field: string, reverseKey: string
    ): Bluebird.Promise {
        return new Promise((resolve, reject) => {

        this.promise.then((idb) => {
            var req = idb.transaction(model.name, READ_WRITE_FLAG)
                         .objectStore(model.name)
                         .index(model.primaryKey)
                         .openCursor(IDBKeyRange.only(recordKey));

            req.onsuccess = (e) => {
                var cursor = e.target.result;
                if (cursor) {
                    var record = cursor.value;
                    record[field] = reverseKey;
                    var updateReq = cursor.update(record);
                    updateReq.onsuccess = (e) => {
                        resolve(record);
                    };
                    updateReq.onerror = (e) => { reject(e) };
                } else {
                    reject(new Error("ForeignKey update failed"));
                }
            };
            req.onerror = (e) => { reject(e); };
        });

        });
    }

    clearForeignKeys(
        model: Model, recordKey: string, field: string, exceptions: string[]
    ): Bluebird.Promise {
        return new Promise((resolve, reject) => {
        this.promise.then((idb) => {
            var stack = [];

            var req = idb.transaction(model.name, READ_WRITE_FLAG)
                         .objectStore(model.name)
                         .index(field)
                         .openCursor(IDBKeyRange.only(recordKey));

            req.onsuccess = (e) => {
                var cursor = e.target.result;
                if (cursor) {
                    var record = cursor.value;

                    if (exceptions.indexOf(record[model.primaryKey]) == -1 ) {
                        record[field] = undefined;
                        var ureq = cursor.update(record);
                        stack.push(new Promise((res, rej) => {
                            ureq.onsuccess = (e) => { res(true); };
                            ureq.onerror = (e) => { rej(false); };
                        }));
                    } else {
                        cursor.continue();
                    }
                } else {
                    Promise.all(stack).then((e) => resolve(true));
                }
            };
            req.onerror = (e) => { reject(e); };
        });
        });
    }


    collectManyToManyRelations(
        relation: Relations.ModelRelationManager, key: string, opts: string
    ): Bluebird.Promise {
        var rel = relation;
        return new Promise((resolve, reject) => {
        this.promise.then((idb) => {
            var stack = [];
            var req = idb.transaction(rel.relationTableName)
                         .objectStore(rel.relationTableName)
                         .index(rel.getOwnKey())
                         .openCursor(IDBKeyRange.only(key))
            req.onsuccess = (e) => {
                var cursor = e.target.result;
                if (cursor) {
                    var record = cursor.value;
                    var recKey = record[rel.getRelKey()];
                    var query: any = {};
                    query[rel.model.primaryKey] = recKey;
                    stack.push(rel.model.query().get(query).evaluate(opts));
                    cursor.continue();
                } else {
                    Promise.all(stack).then((records) => resolve(records));
                }
            };

            req.onerror = (e) => { reject(e); };
        });
        });
    }

    clearManyToManyRelations(
        relTable: Relations.RelationTable, key: string, field: string
    ): Bluebird.Promise {
        return new Promise((resolve, reject) => {
        this.promise.then((idb) => {
            var stack = [];
            var req = idb.transaction(relTable.name, READ_WRITE_FLAG)
                         .objectStore(relTable.name)
                         .index(field)
                         .openCursor(IDBKeyRange.only(key))
            
            req.onsuccess = (e) => {
                var cursor = e.target.result;
                if (cursor) {
                    var ureq = cursor.delete();
                    stack.push(new Promise((res, rej) => {
                        ureq.onsuccess = (e) => { res(true); };
                        ureq.onerror = (e) => { rej(false); };
                    }));
                    cursor.continue();
                } else {
                    Promise.all(stack).then((e) => resolve(true));
                }
            };
            req.onerror = (e) => { reject(e); };
        });
        });
    }

    createManyToManyRelations(
        relTable: Relations.RelationTable, relations: any[]
    ): Bluebird.Promise {
        return new Promise((resolve, reject) => {
        this.promise.then((idb) => {
            var stack = relations.map((relation) => {
                return new Promise((res, rej) => {
                    relation['id'] = UUID.guid();
                    var req = idb.transaction(relTable.name, READ_WRITE_FLAG)
                                 .objectStore(relTable.name)
                                 .add(relation);
                    req.onsuccess = (e) => res(relation);
                    req.onerror = (e) => reject(e);
                });
            });

            Promise.all(stack).then((results) => resolve(results));
        });
        });
    }


    save(model: Model, record: any, opts?: any): Bluebird.Promise {
        return new Promise((resolve, reject) => {

        var saveRecord = this.toIndexedDBRecord(model, record);

        this.promise.then((idb) => {
            var checkPromise = new Promise((chResolve, chReject) => {
                var req = idb.transaction(model.name).objectStore(model.name)
                             .index(model.primaryKey)
                             .openCursor(
                                 IDBKeyRange.only(saveRecord[model.primaryKey])
                             );
                req.onsuccess = (e) => {
                    chResolve(e.target.result ? SaveAction.UPDATE_RECORD
                                              : SaveAction.ADD_RECORD);
                };
                req.onerror = (e) => { chReject(e); };
            });

            checkPromise.then((action: SaveAction) => {

                switch (action) {
                case SaveAction.ADD_RECORD:
                var req = idb.transaction(model.name, READ_WRITE_FLAG)
                             .objectStore(model.name)
                             .add(saveRecord, saveRecord[model.primaryKey]);
                req.onsuccess = (e) => { resolve(saveRecord); };
                req.onerror = (e) => { reject(e); };
                break;

                case SaveAction.UPDATE_RECORD:
                var req = idb.transaction(model.name, READ_WRITE_FLAG)
                             .objectStore(model.name).index(model.primaryKey)
                             .openCursor(
                                 IDBKeyRange.only(saveRecord[model.primaryKey])
                             );
                req.onsuccess = (e) => {
                    var doSave = e.target.result.update(saveRecord);
                    doSave.onsuccess = (e) => { resolve(saveRecord); };
                    doSave.onerror = (e) => { reject(e); };
                };
                req.onerror = (e) => { reject(e); };
                break;

                }

            });
        });

        });
    }




    delete(model: Model, key: string, opts: any): Bluebird.Promise {
        return new Promise((resolve, reject) => {
        this.promise.then((idb) => {
            var req = idb.transaction(model.name, READ_WRITE_FLAG)
                         .objectStore(model.name)
                         .index(model.primaryKey)
                         .openCursor(IDBKeyRange.only(key));
            req.onsuccess = (e) => {
                var cursor = e.target.result;
                if (cursor) {
                    var ureq = cursor.delete();
                    ureq.onsuccess = (e) => { resolve(true) };
                    ureq.onerror = (e) => { reject(e); };
                } else {
                    resolve(true);
                }

            }
                            
            req.onerror = (e) => { reject(e); };

        });
        });
    }



    evaluateQuery(model: Model, query: Queries.Query, opts: any): Bluebird.Promise {
        return new Promise((resolve, reject) => {

        this.promise.then((idb) => {
            if (query.hasStep(Queries.QueryStepType.GET)) {
                var lookups = query.getStep(Queries.QueryStepType.GET).lookups;
                this.getQuery(idb, model, lookups, opts).then((r) => resolve(r));
            } else if (query.hasStep(Queries.QueryStepType.ALL)) {
                this.getAllQuery(idb, model, opts).then((r) => resolve(r));
            } else {
                this.executeQuery(idb, model, query.steps, opts).then((r) => resolve(r));
            }
        });

        });
    }

    private getQuery(
        idb: any, model: Model, lookups: Queries.FieldLookup[], opts: any
    ): Bluebird.Promise {
        return new Promise((resolve, reject) => {

        var matchStack = [];

        if (lookups[0].fields.length == 1 && hasLookupBound(lookups[0])) {
            var leadLookup = lookups.shift();
            var req = idb.transaction(model.name)
                         .objectStore(model.name)
                         .index(getLookupIndex(leadLookup))
                         .openCursor(getLookupBound(leadLookup));
        } else {
            var req = idb.transaction(model.name)
                         .objectStore(model.name)
                         .index(model.primaryKey)
                         .openCursor();
        }

        req.onsuccess = (e) => {
            var cursor = e.target.result;
            if (cursor) {
                matchStack.push(matchLookups(lookups, cursor.value));
                cursor.continue();
            } else {
                Promise.all(matchStack).then((results) => {
                    results = results.filter((r) => r != null);
                    if (results.length > 0) {
                        resolve(this.fromIndexedDBRecord(model, results[0]));
                    } else {
                        resolve(null);
                    }
                });
            }
        };
        req.onerror = (e) => { reject(e); };

        });
    }

    private executeQuery(
        idb: any, model: Model, steps: Queries.QueryStep[], opts: any
    ): Bluebird.Promise {
        return new Promise((resolve, reject) => {

        var stack = [];
        var leadStep = steps[0];
        var willExclude = leadStep.type == Queries.QueryStepType.EXCLUDE;
        var canBeBound = (leadStep.lookups[0].fields.length == 1 &&
                          hasLookupBound(leadStep.lookups[0]));

        if (!canBeBound || willExclude) {
            var req = idb.transaction(model.name).objectStore(model.name)
                         .index(model.primaryKey).openCursor();
        } else {
            var leadLookup = leadStep.lookups.shift();

            var req = idb.transaction(model.name).objectStore(model.name)
                         .index(getLookupIndex(leadLookup))
                         .openCursor(getLookupBound(leadLookup));
        }

        req.onsuccess = (e) => {
            var cursor = e.target.result;
            if (cursor) { 
                stack.push(matchSteps(steps, cursor.value));
                cursor.continue();
            } else {
                Promise.all(stack).then((results) => {
                    var records = results.filter((r) => r != false);
                    resolve(records.map((r) => this.fromIndexedDBRecord(model, r)));
                });

            }

        }

        req.onerror = (e) => { reject(e); };

        });
    }

    private getAllQuery(
        idb: any, model: Model, opts: any
    ): Bluebird.Promise {
        return new Promise((resolve, reject) => {

        var results = [];

        var req = idb.transaction(model.name)
                     .objectStore(model.name)
                     .index(model.primaryKey)
                     .openCursor();

        req.onsuccess = (e) => {
            var cursor = e.target.result;
            if (cursor) {
                results.push(this.fromIndexedDBRecord(model, cursor.value));
                cursor.continue();
            } else {
                resolve(results);
            }
        };
        req.onerror = (e) => { reject(e); };

        });
    }
}


var matchSteps = function (steps: Queries.QueryStep[], record: any): Bluebird.Promise {
    return new Promise((resolve, reject) => {

    var stack = steps.map((step) => matchLookups(step.lookups, record));

    Promise.all(stack).then((results) => {
        var depth = 0;
        for (var ix = 0; ix < steps.length; ix++) {
            if (steps[ix].type == Queries.QueryStepType.EXCLUDE) {
                if (results[ix]) {
                    return resolve(false);
                } else {
                    depth++;
                }
            }

            if (steps[ix].type == Queries.QueryStepType.FILTER && results[ix]) {
                depth++;
            }
        }

        if (depth == steps.length) {
            resolve(record);
        } else {
            resolve(false);
        }
    });

    });
}


var matchLookups = function (lookups: Queries.FieldLookup[], record: any): Bluebird.Promise {
    return new Promise((resolve, reject) => {

    if (lookups.length == 0) {
        resolve(record);
    } else {
        var stack = lookups.map((lookup) => testIDBValue(lookup, record));
        Promise.all(stack).then((results) => {
            if (results.every((tf) => tf)) {
                resolve(record);
            } else {
                resolve(false);
            }
        });
    }

    });
}

var getLookupTestValue = function(lookup: Queries.FieldLookup, term: any): any {
    var model: Model;
    if (lookup.fields.length == 1) {
        var field = lookup.fields[0];
        model = lookup.model;
    } else {
        var field = lookup.fields[lookup.fields.length - 1];
        model = lookup.fields.reduce((lastModel: Model, nextField) => {
            if (lastModel.relations[nextField]) {
                return lastModel.relations[nextField].model;
            } else {
                return lastModel;
            }
        }, lookup.model);
    }

    var mapping = IndexedDBMapping.getMappingFor(model.name, model.fields[field]);
    return mapping.toIDBValue(term);
}

var testIDBValue = function (lookup: Queries.FieldLookup, record: any): Bluebird.Promise {
    return new Promise((resolve, reject) => {

    getLookupRecords(lookup, record).then((args) => {
        var records = args[0], fieldName = args[1];

        if (fieldName == '__neverReached') return resolve(false);

        return resolve(records.some((record) => {
            var term = getLookupTestValue(lookup, lookup.value);
            var val = record[fieldName];
            switch (lookup.type) {
                case Queries.LookupType.EXACT:
                return term === val;

                case Queries.LookupType.GT:
                return val > term;

                case Queries.LookupType.GTE:
                return val >= term;

                case Queries.LookupType.LT:
                return val < term;

                case Queries.LookupType.LTE:
                return val <= term;

                case Queries.LookupType.IN:
                return term.indexOf(val) != -1;
            }
        }));

    });

    });
}

var getLookupRecords = function (lookup: Queries.FieldLookup, record: any): Bluebird.Promise {
    return new Promise((resolve, reject) => {

    if (lookup.fields.length == 1) {
        resolve([[record], lookup.fields[0]]);
    } else {
        var fields = _.clone(lookup.fields);
        recurseLookupFields(fields, [record], lookup.model).then((results) => {
            resolve(results);
        });
    }
    });
}

var recurseLookupFields = function (fields: string[], records: any[], model: Model): Bluebird.Promise {
    return new Promise((resolve, reject) => {

    var field = fields.shift();
    if (fields.length == 0) return resolve([records, field]);

    var stack = [];
    var rel = model.relations[field];
    if (rel.type == 'ForeignKey') {
        stack = records.filter((r) => !!r[field]).map((record) => {
            var query: any = {};
            query[rel.model.primaryKey] = record[field];
            return rel.model.query().get(query).evaluate({depth: 0});
        });

        if (stack.length == 0) {
            resolve([[records], '__neverReached']);
        } else {
            Promise.all(stack).then((nextRecords) => {
                recurseLookupFields(fields, nextRecords, rel.model).then((args) => {
                    resolve(args);
                });
            });
        }
    } else if (rel.type == 'OneToMany') {
        stack = records.map((record) => {
            var query: any = {};
            query[rel.relatedName] = record[model.primaryKey];
            return rel.model.query().filter(query).evaluate({depth: 0});
        });

        Promise.all(stack).then((nextRecordArrays) => {
            recurseLookupFields(
                fields, _.flatten(nextRecordArrays), rel.model
            ).then((args) => { resolve(args); });
        });

    }

    });
}


var getLookupIndex = function (lookup: Queries.FieldLookup) {
    return lookup.fields[0];
}

var hasLookupBound = function (lookup: Queries.FieldLookup) {
    try {
        getLookupBound(lookup);
        return true;
    } catch (e) {
        return false;
    }
}

var getLookupBound = function (lookup: Queries.FieldLookup) {
    switch (lookup.type) {
        case Queries.LookupType.EXACT:
        return IDBKeyRange.only(lookup.value);

        case Queries.LookupType.GT:
        return IDBKeyRange.lowerBound(lookup.value, true);

        case Queries.LookupType.GTE:
        return IDBKeyRange.lowerBound(lookup.value);

        case Queries.LookupType.LT:
        return IDBKeyRange.upperBound(lookup.value, true);

        case Queries.LookupType.LTE:
        return IDBKeyRange.upperBound(lookup.value);

        default:
        throw "No valid lookup found for " + Queries.LookupType[lookup.type];

    };
}



export = IndexedDBStorage;
