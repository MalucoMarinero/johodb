///<reference path="../../typedefs/node.d.ts"/>
///<reference path="../../typedefs/bluebird.d.ts"/>
///<reference path="../../typedefs/lodash.d.ts"/>
var _ = require('lodash');

interface WebSQLTransaction {
    executeSql: (statement: string, params: any[]) => any;
}

interface WebSQLDatabase {
    transaction: (
        doTransaction: (tx: WebSQLTransaction) => any,
        onError: (tx: any, error: any) => any,
        onSucess: (tx: any, success: any) => any
    ) => any;
}


interface WebSQLWindow extends Window {
    openDatabase: (name: string, version: number,
                   niceName: string, size: number) => WebSQLDatabase;
}
declare var window: WebSQLWindow;

var Promise: Bluebird.PromiseStatic = require('bluebird');

var READ_WRITE_FLAG = "readwrite";
import BaseStorage = require('./BaseStorage');
import WebSQLMapping = require('./WebSQLMapping');
import Schema = require('../Schema');
import Model = require('../Model');
import Queries = require('../Queries');
import Relations = require('../Relations');
import UUID = require('../utils/UUID');


class WebSQLStorage extends BaseStorage {
    initDB(tableSchema: any, migrations: Schema.Migration[], clobber?: boolean): Bluebird.Promise {

        var db = window.openDatabase(this.name, migrations.length,
                          'Joho Database', 2 * 1024 * 1024);

        if (clobber) {
            this.promise = this.promise.then(() => { return new Promise((resolve, reject) => {
                console.warn('Clobbering WebSQL', this.name);
                db.transaction((tx) => {
                    this.dropAllTables(tx, tableSchema);
                }, (tx, error) => {
                    reject(error);
                    console.error("Failed clobber of WebSQL", error);
                }, (tx, success) => {
                    console.warn('Clobbered WebSQL', this.name);
                    resolve(success);
                });
            }); });
        }

        this.promise = this.promise.then(() => { return new Promise((resolve, reject) => {
            db.transaction((tx) => {
                this.applyMigrations(tx, migrations);
            }, (tx, error) => {
                reject(error);
                console.error("Failed migration of WebSQL", tx, error);
            }, (tx, success) => {
                console.warn('Migrated WebSQL', this.name);
                resolve(db);
            });
        }); });

        return this.promise;
    }

    private dropAllTables(tx, tableSchema) {
        _.forOwn(tableSchema.models, (model, modelName) => {
            tx.executeSql("DROP TABLE IF EXISTS " + model.name + ';');
        });
    }

    private applyMigrations(tx, migrations: Schema.Migration[]) {
        migrations.forEach((migration) => {
            migration.actions.forEach((action) => {
                this.applyMigrationAction(tx, action,
                                          migration.frozenDatabaseModel)
            });
        });
    }


    private applyMigrationAction(
        tx, action: Schema.MigrationAction, dbModel: Schema.DatabaseModelling
    ) {
        switch (action.type) {
            case Schema.MigrationActionTypes.ADD_TABLE:
                var key = dbModel.models[action.args.tableName].primaryKey;
                var pField = dbModel.models[action.args.tableName].fields[key];

                // patched in as table NEEDS a primary column
                pField['sqlUpdatedEarly'] = true;
                var mapping = WebSQLMapping.getMappingFor(
                    action.args.tableName,
                    pField
                );
                tx.executeSql(
                    "CREATE TABLE IF NOT EXISTS " + action.args.tableName +
                    " (" + mapping.makeSQLField() + ");"
                );
                break;
            case Schema.MigrationActionTypes.ADD_COLUMN:
                var field = dbModel.models[action.args.tableName].fields
                                          [action.args.columnName];

                if (field['sqlUpdatedEarly']) return;

                var mapping = WebSQLMapping.getMappingFor(
                    action.args.tableName, field
                );
                if (mapping.createsTable()) {
                    mapping.createSQLTable(tx);
                } else {
                    tx.executeSql(
                        "ALTER TABLE " + action.args.tableName +
                        " ADD COLUMN " + mapping.makeSQLField()
                    );
                }
                break;
            case Schema.MigrationActionTypes.REMOVE_COLUMN:
                throw new Error('not implemented yet');
                break;
            case Schema.MigrationActionTypes.ALTER_COLUMN:
                throw new Error('not implemented yet');
                break;
            case Schema.MigrationActionTypes.REMOVE_TABLE:
                tx.executeSql("DROP TABLE IF EXISTS " + action.args.tableName);
                break;
        }
    }


    private toWebSQLRecord(model: Model, record: any): any {
        var sqlRecord: any = {};
        _.forOwn(model.fields, (field, name) => {
            var mapping = WebSQLMapping.getMappingFor(model.name,
                                                      field);
            if (mapping.createsValue()) {
                if (mapping.changesRecord()) {
                    sqlRecord = mapping.toSQLRecord(record, sqlRecord);
                } else {
                    sqlRecord[name] = mapping.toSQLValue(record[name]);
                }
            }
        });

        return sqlRecord;
    }

    private fromWebSQLRecord(model: Model, sqlRecord: any): any {
        var record: any = {};
        _.forOwn(model.fields, (field, name) => {
            var mapping = WebSQLMapping.getMappingFor(model.name,
                                                      field);
            if (mapping.createsValue()) {
                if (mapping.changesRecord()) {
                    record = mapping.fromSQLRecord(sqlRecord, record);
                } else {
                    record[name] = mapping.fromSQLValue(sqlRecord[name]);
                }
            }
        });

        return record;
    }

    clearForeignKeys(
        model: Model, recordKey: string, field: string, exceptions: string[]
    ): Bluebird.Promise {
        return new Promise((resolve, reject) => {
        exceptions = exceptions.filter((ex) => !!ex);

        this.promise.then((db) => {
            db.transaction((tx) => {
                var sql = "UPDATE " + model.name + " SET " + field + " = ? ";
                sql += "WHERE " + field + " = ?";

                if (exceptions.length > 0) {
                    var places = exceptions.map(() => '?').join(',');
                    sql += " AND " + model.primaryKey;
                    sql += " not in (" + places + ");";
                    tx.executeSql(sql, [null, recordKey].concat(exceptions));
                } else {
                    sql += ';';
                    tx.executeSql(sql, [null, recordKey]);
                }
            }, (tx, error) => {
                reject(error);
            }, (tx, success) => {
                resolve(recordKey);
            });
        });

        });
    }



    updateForeignKey(
        model: Model, recordKey: string, field: string, reverseKey: string
    ): Bluebird.Promise {
        return new Promise((resolve, reject) => {

        this.promise.then((db) => {
            db.transaction((tx) => {
                var sql = "UPDATE " + model.name + " SET " + field + " = ? ";
                sql += "WHERE " + model.primaryKey + " = ?;";

                tx.executeSql(sql, [reverseKey, recordKey]);
            }, (tx, error) => {
                reject(error);
            }, (tx, success) => {
                resolve(recordKey);
            });
        });

        });
    }


    collectManyToManyRelations(
        relation: Relations.ModelRelationManager, key: string, opts: string
    ): Bluebird.Promise {
        return new Promise((resolve, reject) => {
        var rel = relation;

        var stack = [];
        this.promise.then((db) => {
            db.transaction((tx) => {
                var sql = "SELECT * FROM " + rel.relationTableName + " WHERE ";
                sql += rel.getRelKey() + " IN (SELECT ";
                sql += rel.relationTableName + "." + rel.getRelKey();
                sql += " FROM " + rel.relationTableName + " WHERE ";
                sql += rel.relationTableName + "." + rel.getOwnKey() + " = ?);";

                tx.executeSql(sql, [key]
                , (tx, res) => {
                    stack = _.range(res.rows.length).map((ix) => {
                        var record = res.rows.item(ix);
                        var recKey = record[rel.getRelKey()];
                        var query: any = {};
                        query[rel.model.primaryKey] = recKey;
                        return rel.model.query().get(query).evaluate(opts);
                    });
                }, (tx, error) => {
                    console.error(tx, error);
                    reject([tx, error]);
                });
            }, (error) => {
                console.error(error);
                reject(error);
            }, (tx, success) => {
                Promise.all(stack).then((results) => resolve(results));
            });
        });
        });
    }

    clearManyToManyRelations(
        relTable: Relations.RelationTable, key: string, field: string
    ): Bluebird.Promise {
        return new Promise((resolve, reject) => {
        this.promise.then((db) => {
            db.transaction((tx) => {
                var sql = "DELETE FROM " + relTable.name + " WHERE " + field;
                sql += " IN (SELECT " + relTable.name + "." + field + " ";
                sql += "FROM " + relTable.name + " WHERE ";
                sql += relTable.name + "." + field + " = ?);";
                console.log(key);
                tx.executeSql(sql, [key]);

            }, (error) => {
                console.error(error);
                reject(error);
            }, (tx, success) => {
                resolve(key);
            });
        });
        });
    }

    createManyToManyRelations(
        relTable: Relations.RelationTable, relations: any[]
    ): Bluebird.Promise {
        return new Promise((resolve, reject) => {
        this.promise.then((db) => {
            db.transaction((tx) => {
                relations.forEach((relation) => {
                    relation['id'] = UUID.guid();
                    var fieldList = [];
                    var valueList = [];
                    var shroud = [];
                    _.forOwn(relation, (value, key) => {
                        fieldList.push(key);
                        valueList.push(value);
                        shroud.push('?');
                    });

                    var sql = "INSERT INTO " + relTable.name + " (";
                    sql += fieldList.join(",") + ") VALUES (";
                    sql += shroud.join(",") + ")";

                    tx.executeSql(sql, valueList);
                });
            }, (error) => {
                console.error(error);
                reject(error);
            }, (tx, success) => {
                resolve(relations);
            });
        });
        });
    }


    save(model: Model, record: any, opts?: any): Bluebird.Promise {
        return new Promise((resolve, reject) => {

        var saveRecord = this.toWebSQLRecord(model, record);

        this.promise.then((db) => {
            db.transaction((tx) => {
                var fieldList = [];
                var valueList = [];
                var shroud = [];

                _.forOwn(saveRecord, (value, key) => {
                    fieldList.push(key);
                    valueList.push(value);
                    shroud.push('?');
                });

                var sql = "INSERT OR REPLACE INTO " + model.name + " (";
                sql += fieldList.join(',') + ') VALUES (';
                sql += shroud.join(',') + ')';

                tx.executeSql(sql, valueList);
            }, (error) => {
                console.error(error);
                reject(error);
            }, (tx, success) => {
                resolve(saveRecord);
            });

        });


        });
    }



    delete(model: Model, key: string, opts: any): Bluebird.Promise {
        return new Promise((resolve, reject) => {
        this.promise.then((db) => {
            db.transaction((tx) => {
                var sql = "DELETE FROM " + model.name + " WHERE ";
                sql += model.primaryKey + ' = ?';
                tx.executeSql(sql, [key]);
            }, (error) => {
                console.error(error);
                reject(error);
            }, (tx, success) => {
                resolve(true);
            });
        });
        });
    }





    evaluateQuery(model: Model, query: Queries.Query, opts: any): Bluebird.Promise {
        return new Promise((resolve, reject) => {

        this.promise.then((db) => {
            if (query.hasStep(Queries.QueryStepType.GET)) {
                var lookups = query.getStep(Queries.QueryStepType.GET).lookups;
                this.getQuery(db, model, lookups, opts).then((r) => resolve(r));
            } else if (query.hasStep(Queries.QueryStepType.ALL)) {
                this.getAllQuery(db, model, opts).then((r) => resolve(r));
            } else {
                this.executeQuery(db, model, query.steps, opts).then((r) => resolve(r));
            }
        });

        });
    }

    private executeQuery(
        idb: any, model: Model, steps: Queries.QueryStep[], opts: any
    ): Bluebird.Promise {
        return new Promise((resolve, reject) => {

        var results: any[];
        this.promise.then((db) => {
            db.readTransaction((tx) => {
                var sql = "SELECT * FROM " + model.name + " ";
                sql += stepsToSql(model, steps);
                sql += ";";

                tx.executeSql(sql, null,
                (tx, res) => {
                    results = _.range(res.rows.length).map((ix) => {
                        return this.fromWebSQLRecord(model, res.rows.item(ix));
                    });
                }, (tx, res) => {
                    reject(res);
                });
            }, (error) => {
                console.error(error);
                reject(error);
            }, (tx, success) => {
                resolve(results);
            });
        });

        });
    }


    private getQuery(
        idb: any, model: Model, lookups: Queries.FieldLookup[], opts: any
    ): Bluebird.Promise {
        return new Promise((resolve, reject) => {

        var steps = [{
            type: Queries.QueryStepType.GET,
            lookups: lookups,
        }];

        var result: any;

        this.promise.then((db) => {
            db.readTransaction((tx) => {
                var sql = "SELECT * FROM " + model.name + " ";
                sql += stepsToSql(model, steps);
                sql += ";";

                tx.executeSql(sql, null,
                (tx, res) => {
                    if (res.rows.length > 0) {
                        result = this.fromWebSQLRecord(model, res.rows.item(0));
                    }
                }, (tx, res) => {
                    reject(res);
                });
            }, (error) => {
                console.error(error);
                reject(error);
            }, (tx, success) => {
                resolve(result || null);
            });
        });

        });
    }

    private getAllQuery(
        idb: any, model: Model, opts: any
    ): Bluebird.Promise {
        return new Promise((resolve, reject) => {

        var results: any[] = [];

        this.promise.then((db) => {
            db.readTransaction((tx) => {
                tx.executeSql("SELECT * FROM " + model.name + ";", null,
                (tx, res) => {
                    results = _.range(res.rows.length).map((ix) => {
                        return this.fromWebSQLRecord(
                            model, res.rows.item(ix)
                        );
                    });
                }, (tx, res) => {
                    reject(res);
                });
            }, (error) => {
                console.error(error);
            }, (tx, success) => {
                resolve(results);
            });
        });

        });
    }

}


var buildStatement = function(clauses: any): string {
    var statement = '';
    if (clauses.joinType) {
        statement += clauses.joinType + " JOIN ";
        statement += clauses.joinTable.join(', ');

        if (clauses.on.length > 0) {
            statement += ' ON ' + clauses.on.join(' AND ');
        }
    }

    if (clauses.where.length > 0) {
        statement += ' WHERE ';
        statement += clauses.where.join(' AND ');
    }

    return statement;
};

var stepsToSql = function(model: Model, steps: Queries.QueryStep[]): string {
    var clauses = steps.reduce((clauses: any, step: Queries.QueryStep) => {
        return processStep(clauses, model, step);
    }, {
        on: [],
        joinTable: [],
        joinType: null,
        where: []
    });

    return buildStatement(clauses);
}

var processStep = function(
    clauses: any, model: Model, step: Queries.QueryStep
): any {
    return step.lookups.reduce((clauses, lookup) => {
        return processLookup(clauses, model, step, lookup);
    }, clauses);
}

var processLookup = function(
    clauses: any, model: Model, step: Queries.QueryStep,
    lookup: Queries.FieldLookup
): any {
    var fields = _.clone(lookup.fields);
    return recurseJoin(clauses, model, step, lookup, fields);
}


var sType = Queries.QueryStepType
var recurseJoin = function(
    clauses: any, model: Model, step: Queries.QueryStep,
    lookup: Queries.FieldLookup, fields: string[]
): any {
    var field = fields.shift();

    if (fields.length == 0) {
        var sql = step.type == sType.EXCLUDE ? 'NOT ' : '';
        sql += model.name + "." + getWhereClause(lookup, field);

        clauses.where.push(sql);
        return clauses;
    } else {
        var rel = model.relations[field];

        if (rel.type == 'ForeignKey') {
            clauses.joinType = 'INNER';
            clauses.joinTable.push(rel.model.name);
            clauses.on.push(
                rel.ownModel + "." + rel.name + " = " +
                rel.relatedModel + "." + rel.relatedKey
            );
            clauses = recurseJoin(clauses, rel.model, step,
                                  lookup, fields);
        } else if (rel.type == 'OneToMany') {
            clauses.joinType = 'INNER';
            clauses.joinTable.push(rel.model.name);
            clauses.on.push(
                rel.ownModel + "." + rel.ownKey + " = " +
                rel.relatedModel + "." + rel.relatedName
            );
            clauses = recurseJoin(clauses, rel.model, step,
                                  lookup, fields);
        };

        return clauses;
    }
}

var getLookupTestValue = function(lookup: Queries.FieldLookup, term: any): any {
    var model: Model;
    if (lookup.fields.length == 1) {
        var field = lookup.fields[0];
        model = lookup.model;
    } else {
        var field = lookup.fields[lookup.fields.length - 1];
        model = lookup.fields.reduce((lastModel: Model, nextField) => {
            if (lastModel.relations[nextField]) {
                return lastModel.relations[nextField].model;
            } else {
                return lastModel;
            }
        }, lookup.model);
    }

    var mapping = WebSQLMapping.getMappingFor(model.name, model.fields[field]);
    return mapping.toSQLValue(term);
}


var WhereOperatorMap: any = {};
WhereOperatorMap[Queries.LookupType.EXACT] = "=";
WhereOperatorMap[Queries.LookupType.GT] = ">";
WhereOperatorMap[Queries.LookupType.GTE] = ">=";
WhereOperatorMap[Queries.LookupType.LT] = "<";
WhereOperatorMap[Queries.LookupType.LTE] = "=<";

var getWhereClause = function(
    lookup: Queries.FieldLookup, field: string
): string {
    if (lookup.type == Queries.LookupType.IN) {
        var values = lookup.value.map((val) => '"' + val + '"').join(',');
        return field + " IN (" + values + ")";
    };

    var operator = WhereOperatorMap[lookup.type];
    if (!operator) {
        throw "No valid where query for " + Queries.LookupType[lookup.type];
    }

    return field + " " + operator + ' "' +
           getLookupTestValue(lookup, lookup.value) + '"';
}


export = WebSQLStorage;
