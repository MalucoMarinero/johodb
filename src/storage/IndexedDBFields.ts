///<reference path="../../typedefs/node.d.ts"/>
///<reference path="../../typedefs/bluebird.d.ts"/>

var Promise: Bluebird.PromiseStatic = require('bluebird');

var READ_WRITE_FLAG = "readwrite";
import Schema = require('../Schema');
import Fields = require('../Fields');

class IDBMapping {
    constructor(public name: string, public field: Fields.Field) { }

    makeIDBIndex(store) {
    }

}
