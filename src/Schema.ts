///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>
var _ = require('lodash');
var diff = require('deep-diff');
import Model = require('./Model');

export interface Schema {
    [key: string]: Table;
}

export interface Table {
    [key: string]: Column;
}

export interface Column {
    type: string;
    primaryKey?: boolean;
    unique?: boolean;
    required?: boolean;
    relation?: string;
    relatedName?: string;
    onDelete?: string;
    validators?: any[];
}


export function validateTable(table: Table): Boolean {
    _.forOwn(table, (val, key) => {
        validateColumn(val);
    });
    return true;
}

export function validateColumn(column: Column): Boolean {
    if (!column.type) throw new Error("Column missing type - " + column);
    return true;
}

export interface DatabaseModelling {
    models: {[key:string]: Model};
    relationTables: any;
}

export interface Migration {
    actions: MigrationAction[];
    frozenDatabaseModel?: DatabaseModelling;
}


export module MigrationActionTypes {
    export var ADD_TABLE = "add_table";
    export var ADD_COLUMN = "add_column";
    export var REMOVE_TABLE = "remove_table";
    export var REMOVE_COLUMN = "remove_column";
    export var ALTER_COLUMN = "alter_column";
    export var CUSTOM = "custom";
}

export class MigrationAction {
    constructor(
        public type: string,
        public args?: any
    ) {
    }

    apply(schema: Schema): Schema {
        switch (this.type) {
            case MigrationActionTypes.ADD_TABLE:
                schema[this.args.tableName] = {};
                break;

            case MigrationActionTypes.ADD_COLUMN:
                schema[this.args.tableName]
                      [this.args.columnName] = this.args.columnTraits;
                break;

            case MigrationActionTypes.ALTER_COLUMN:
                schema[this.args.tableName]
                      [this.args.columnName]
                      [this.args.columnTrait] = this.args.columnValue;
                break;

            case MigrationActionTypes.REMOVE_COLUMN:
                delete schema[this.args.tableName][this.args.columnName]
                break;

            case MigrationActionTypes.REMOVE_TABLE:
                delete schema[this.args.tableName]
                break;

            case MigrationActionTypes.CUSTOM:
                if (this.args.changeSchema) this.args.changeSchema(schema);
                break;

        }
        return schema;
    }

    toSuggestion(): string {
        return (
            'new JohoDB.MigrationAction("' + this.type + '", ' +
            JSON.stringify(this.args) +
            ')'
        );
    }
}

export function cloneAndStripNonMigratoryTraits(schema: Schema): Schema {
    schema = _.clone(schema);
    _.forOwn(schema, (table, key) => {
        _.forOwn(table, (column, key) => {
            if (column['validators']) delete column['validators'];
        });
    });

    return schema;
}


export function compareSchemas(schema: Schema, migratedSchema: Schema) {
    schema = cloneAndStripNonMigratoryTraits(schema);
    migratedSchema = cloneAndStripNonMigratoryTraits(migratedSchema);
    var schemaDiff = diff(migratedSchema, schema);

    if (!schemaDiff) return true; // migrations result matches schema

    var suggestedMigration = "";

    var actions: MigrationAction[] = schemaDiff.reduce((acts, diffNode) => {
        return acts.concat(diffToActions(diffNode));
    }, []);

    var message = (
        "db.addMigration({ actions: [\n" +
        actions.map((a) => '    ' + a.toSuggestion()).join(',\n') +
        "\n]});"
    );
    message = "UPDATED MIGRATION REQUIRED: \n\n" + message;
    console.warn(message);
    throw new Error(message);
}


export function diffToActions(diff: any): MigrationAction[] {
    var actions = [];

    // diff is a new table
    if (diff.path.length == 1 && diff.kind == "N") {
        actions.push(new MigrationAction(
            MigrationActionTypes.ADD_TABLE,
            {tableName: diff.path[0]}
        ));
        _.forOwn(diff.rhs, (val, key) => {
            actions.push(new MigrationAction(
                MigrationActionTypes.ADD_COLUMN,
                {tableName: diff.path[0], columnName: key, columnTraits: val}
            ));
        });
        return actions;
    }


    // diff is a table that should be removed
    if (diff.path.length == 1 && diff.kind == "D") {
        actions.push(new MigrationAction(
            MigrationActionTypes.REMOVE_TABLE,
            {tableName: diff.path[0]}
        ));
    }


    // diff is a new column
    if (diff.path.length == 2 && diff.kind == "N") {
        actions.push(new MigrationAction(
            MigrationActionTypes.ADD_COLUMN,
            {tableName: diff.path[0], columnName: diff.path[1],
             columnTraits: diff.rhs}
        ));
        return actions;
    }


    // diff is a column that should be removed
    if (diff.path.length == 2 && diff.kind == "D") {
        actions.push(new MigrationAction(
            MigrationActionTypes.REMOVE_COLUMN,
            {tableName: diff.path[0], columnName: diff.path[1]}
        ));
    }


    // diff is a column value that should be altered
    if (diff.path.length == 3 && diff.kind == "E") {
        actions.push(new MigrationAction(
            MigrationActionTypes.ALTER_COLUMN,
            {tableName: diff.path[0], columnName: diff.path[1],
             columnTrait: diff.path[2], columnValue: diff.rhs}
        ));
    }

    return actions;

}
