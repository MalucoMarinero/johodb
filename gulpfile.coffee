gulp = require 'gulp'
gutil = require 'gulp-util'
using = require 'gulp-using'
jade = require 'gulp-jade'
debug = require 'gulp-debug'
concat = require 'gulp-concat'
connect = require 'gulp-connect'
wrap = require 'gulp-wrap-amd'
rename = require 'gulp-rename'
through = require 'through2'
tsc = require 'gulp-tsc'
browserify = require 'gulp-browserify'
shell = require 'gulp-shell'
watch = require 'gulp-watch'


source =
  ts: 'src'
  tests: 'tests'
  docs: 'docs'

target =
  js: '.tmp/src'
  root: '.tmp'
  tests: '.tmp/tests'
  build: 'build'
  docs: 'docs/_build/html'

gulp.task 'styles', ->
  gulp.src("#{ client.src }/styles/main.sass")
      .pipe(sass(style: 'expanded'))
      .pipe(gulp.dest("#{ client.devOut }/styles"))
      .pipe(using(prefix: 'Output CSS to'))

gulp.task "tsc", ->
  gulp.src("{src,tests}/**/*.ts")
    .pipe(tsc(outDir: "#{ target.root }", emitError: false, target: "ES5"))
    .pipe(gulp.dest("#{ target.root }"))
    .pipe(using(prefix: 'Output JS to'))


gulp.task "build:node", ->
  gulp.src("src/**/*.ts")
    .pipe(tsc(outDir: "#{ target.build }", emitError: false, target: "ES5"))
    .pipe(gulp.dest("#{ target.build }/node"))
    .pipe(using(prefix: 'Output Built JS to'))

gulp.task 'browserify:src', ->
  gulp.src("#{ target.js }/JohoDB.js")
    .pipe(browserify())
    .pipe(rename('johodb.js'))
    .pipe(gulp.dest(target.build))
    .pipe(using(prefix: 'Output Bundle to'))

gulp.task 'browserify:tests', ->
  gulp.src("#{ target.tests }/TestBase.js")
    .pipe(browserify())
    .pipe(rename('tests.js'))
    .pipe(gulp.dest(target.build))
    .pipe(using(prefix: 'Output Bundle to'))


gulp.task 'build:docs', shell.task([
  'make html'
], cwd: 'docs')


gulp.task 'default', ['tsc'],  ->
  watch "#{ source.ts }/**/*.ts", (files) ->
    files.pipe(tsc(outDir: "#{ target.js }", emitError: false, target: "ES5"))
         .pipe(gulp.dest("#{ target.js }"))
         .pipe(using(prefix: 'Output JS to'))

  watch "#{ source.tests }/**/*.ts", (files) ->
    files.pipe(tsc(
      outDir: "#{ target.root }", sourceRoot: source.root, emitError: false,
      target: "ES5"
    ))
         .pipe(gulp.dest("#{ target.root }"))
         .pipe(using(prefix: 'Output JS to'))

  # gulp.watch("#{ target.js }/**/*.js", ["browserify:src"])
  gulp.watch("#{ target.root }/**/*.js", ["browserify:tests"])


gulp.task 'docs', ['build:docs'],  ->
  gulp.watch("#{ source.docs }/**/*.rst", ['build:docs'])

  connect.server {
    root: target.docs
    port: 3035
    livereload: true
  }
