# JohoDB

## Preamble

JohoDB is an abstraction library strictly for browser supported databases. It allows the developer to build a relational database for use on the client, completely independant of any server calls. JohoDB currently supports IndexedDB and WebSQL, which allows it to support the following browsers:

## Installation

Using Browserify:

    npm install johodb

    var JohoDB = require('johodb');


If you just want a static file you can use on the client, use ./build/johodb.js



## Documentation

Full Documentation: http://johodb.readthedocs.org/en/latest/

## Browser Support

### Desktop Browsers


- Safari 3.1+
- Opera 10.5+
- Chrome 4.0+
- Firefox 16.0+
- Internet Explorer 10.0+

### Mobile/Tablet Browsers

- iOS Safari 3.2+
- Android 2.1+
- Chrome for Android 35+
- Blackberry 7.0+
- Opera Mobile 11.0+
- Firefox for Android 29.0+
- IE Mobile 10.0+

Local storage is not and **will never be** supported by JohoDB. It is merely a key value store and any attempt at implementation would be horribly inefficient, and looking to the future this browser support level is adequate for the purposes JohoDB is meant for, namely complex web applications.




## MIT License

Copyright (c) 2013 Full and By Design

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

