///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>

import JohoDB = require('../src/JohoDB');
var Promise: Bluebird.PromiseStatic = require('bluebird');

export function loadMessagePersonSchema(db: JohoDB) {
    db.addSchema('Message', {
        'id': {type: "string", primaryKey: true},
        'body': {type: "string", required: true},
        'created_by': {type: "fk", required: true,
                       relation: 'Person', relatedName: 'messages'},
    });

    db.addSchema('Person', {
        'id': {type: "string", primaryKey: true},
        'name': {type: "string", required: true},
        'age': {type: "int"},
    });

    db.addMigration({ actions: [
        new JohoDB.MigrationAction("add_table", {"tableName":"Message"}),
        new JohoDB.MigrationAction("add_column", {"tableName":"Message","columnName":"id","columnTraits":{"type":"string","primaryKey":true}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"Message","columnName":"body","columnTraits":{"type":"string","required":true}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"Message","columnName":"created_by","columnTraits":{"type":"fk","required":true,"relation":"Person","relatedName":"messages"}}),
        new JohoDB.MigrationAction("add_table", {"tableName":"Person"}),
        new JohoDB.MigrationAction("add_column", {"tableName":"Person","columnName":"id","columnTraits":{"type":"string","primaryKey":true}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"Person","columnName":"name","columnTraits":{"type":"string","required":true}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"Person","columnName":"age","columnTraits":{"type":"int"}}),
    ]});

}


export function loadAllFieldsSchema(db: JohoDB) {
    db.addSchema('AllFields', {
        'id': {type: "string", primaryKey: true},
        'string': {type: "string"},
        'int': {type: "int"},
        'float': {type: "float"},
        'boolean': {type: "boolean"},
        'json': {type: "json"},
        'datetime': {type: "datetime"},
        'time': {type: "time"},
        'date': {type: "date"},
        'email': {type: "string", validators:[JohoDB.Validation.isEmail]},
        'created_by': {type: "fk",
                       relation: 'RelTarget', relatedName: 'backRef'},
    });

    db.addSchema('RelTarget', {
        'id': {type: "string", primaryKey: true},
        'string': {type: "string"},
    });

    db.addMigration({ actions: [
        new JohoDB.MigrationAction("add_table", {"tableName":"AllFields"}),
        new JohoDB.MigrationAction("add_column", {"tableName":"AllFields","columnName":"id","columnTraits":{"type":"string","primaryKey":true}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"AllFields","columnName":"string","columnTraits":{"type":"string"}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"AllFields","columnName":"int","columnTraits":{"type":"int"}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"AllFields","columnName":"float","columnTraits":{"type":"float"}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"AllFields","columnName":"boolean","columnTraits":{"type":"boolean"}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"AllFields","columnName":"json","columnTraits":{"type":"json"}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"AllFields","columnName":"datetime","columnTraits":{"type":"datetime"}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"AllFields","columnName":"time","columnTraits":{"type":"time"}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"AllFields","columnName":"date","columnTraits":{"type":"date"}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"AllFields","columnName":"email","columnTraits":{"type":"string"}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"AllFields","columnName":"created_by","columnTraits":{"type":"fk","relation":"RelTarget","relatedName":"backRef"}}),
        new JohoDB.MigrationAction("add_table", {"tableName":"RelTarget"}),
        new JohoDB.MigrationAction("add_column", {"tableName":"RelTarget","columnName":"id","columnTraits":{"type":"string","primaryKey":true}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"RelTarget","columnName":"string","columnTraits":{"type":"string"}})
    ]});

}



export function loadHouseholdSchema(db: JohoDB) {
    db.addSchema("Household", {
      'id': {type: "string", primaryKey: true},
      'address': {type: "string", required: true},
      'suburb': {type: "string", required: true},
      'postCode': {type: "string", required: true},
    });
    db.addSchema('People', {
      'id': {type: "string", primaryKey: true},
      'firstName': {type: "string", required: true},
      'lastName': {type: "string", required: true},
      'email': {type: "string", validators:[JohoDB.Validation.isEmail]},
      'dateOfBirth': {type: "datetime", required: true},
      'home': {type: "fk", relation: "Household", relatedName: "occupants", onDelete: "setNull"},
      'bio': {type: "string"},
      'employed': {type: "boolean", default: true},
      'pets': {type: "m2m", relation: "Pets", relatedName: "owners"}
    });
    db.addSchema('Pets', {
      'id': {type: "string", primaryKey: true},
      'name': {type: "string", required: true},
      'age': {type: "int"},
    });
    // db.addSchema('Comments', {
    //   id: {type: "string", primaryKey: true, autoHash: true},
    //   text: {type: "string", required: true},
    //   subject: {type: "gk"},
    // });
    db.addMigration({ actions: [
        new JohoDB.MigrationAction("add_table", {"tableName":"Household"}),
        new JohoDB.MigrationAction("add_column", {"tableName":"Household","columnName":"id","columnTraits":{"type":"string","primaryKey":true}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"Household","columnName":"address","columnTraits":{"type":"string","required":true}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"Household","columnName":"suburb","columnTraits":{"type":"string","required":true}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"Household","columnName":"postCode","columnTraits":{"type":"string","required":true}}),
        new JohoDB.MigrationAction("add_table", {"tableName":"People"}),
        new JohoDB.MigrationAction("add_column", {"tableName":"People","columnName":"id","columnTraits":{"type":"string","primaryKey":true}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"People","columnName":"firstName","columnTraits":{"type":"string","required":true}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"People","columnName":"lastName","columnTraits":{"type":"string","required":true}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"People","columnName":"email","columnTraits":{"type":"string"}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"People","columnName":"dateOfBirth","columnTraits":{"type":"datetime","required":true}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"People","columnName":"home","columnTraits":{"type":"fk","relation":"Household","relatedName":"occupants","onDelete":"setNull"}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"People","columnName":"bio","columnTraits":{"type":"string"}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"People","columnName":"employed","columnTraits":{"type":"boolean","default":true}}),
        new JohoDB.MigrationAction("add_table", {"tableName":"Pets"}),
        new JohoDB.MigrationAction("add_column", {"tableName":"Pets","columnName":"id","columnTraits":{"type":"string","primaryKey":true}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"Pets","columnName":"name","columnTraits":{"type":"string","required":true}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"Pets","columnName":"age","columnTraits":{"type":"int"}}),
        new JohoDB.MigrationAction("add_column", {"tableName":"People","columnName":"pets","columnTraits":{"type":"m2m","relation":"Pets","relatedName":"owners"}})
    ]});
}

export var prefab = {
    "Household": {
      validList: [
        {address: "52 Sandy Bay Rd", suburb: "Sandy Bay", postCode: "7005"},
        {address: "123 Mall Rd", suburb:"Moonah", postCode: "7001"},
        {address: "512 Plenty Rd", suburb:"Moonah", postCode: "7001"},
      ],

      invalidList: [
        {address: "52 Sandy Bay Rd", suburb: "Sandy Bay"},
        {address: "123 Mall Rd", postCode: "7001"},
      ],

      no1: {address: "52 Sandy Bay Rd", suburb: "Sandy Bay", postCode: "7005"},
      no2: {address: "123 Mall Rd", suburb:"Moonah", postCode: "7001"},
      no3: {address: "512 Plenty Rd", suburb:"Moonah", postCode: "7001"},

      oneToMany1: {
        address: "52 Goldilock Road",
        suburb: "Manjimup",
        postCode: "8732",
        occupants: [
          {id: "jrakich", firstName: "James", lastName: "Rakich", dateOfBirth: new Date(1952, 10, 12)},
          {id: "joebloggs", firstName: "Joe", lastName: "Bloggs", dateOfBirth: new Date(1982, 9, 2)}
        ],
      },
      oneToManyFail: {
        address: "52 Goldilock Road",
        suburb: "Manjimup",
        postCode: "8732",
        occupants: [
          {firstName: "James", lastName: "Rakich", dateOfBirth: new Date(1952, 10, 12)},
          {firstName: "Joe", lastName: "Bloggs", dateOfBirth: new Date(1982, 9, 2)}
        ],
      },
    },
    "People": {
      fail1: {firstName: "Byron", lastName: "Briony", email: "notValid"},
      no1: {id: "jrakich2", firstName: "James", lastName: "Rakich", dateOfBirth: new Date(1952, 10, 12), card: {testObj: "hello there", number5: 5, dob: new Date()}},
      no2: {id: "joebloggs2", firstName: "Joe", lastName: "Bloggs", dateOfBirth: new Date(1938, 1, 12), card: {testObj: "hello", number2: 2, dob: new Date()}},
    },
    "Pets": {
        no1: {id: "barney", name: "Barney", age: 32},
        no2: {id: "tiger", name: "Tiger", age: 21},
        no3: {id: "gigantor", name: "Gigantor", age: 18},
    }
};


export var dataSet = {
    "Households": [
      {id: "52", address: "52 Sandy Bay Rd", suburb: "Sandy Bay", postCode: "7005"},
      {id: "123", address: "123 Mall Rd", suburb:"Moonah", postCode: "7001"},
      {id: "512", address: "512 Plenty Rd", suburb:"Moonah", postCode: "7001", occupants: [
        {id: "flobullimore", firstName: "Flo", lastName: "Bullimore", dateOfBirth: new Date(1983, 8, 28)}
      ]},
      {id: "52A", address: "52 Goldilock Road", suburb: "Manjimup", postCode: "8732", occupants: [
        {id: "jrakich", firstName: "James", lastName: "Rakich", dateOfBirth: new Date(1952, 10, 12)},
        {id: "jbloggs", firstName: "Joe", lastName: "Bloggs", dateOfBirth: new Date(1982, 9, 2)}
      ]},
    ],
    "People": [
      {id: "hotelbob", firstName: "Hotel", lastName: "Bob",
       dateOfBirth: new Date(1920, 5, 5),
       pets: [
        {id: "barney", name: "Barney", age: 32},
        {id: "gigantor", name: "Gigantor", age: 18}
      ]}
    ]
};

export function loadDataset(db): Bluebird.Promise {
    var stack = dataSet.Households.map((household) => {
        return db.models.Household.save(household, {updateRelatedRecords: true});
    });
    stack = stack.concat(dataSet.People.map((person) => {
        return db.models.People.save(person, {updateRelatedRecords: true});
    }));
    return Promise.all(stack);
}

