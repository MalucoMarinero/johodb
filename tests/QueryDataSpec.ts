///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>

var _ = require('lodash');
declare var describe;
declare var it;
declare var before;
declare var beforeEach;
declare var React;
declare var assert;
var Promise: Bluebird.PromiseStatic = require('bluebird');
var sinon = require('sinon');
var expect = require('chai').expect;

import JohoDB = require('../src/JohoDB');
import Fixtures = require('./Fixtures');
var mTypes = JohoDB.MigrationActionTypes;

describe('JohoDB Queries', function() {
    describe('Queries', function() {
        it('build an all query with one step', () => {
            var db = new JohoDB("queryTest");
            Fixtures.loadMessagePersonSchema(db);
            
            db.init({dontLoadStore: true});
            var q = db.models['Person'].query().all();
            expect(q.steps.length).to.equal(1);
        });

        it('queries should chain clones, not mutate', () => {
            var db = new JohoDB("queryTest");
            Fixtures.loadMessagePersonSchema(db);
            
            db.init({dontLoadStore: true});
            var q = db.models['Person'].query().filter({
                id: "eoa", name: "blahblah"
            });

            var q2 = q.exclude({id: "blahblah"});
            expect(q.steps.length).to.equal(1);
            expect(q2.steps.length).to.equal(2);
        });

    });

    var testRunner = function(storageEngine) { return function() {
        var db;
        before((done) => {
            db = new JohoDB("queryTest");
            Fixtures.loadHouseholdSchema(db);
            db.init({storageType: storageEngine, clobber: true}).then(() => {
                return Fixtures.loadDataset(db);
            }).delay(20).then(() => {
                done();
            });
        });

        it('all query returns all saved records', (done) => {
            db.models['People'].query().all().evaluate().then((records) => {
                expect(records.length).to.equal(4);
                done();
            }).catch(done);
        });

        it('by default collect 1 relationship layer', (done) => {
            db.models['Household'].query().all().evaluate().then((records) => {
                var hasOccupants = records.every((house) => {
                    return _.isArray(house.occupants);
                });
                expect(hasOccupants).to.be.true;
                done();
            }).catch(done);
        });

        it('can query specific relations', (done) => {
            db.models['People'].query().all().evaluate({
              only: ["pets"]
            }).then((records) => {
                records.forEach((record) => {
                  expect(record.home).to.be.undefined;
                  expect(record.pets).to.be.instanceof(Array);
                });
                done();
            }).catch(done);
        });

        it('can fetch via relations', (done) => {
            db.models['Household'].query().all().evaluate({
              depth: 2,
              only: ["occupants__pets"]
            }).then((records) => {
                records.forEach((record) => {
                  expect(record.occupants).to.be.instanceof(Array);
                  record.occupants.forEach((occ) => {
                    expect(occ.pets).to.be.instanceof(Array);
                  });
                });
                done();
            }).catch(done);
        });


        it('get query returns matching record', (done) => {
            return db.models['People'].query()
                     .get({'id': 'jrakich'}).evaluate().then((record) => {
                expect(record.id).to.equal('jrakich');
                expect(record.firstName).to.equal('James');
                done();
            }).catch(done);
        });

        it('get query returns null when no match', (done) => {
            return db.models['People'].query()
                     .get({'id': 'nulled'}).evaluate().then((record) => {
                expect(record).to.be.null;
                done();
            }).catch(done);
        });

        it('filter() should narrow down results accurately', (done) => {
            var tests = [
            {
                query: db.models.Household.query().filter({'postCode': "7001"}),
                expectedLength: 2
            },
            {
                query: db.models.Household.query().filter({'postCode__gt': "7001"}),
                expectedLength: 2
            },
            {
                query: db.models.Household.query().filter({'postCode__gte': "7001"}),
                expectedLength: 4
            },
            {
                query: db.models.Household.query().filter({'postCode__in': ["7001", "8732"]}),
                expectedLength: 3
            },
            {
                query: db.models.Household.query().filter(
                    {'postCode__gte': "7001", "suburb": "Sandy Bay"}
                ),
                expectedLength: 1
            },
            ];

            var stack = tests.map((test, ix) => {
                return test.query.evaluate().then((results) => {
                    expect(results.length).to.equal(
                        test.expectedLength, "Test " + (ix + 1)
                    );
                });
            });

            Promise.all(stack).then(() => {
                done();
            }).catch(done);
        });

        it('exclude() should narrow down results accurately', (done) => {
            var tests = [
            {
                query: db.models.Household.query().exclude({'postCode': "7005"}),
                expectedLength: 3
            },
            {
                query: db.models.Household.query().exclude({'postCode__gte': "7005"}),
                expectedLength: 2
            },
            {
                query: db.models.Household.query().exclude({'suburb': "Moonah"}),
                expectedLength: 2
            },
            {
                query: db.models.Household.query().exclude({'suburb': "Manjimup"}),
                expectedLength: 3
            },
            {
                query: db.models.Household.query().exclude({'postCode__gt': "2000"}),
                expectedLength: 0
            },
            ];

            var stack = tests.map((test, ix) => {
                return test.query.evaluate().then((results) => {
                    expect(results.length).to.equal(
                        test.expectedLength, "Test " + (ix + 1)
                    );
                });
            });

            Promise.all(stack).then(() => {
                done();
            }).catch(done);
        });


        it('filter() and exclude() should narrow down results accurately', (done) => {
            var tests = [
            {
                query: db.models.Household.query()
                                .exclude({'postCode': "7005"})
                                .filter({'suburb': 'Moonah'}),
                expectedLength: 2,
                matches: [
                    (record) => record.suburb == 'Moonah'
                ],
            },
            {
                query: db.models.Household.query()
                                .filter({'postCode__gt': "7000"})
                                .exclude({'postCode__lt': "7005"}),
                expectedLength: 2,
                matches: [],
            },
            {
                query: db.models.People.query()
                                .filter({'lastName__lt': "Rakich"})
                                .exclude({'firstName': "Joe"}),
                expectedLength: 2,
                matches: [
                    (record) => record.firstName != 'Joe'
                ],
            },
            ];

            var stack = tests.map((test, ix) => {
                return test.query.evaluate().then((results) => {
                    expect(results.length).to.equal(
                        test.expectedLength, "Test " + (ix + 1)
                    );
                    if (!test.matches) return;
                    test.matches.forEach((match) => {
                        expect(results.every(match)).to.equal(
                            true, "Match failed = " + match
                        );
                    });
                });
            });

            Promise.all(stack).then(() => {
                done();
            }).catch(done);
        });

        it('two level lookups should retrieve accurately', (done) => {
            var tests = [
            {
                query: db.models.People.query()
                                .filter({'home__suburb': "Manjimup"}),
                expectedLength: 2,
                matches: [
                    (record) => record.home.suburb == 'Manjimup'
                ],
            },
            {
                query: db.models.Household.query()
                                .filter({'occupants__firstName': "James"}),
                expectedLength: 1,
                matches: [],
            },
            {
                query: db.models.Household.query()
                                .filter({'occupants__lastName__lt': "Rakich"}),
                expectedLength: 2,
                matches: [],
            },
            ];

            var stack = tests.map((test, ix) => {
                return test.query.evaluate().then((results) => {
                    expect(results.length).to.equal(
                        test.expectedLength, "Test " + (ix + 1)
                    );
                    if (!test.matches) return;
                    test.matches.forEach((match) => {
                        expect(results.every(match)).to.equal(
                            true, "Match failed = " + match
                        );
                    });
                });
            });

            Promise.all(stack).then(() => {
                done();
            }).catch(done);
        });
    }};

    describe('IndexedDB', testRunner(JohoDB.IndexedDBStorage));
    describe('WebSQL', testRunner(JohoDB.WebSQLStorage));
});
