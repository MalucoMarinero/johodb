///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/moment.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>

var _ = require('lodash');
var moment = require('moment');
declare var describe;
declare var it;
declare var before;
declare var beforeEach;
declare var React;
declare var assert;
var Promise: Bluebird.PromiseStatic = require('bluebird');
var sinon = require('sinon');
var expect = require('chai').expect;

import JohoDB = require('../src/JohoDB');
import Fixtures = require('./Fixtures');
var mTypes = JohoDB.MigrationActionTypes;

describe('JohoDB Fields', function() {


    var testRunner = function(storageEngine) { return function() {
        var db;
        before((done) => {
            db = new JohoDB("fieldsTesting");
            Fixtures.loadAllFieldsSchema(db);
            db.init({storageType: storageEngine, clobber: true});
            done();
        });

        var idNum = 0;

        var testValue = function(name, value) { return function(done) {
            idNum++;
            var ID = idNum.toString();
            var saveRecord = {'id': ID};
            saveRecord[name] = value;

            db.models['AllFields'].save(saveRecord).delay(1).then(() => {
                return db.models['AllFields'].query().get({'id': ID}).evaluate();
            }).then((record) => {
                if (moment.isMoment(record[name])) {
                    var unit = 'minute';
                    if (name == 'date') {
                        unit = 'day';
                    }
                    if (name == 'time') {
                      expect(moment(record[name]).hour()).to.equal(value.hour());
                      expect(moment(record[name]).minute()).to.equal(value.minute());
                      expect(moment(record[name]).second()).to.equal(value.second());
                    } else {
                      expect(moment(record[name]).isSame(value, unit)).to.be.true;
                    }

                } else {
                    expect(_.isEqual(record[name], value)).to.be.true;
                }
                done();
            }).catch(done);
        }};

        describe('Primitives saved and retrieved', function() {
        it('string', testValue( 'string', 'TEST_STRING'));
        it('undefined string', testValue('string', undefined));

        it('integer', testValue('int', 2301));
        it('undefined integer', testValue('int', undefined));

        it('float', testValue('float', 2301.242));
        it('undefined float', testValue('float', undefined));

        it('true boolean', testValue( 'boolean', true));
        it('false boolean', testValue( 'boolean', false));
        it('undefined boolean', testValue( 'boolean', undefined));
        });

        describe('Custom Validation saved and retrieved', function() {
        it('email', testValue( 'email', 'joebloggs@example.com'));
        });

        describe('Temporal moments() saved and retrieved', function() {
        it('undefined datetime', testValue('datetime', undefined));
        it('undefined date', testValue('date', undefined));
        it('undefined time', testValue('time', undefined));

        it('now datetime', testValue('datetime', moment()));
        it('now date', testValue('date', moment()));
        it('now time', testValue('time', moment()));
        });

        describe('JSON saved and retrieved', function() {
        it('undefined', testValue( 'json', undefined));
        it('string', testValue( 'json', "this is a string"));
        it('array', testValue( 'json', [1,2,3,4]));
        it('number', testValue( 'json', 23));
        it('object', testValue( 'json', {
            "bill": [1,2,3],
            "saw": "ueotnhdueo",
        }));
        });
    }};

    describe('IndexedDB', testRunner(JohoDB.IndexedDBStorage));
    describe('WebSQL', testRunner(JohoDB.WebSQLStorage));
});
