///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>

var _ = require('lodash');
declare var describe;
declare var it;
declare var beforeEach;
declare var React;
declare var assert;
var Promise: Bluebird.PromiseStatic = require('bluebird');
var sinon = require('sinon');
var expect = require('chai').expect;

import JohoDB = require('../src/JohoDB');
var mTypes = JohoDB.MigrationActionTypes;

describe('JohoDB Modelling Setup', function() {
    describe('Models', function() {

        var addDefaultSchema = (db) => {
            db.addSchema('Message', {
                'id': {type: "string", primaryKey: true},
                'body': {type: "string", required: true},
                'created_by': {type: "fk", required: true,
                               relation: 'Person', relatedName: 'messages'},
            });

            db.addSchema('Person', {
                'id': {type: "string", primaryKey: true},
                'name': {type: "string", required: true},
            });

            db.addMigration({ actions: [
                new JohoDB.MigrationAction("add_table", {"tableName":"Message"}),
                new JohoDB.MigrationAction("add_column", {"tableName":"Message","columnName":"id","columnTraits":{"type":"string","primaryKey":true}}),
                new JohoDB.MigrationAction("add_column", {"tableName":"Message","columnName":"body","columnTraits":{"type":"string","required":true}}),
                new JohoDB.MigrationAction("add_column", {"tableName":"Message","columnName":"created_by","columnTraits":{"type":"fk","required":true,"relation":"Person","relatedName":"messages"}}),
                new JohoDB.MigrationAction("add_table", {"tableName":"Person"}),
                new JohoDB.MigrationAction("add_column", {"tableName":"Person","columnName":"id","columnTraits":{"type":"string","primaryKey":true}}),
                new JohoDB.MigrationAction("add_column", {"tableName":"Person","columnName":"name","columnTraits":{"type":"string","required":true}})
            ]});
        }

        it('should initialize with default field registry', () => {
            var db = new JohoDB("dbName");
            addDefaultSchema(db);
            
            db.init({dontLoadStore: true});
            expect(db.fieldRegistry).to.equal(JohoDB.defaultFieldRegistry);
        });

        it('should initialize with correct models', () => {
            var db = new JohoDB("dbName");
            addDefaultSchema(db);
            db.init({dontLoadStore: true});
            expect(_.keys(db.models).length).to.equal(2);
            expect(db.models['Person']).to.exist;
            expect(db.models['Message']).to.exist;
        });

        it('should initialize with no relationship tables', () => {
            var db = new JohoDB("dbName");
            addDefaultSchema(db);
            db.init({dontLoadStore: true});
            expect(_.keys(db.relationTables).length).to.equal(0);
        });
    });
});
