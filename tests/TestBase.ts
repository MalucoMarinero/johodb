///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>

declare var mocha;

mocha.setup('bdd');

require('./InitSpec');
require('./ModellingSpec');
require('./DatabaseInitSpec');
require('./ValidateDataSpec');
require('./TestFields');
require('./SaveDataSpec');

require('./QueryDataSpec');
