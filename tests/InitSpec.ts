///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>

declare var describe;
declare var it;
declare var beforeEach;
declare var React;
declare var assert;
var Promise: Bluebird.PromiseStatic = require('bluebird');
var sinon = require('sinon');
var expect = require('chai').expect;

import JohoDB = require('../src/JohoDB');
var mTypes = JohoDB.MigrationActionTypes;

describe('JohoDB Schema Initialization', function() {
    describe('JohoDB Creation', function() {

        it('has a name', () => {
            var db = new JohoDB("dbName");
            expect(db.name).to.equal("dbName");
        });

        it('accepts schema tables', () => {
            var db = new JohoDB("dbName");
            db.addSchema('Message', {
                'id': {type: "string", primaryKey: true},
                'body': {type: "string", required: true},
            });

            db.addSchema('Person', {
                'id': {type: "string", primaryKey: true},
                'name': {type: "string", required: true},
            });
        });

        it('disallows schema columns without a type', () => {
            var db = new JohoDB("dbName");
            expect(() => {
                db.addSchema('Message', {
                    'id': {type: "", primaryKey: true},
                    'body': {type: "string", required: true},
                })
            }).to.throw(Error);
        });
    });


    describe('Migrations', function() {

        var addDefaultSchema = (db) => {
            db.addSchema('Message', {
                'id': {type: "string", primaryKey: true},
                'body': {type: "string", required: true},
            });

            db.addSchema('Person', {
                'id': {type: "string", primaryKey: true},
                'name': {type: "string", required: true},
            });
        }

        it('should throw if migrations will not reach schema', () => {
            var db = new JohoDB("dbName");
            addDefaultSchema(db);
            
            expect(() => {
                db.init({dontLoadStore: true});
            }).to.throw(Error, /UPDATED MIGRATION REQUIRED/);
        });


        it('should succeed if migrations will reach schema', () => {
            var db = new JohoDB("dbName");
            addDefaultSchema(db);

            db.addMigration({ actions: [
                new JohoDB.MigrationAction(mTypes.ADD_TABLE, {tableName: 'Message'}),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Message', columnName: 'id', columnTraits: {type: 'string', primaryKey: true} }),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Message', columnName: 'body', columnTraits: {type: 'string', required: true} }),
                new JohoDB.MigrationAction(mTypes.ADD_TABLE, {tableName: 'Person'}),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Person', columnName: 'id', columnTraits: {type: 'string', primaryKey: true} }),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Person', columnName: 'name', columnTraits: {type: 'string', required: true} })
            ]});

            expect(() => {
                db.init({dontLoadStore: true});
            }).to.not.throw(Error);
        });


        it('should throw if migrations fall short of schema', () => {
            var db = new JohoDB("dbName");
            addDefaultSchema(db);

            db.addMigration({ actions: [
                new JohoDB.MigrationAction(mTypes.ADD_TABLE, {tableName: 'Message'}),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Message', columnName: 'id', columnTraits: {type: 'string', primaryKey: true} }),
            ]});

            expect(() => {
                db.init({dontLoadStore: true});
            }).to.throw(Error, /UPDATED MIGRATION REQUIRED/);
        });


        it('should suggest adding tables when tables are not migrated', () => {
            var db = new JohoDB("dbName");
            addDefaultSchema(db);


            db.addMigration({ actions: [
                new JohoDB.MigrationAction(mTypes.ADD_TABLE, {tableName: 'Message'}),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Message', columnName: 'id', columnTraits: {type: 'string', primaryKey: true} }),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Message', columnName: 'body', columnTraits: {type: 'string', required: true} }),
            ]});

            expect(() => {
                db.init({dontLoadStore: true});
            }).to.throw(Error, /add_table/);
        });


        it('should suggest adding columns when columns are not migrated', () => {
            var db = new JohoDB("dbName");
            addDefaultSchema(db);

            db.addMigration({ actions: [
                new JohoDB.MigrationAction(mTypes.ADD_TABLE, {tableName: 'Message'}),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Message', columnName: 'id', columnTraits: {type: 'string', primaryKey: true} }),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Message', columnName: 'body', columnTraits: {type: 'string', required: true} }),
                new JohoDB.MigrationAction(mTypes.ADD_TABLE, {tableName: 'Person'}),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Person', columnName: 'id', columnTraits: {type: 'string', primaryKey: true} }),
            ]});

            expect(() => {
                db.init({dontLoadStore: true});
            }).to.throw(Error, /add_column/);
        });


        it('should suggest removing tables when migrated tables are not in the schema', () => {
            var db = new JohoDB("dbName");
            addDefaultSchema(db);

            db.addMigration({ actions: [
                new JohoDB.MigrationAction(mTypes.ADD_TABLE, {tableName: 'Message'}),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Message', columnName: 'id', columnTraits: {type: 'string', primaryKey: true} }),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Message', columnName: 'body', columnTraits: {type: 'string', required: true} }),
                new JohoDB.MigrationAction(mTypes.ADD_TABLE, {tableName: 'Person'}),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Person', columnName: 'id', columnTraits: {type: 'string', primaryKey: true} }),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Person', columnName: 'name', columnTraits: {type: 'string', required: true} }),
                new JohoDB.MigrationAction(mTypes.ADD_TABLE, {tableName: 'Junk'}),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Junk', columnName: 'id', columnTraits: {type: 'string', primaryKey: true} }),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Junk', columnName: 'name', columnTraits: {type: 'string', required: true} })
            ]});

            expect(() => {
                db.init({dontLoadStore: true});
            }).to.throw(Error, /remove_table/);
        });


        it('should suggest removing columns when migrated columns are not in the schema', () => {
            var db = new JohoDB("dbName");
            addDefaultSchema(db);

            db.addMigration({ actions: [
                new JohoDB.MigrationAction(mTypes.ADD_TABLE, {tableName: 'Message'}),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Message', columnName: 'id', columnTraits: {type: 'string', primaryKey: true} }),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Message', columnName: 'body', columnTraits: {type: 'string', required: true} }),
                new JohoDB.MigrationAction(mTypes.ADD_TABLE, {tableName: 'Person'}),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Person', columnName: 'id', columnTraits: {type: 'string', primaryKey: true} }),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Person', columnName: 'name', columnTraits: {type: 'string', required: true} }),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Person', columnName: 'description', columnTraits: {type: 'string'} }),
            ]});

            expect(() => {
                db.init({dontLoadStore: true});
            }).to.throw(Error, /remove_column/);
        });


        it('should suggest altering columns when migrated columns are not the same as in the schema', () => {
            var db = new JohoDB("dbName");
            addDefaultSchema(db);

            db.addMigration({ actions: [
                new JohoDB.MigrationAction(mTypes.ADD_TABLE, {tableName: 'Message'}),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Message', columnName: 'id', columnTraits: {type: 'string', primaryKey: true} }),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Message', columnName: 'body', columnTraits: {type: 'string', required: true} }),
                new JohoDB.MigrationAction(mTypes.ADD_TABLE, {tableName: 'Person'}),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Person', columnName: 'id', columnTraits: {type: 'string', primaryKey: true} }),
                new JohoDB.MigrationAction(mTypes.ADD_COLUMN, {tableName: 'Person', columnName: 'name', columnTraits: {type: 'int', required: false} }),
            ]});

            expect(() => {
                db.init({dontLoadStore: true});
            }).to.throw(Error, /alter_column/);
        });
    });
});
