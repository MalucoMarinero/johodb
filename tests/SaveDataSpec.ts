///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>

var _ = require('lodash');
declare var describe;
declare var it;
declare var before;
declare var beforeEach;
declare var React;
declare var assert;
var Promise: Bluebird.PromiseStatic = require('bluebird');
var sinon = require('sinon');
var expect = require('chai').expect;

import JohoDB = require('../src/JohoDB');
import Fixtures = require('./Fixtures');
var mTypes = JohoDB.MigrationActionTypes;

describe('JohoDB Save Record', function() {
    var testRunner = function(storageEngine) { return function() {
        var db;

        before((done) => {
            db = new JohoDB("saveTest");
            Fixtures.loadHouseholdSchema(db);
            db.init({storageType: storageEngine, clobber: true}).then(() => {
                done();
            });
        });

        var idNum = 0;

        describe("Simple Saving", () => {
        it('save record', (done) => {
            idNum++;
            var idS = idNum.toString();
            db.models['Household'].save({
                'id': idS,
                'address': '11 Phillip St',
                'suburb': 'Mornay Bay',
                'postCode': "2405"
            }).then((record) => {
                expect(record['id']).to.equal(idS);
                expect(record['address']).to.equal('11 Phillip St');
                done();
            }).catch(done);
        });

        it('reject promise on failed validation', (done) => {
            idNum++; var idS = idNum.toString();
            var fixture = Fixtures.prefab.People.fail1;
            fixture['id'] = idS;

            db.models['People'].save(fixture).then((record) => {
                done(new Error("Expected not to succeed"));
            }).catch((error) => {
                expect(error.isValid).to.be.false;
                done();
            });
        });
        });

        describe("Saving OneToMany Relationships", () => {
        it('should save new related records when opts.updateRelatedRecords', (done) => {
            idNum++; var idS = idNum.toString();
            var fixture = Fixtures.prefab.Household.oneToMany1;
            fixture['id'] = idS;

            var p;
            p = db.models['Household'].save(fixture, {updateRelatedRecords: true});
            p = p.delay(1).then((result) => {
                return db.models['People'].query().filter({home: idS}).evaluate();
            }).then((occupants) => {
                expect(occupants.length).to.equal(2);
                expect(occupants[0].home.id).to.equal(idS);
                expect(occupants[1].home.id).to.equal(idS);
                done();
            }).catch(done);
        });

        it('should fail new related records when opts.updateRelatedRecords and no primaryKeys provided', (done) => {
            idNum++; var idS = idNum.toString();
            var fixture = Fixtures.prefab.Household.oneToManyFail;
            fixture['id'] = idS;

            var p;
            p = db.models['Household'].save(fixture, {updateRelatedRecords: true});
            p.then((record) => {
                done(new Error("Expected not to succeed"));
            }).catch((error) => {
                expect(error.isValid).to.be.false;
                done();
            });
        });

        it('should update relationship keys only when opts.updateRelations', (done) => {
            idNum++; var idS = idNum.toString();
            var fixture = _.clone(Fixtures.prefab.Household.oneToMany1);
            fixture['id'] = idS;

            var p;
            p = db.models['Household'].save(fixture, {updateRelatedRecords: true});
            p.then((record) => {
                fixture.occupants = [_.clone(fixture.occupants[0])];
                fixture.occupants[0].lastName = "NOT HIS NAME";
                return db.models['Household']
                         .save(fixture, {updateRelations: true});
            }).delay(1).then(() => {
                return db.models['People'].query().filter({home: idS}).evaluate();
            }).then((occupants) => {
                console.log(occupants);
                expect(occupants.length).to.equal(1);
                expect(occupants[0].home.id).to.equal(idS);
                expect(occupants[0].lastName).not.to.equal("NOT HIS NAME");
                done();
            }).catch(done);
        });

        it('should update relationship data when opts.updateRelatedRecords', (done) => {
            idNum++; var idS = idNum.toString();
            var fixture = _.clone(Fixtures.prefab.Household.oneToMany1);
            fixture['id'] = idS;

            var p;
            p = db.models['Household'].save(fixture, {updateRelatedRecords: true});
            p.then((record) => {
                fixture.occupants = [
                    _.clone(fixture.occupants[0]),
                    _.clone(fixture.occupants[1]),
                ];
                fixture.occupants[0].lastName = "Newlastname";
                console.log(fixture.occupants[0]);
                return db.models['Household']
                         .save(fixture, {updateRelatedRecords: true});
            }).delay(50).then(() => {
                return db.models['People'].query().filter({home: idS}).evaluate();
            }).then((occupants) => {
                expect(occupants.length).to.equal(2);
                var person = occupants.filter((occ) => occ.id == 'jrakich')[0];
                expect(person.home.id).to.equal(idS);
                expect(person.lastName).to.equal("Newlastname");
                done();
            }).catch(done);
        });
        });

        describe("Saving ManyToMany Relationships", () => {
        it('should create relationships through opts.updateRelations', (done) => {
            idNum++; var idS = idNum.toString();
            var pet1 = _.clone(Fixtures.prefab.Pets.no1);
            pet1['id'] = idS;

            idNum++; idS = idNum.toString();
            var pet2 = _.clone(Fixtures.prefab.Pets.no2);
            pet2['id'] = idS;

            db.models['Pets'].save(pet1).then(() => {
                return db.models['Pets'].save(pet2);
            }).then(() => {
                idNum++; idS = idNum.toString();
                var person = _.clone(Fixtures.prefab.People.no1);
                person.id = idS;
                person.pets = [pet1, pet2];

                return db.models['People'].save(person, {
                    updateRelations: true
                });
            }).delay(1).then(() => {
                return db.models['People'].query().get({id: idS}).evaluate();
            }).then((person) => {
                expect(person.pets.length).to.equal(2);
                done();
            }).catch(done);
        });

        it('should create relationships through opts.updateRelations from the subject', (done) => {
            idNum++; var idS = idNum.toString();
            var person1 = _.clone(Fixtures.prefab.People.no1);
            person1['id'] = idS;

            idNum++; idS = idNum.toString();
            var person2 = _.clone(Fixtures.prefab.People.no2);
            person2['id'] = idS;

            db.models['People'].save(person1).then(() => {
                return db.models['People'].save(person2);
            }).then(() => {
                idNum++; idS = idNum.toString();
                var pet = _.clone(Fixtures.prefab.Pets.no3);
                pet.id = idS;
                pet.owners = [person1, person2];

                return db.models['Pets'].save(pet, {
                    updateRelations: true
                });
            }).delay(1).then(() => {
                return db.models['Pets'].query().get({id: idS}).evaluate();
            }).then((person) => {
                expect(person.owners.length).to.equal(2);
                done();
            }).catch(done);
        });

        it('should create relationships through opts.updateRelatedRecords', (done) => {
            idNum++; var idS = idNum.toString();
            var pet1 = _.clone(Fixtures.prefab.Pets.no1);
            pet1['id'] = idS;

            idNum++; idS = idNum.toString();
            var pet2 = _.clone(Fixtures.prefab.Pets.no2);
            pet2['id'] = idS;

            db.models['Pets'].save(pet1).then(() => {
                return db.models['Pets'].save(pet2);
            }).then(() => {
                idNum++; idS = idNum.toString();
                var person = _.clone(Fixtures.prefab.People.no1);
                person.id = idS;
                person.pets = [pet1, pet2];

                return db.models['People'].save(person, {
                    updateRelatedRecords: true
                });
            }).delay(1).then(() => {
                return db.models['People'].query().get({id: idS}).evaluate();
            }).then((person) => {
                expect(person.pets.length).to.equal(2);
                done();
            }).catch(done);
        });

        it('should create relationships through opts.updateRelatedRecords from the subject', (done) => {
            idNum++; var idS = idNum.toString();
            var person1 = _.clone(Fixtures.prefab.People.no1);
            person1['id'] = idS;

            idNum++; idS = idNum.toString();
            var person2 = _.clone(Fixtures.prefab.People.no2);
            person2['id'] = idS;

            db.models['People'].save(person1).then(() => {
                return db.models['People'].save(person2);
            }).then(() => {
                idNum++; idS = idNum.toString();
                var pet = _.clone(Fixtures.prefab.Pets.no3);
                pet.id = idS;
                pet.owners = [person1, person2];

                return db.models['Pets'].save(pet, {
                    updateRelatedRecords: true
                });
            }).delay(1).then(() => {
                return db.models['Pets'].query().get({id: idS}).evaluate();
            }).then((person) => {
                expect(person.owners.length).to.equal(2);
                done();
            }).catch(done);
        });

        it('absent relationships removed through opts.updateRelations', (done) => {
            idNum++; var idS = idNum.toString();
            var pet1 = _.clone(Fixtures.prefab.Pets.no1);
            pet1['id'] = idS;

            idNum++; idS = idNum.toString();
            var pet2 = _.clone(Fixtures.prefab.Pets.no2);
            pet2['id'] = idS;

            db.models['Pets'].save(pet1).then(() => {
                return db.models['Pets'].save(pet2);
            }).then(() => {
                idNum++; idS = idNum.toString();
                var person = _.clone(Fixtures.prefab.People.no1);
                person.id = idS;
                person.pets = [pet1, pet2];

                return db.models['People'].save(person, {
                    updateRelatedRecords: true
                });
            }).delay(1).then(() => {
                var person = _.clone(Fixtures.prefab.People.no1);
                person.id = idS;
                person.pets = [pet1];
                return db.models['People'].save(person, {
                    updateRelations: true
                });
            }).delay(1).then(() => {
                return db.models['People'].query().get({id: idS}).evaluate();
            }).then((person) => {
                expect(person.pets.length).to.equal(1);
                done();
            }).catch(done);
        });

        it('absent relationships removed through opts.updateRelations from the subject', (done) => {
            idNum++; var idS = idNum.toString();
            var person1 = _.clone(Fixtures.prefab.People.no1);
            person1['id'] = idS;

            idNum++; idS = idNum.toString();
            var person2 = _.clone(Fixtures.prefab.People.no2);
            person2['id'] = idS;

            db.models['People'].save(person1).then(() => {
                return db.models['People'].save(person2);
            }).then(() => {
                idNum++; idS = idNum.toString();
                var pet = _.clone(Fixtures.prefab.Pets.no3);
                pet.id = idS;
                pet.owners = [person1, person2];

                return db.models['Pets'].save(pet, {
                    updateRelatedRecords: true
                });
            }).delay(1).then(() => {
                var pet = _.clone(Fixtures.prefab.Pets.no3);
                pet.id = idS;
                pet.owners = [person1];
                return db.models['Pets'].save(pet, {
                    updateRelations: true
                });
            }).delay(1).then(() => {
                return db.models['Pets'].query().get({id: idS}).evaluate();
            }).then((person) => {
                expect(person.owners.length).to.equal(1);
                done();
            }).catch(done);
        });

        it('absent relationships removed through opts.updateRelatedRecords', (done) => {
            idNum++; var idS = idNum.toString();
            var pet1 = _.clone(Fixtures.prefab.Pets.no1);
            pet1['id'] = idS;

            idNum++; idS = idNum.toString();
            var pet2 = _.clone(Fixtures.prefab.Pets.no2);
            pet2['id'] = idS;

            db.models['Pets'].save(pet1).then(() => {
                return db.models['Pets'].save(pet2);
            }).then(() => {
                idNum++; idS = idNum.toString();
                var person = _.clone(Fixtures.prefab.People.no1);
                person.id = idS;
                person.pets = [pet1, pet2];

                return db.models['People'].save(person, {
                    updateRelatedRecords: true
                });
            }).delay(1).then(() => {
                var person = _.clone(Fixtures.prefab.People.no1);
                person.id = idS;
                person.pets = [pet1];
                return db.models['People'].save(person, {
                    updateRelatedRecords: true
                });
            }).delay(1).then(() => {
                return db.models['People'].query().get({id: idS}).evaluate();
            }).then((person) => {
                expect(person.pets.length).to.equal(1);
                done();
            }).catch(done);
        });

        it('absent relationships removed through opts.updateRelatedRecords from the subject', (done) => {
            idNum++; var idS = idNum.toString();
            var person1 = _.clone(Fixtures.prefab.People.no1);
            person1['id'] = idS;

            idNum++; idS = idNum.toString();
            var person2 = _.clone(Fixtures.prefab.People.no2);
            person2['id'] = idS;

            db.models['People'].save(person1).then(() => {
                return db.models['People'].save(person2);
            }).then(() => {
                idNum++; idS = idNum.toString();
                var pet = _.clone(Fixtures.prefab.Pets.no3);
                pet.id = idS;
                pet.owners = [person1, person2];

                return db.models['Pets'].save(pet, {
                    updateRelatedRecords: true
                });
            }).delay(1).then(() => {
                var pet = _.clone(Fixtures.prefab.Pets.no3);
                pet.id = idS;
                pet.owners = [person1];
                return db.models['Pets'].save(pet, {
                    updateRelatedRecords: true
                });
            }).delay(1).then(() => {
                return db.models['Pets'].query().get({id: idS}).evaluate();
            }).then((person) => {
                expect(person.owners.length).to.equal(1);
                done();
            }).catch(done);
        });
        });

    } };

    describe('IndexedDB', testRunner(JohoDB.IndexedDBStorage));
    describe('WebSQL', testRunner(JohoDB.WebSQLStorage));
});
