///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>

var _ = require('lodash');
declare var describe;
declare var it;
declare var beforeEach;
declare var before;
declare var React;
declare var assert;
var Promise: Bluebird.PromiseStatic = require('bluebird');
var sinon = require('sinon');
var expect = require('chai').expect;

import JohoDB = require('../src/JohoDB');
import Fixtures = require('./Fixtures');
var mTypes = JohoDB.MigrationActionTypes;

describe('JohoDB Validate Record', function() {
    var testDB: JohoDB = null;

    before((done) => {
        testDB = new JohoDB("saveTest");
        Fixtures.loadMessagePersonSchema(testDB);
        testDB.init({dontLoadStore: true});
        done();
    });


    it('validate a simple record successfully', (done) => {
        var result = testDB.models['Person'].validate({
            'id': 'keyID',
            'name': 'TempPerson'
        }).then((result) => {
            expect(result.isValid).to.equal(true); done();
        }).catch(done);
    });

    it('fail validation on an incomplete record successfully', (done) => {
        var result = testDB.models['Person'].validate({
            'id': 'keyID'
        }).then((result) => {
            expect(result.isValid).to.equal(false); done();
        }).catch(done);
    });

    it('fail validation for number provided where string expected', (done) => {
        var result = testDB.models['Person'].validate({
            'id': 'keyID',
            'name': 312231
        }).then((result) => {
            expect(result.isValid).to.equal(false); done();
        }).catch(done);
    });

    it('fail validation for string provided where Integer expected', (done) => {
        var result = testDB.models['Person'].validate({
            'id': 'keyID',
            'name': 'Blogumbo',
            'age': 'Hello'
        }).then((result) => {
            expect(result.isValid).to.equal(false); done();
        }).catch(done);
    });
});
